<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStudentFeesStatusAndAdvanceFeeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->integer('advance_amount')->default(0);
            $table->tinyInteger('fee_status')->default(1);

        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('advance_amount');
            $table->dropColumn('fee_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropColumn('advance_amount');
            $table->dropColumn('fee_status');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->decimal('advance_amount')->default(0);
            $table->tinyInteger('fee_status')->default(1);

        });
    }
}
