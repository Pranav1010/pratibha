<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraFeesTable extends Migration
{

    public function up()
    {
        Schema::create('extra_fees', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('extra_fees');
    }
}
