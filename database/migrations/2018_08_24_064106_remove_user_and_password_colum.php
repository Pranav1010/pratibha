<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\AppUser;
use App\AppLogin;

class RemoveUserAndPasswordColum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $students = AppUser::all();
        foreach ($students as $student)
        {
            $appLogin = AppLogin::where('username',$student->contact_no)->first();
            if(!isset($appLogin))
            {
                $appLogin = new AppLogin();
                $appLogin->username = $student->contact_no;
                $appLogin->password = $student->password;
                $appLogin->save();
            }
            $student->app_login_id = $appLogin->id;
            $student->save();
        }

        Schema::table('app_users', function (Blueprint $table) {
            $table->dropColumn('user');
            $table->dropColumn('password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('app_users', function (Blueprint $table) {
            $table->text('user')->after('id');
            $table->text('password')->after('user');
        });
    }
}
