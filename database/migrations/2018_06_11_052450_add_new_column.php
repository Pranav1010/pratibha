<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->string('address')->after('name')->nullable();
            $table->string('contact_no',13)->after('address');
            $table->integer('city_id')->after('contact_no');
            $table->string('school_status',20)->after('city_id');
            $table->integer('branch_id')->after('school_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->dropColumn('address');
            $table->dropColumn('contact_no');
            $table->dropColumn('city_id');
            $table->dropColumn('school_status');
        });
    }
}
