<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquiryDependentExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inquiry_dependent_exams', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('dependent_id');
            $table->enum('exam_type',['IELTS','PTE','TOEFL','GRE']);
            $table->string('listening')->nullable();
            $table->string('reading')->nullable();
            $table->string('writing')->nullable();
            $table->string('speaking')->nullable();
            $table->string('overall')->nullable();
            $table->string('year')->nullable();
            $table->string('result_status')->nullable();
            $table->text('remark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inquiry_dependent_exams');
    }
}
