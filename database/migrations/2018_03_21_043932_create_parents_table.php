<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parents', function (Blueprint $table) {
            $table->increments('id');
            $table->text('father_name')->nullable();
            $table->text('father_mobile')->nullable();
            $table->text('father_email')->nullable();
            $table->integer('father_profession')->nullable();
            $table->integer('father_blood_group')->nullable();
            $table->text('aadhar_no_father')->nullable();
            $table->text('mother_name')->nullable();
            $table->text('mother_email')->nullable();
            $table->text('mother_mobile')->nullable();
            $table->text('mother_profession')->nullable();
            $table->integer('mother_blood_group')->nullable();
            $table->text('aadhar_no_mother')->nullable();
            $table->integer('city_id')->nullable();
            $table->text('address')->nullable();
            $table->unsignedInteger('branch_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parents');
    }
}
