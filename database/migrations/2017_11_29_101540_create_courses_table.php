<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('school_fees')->default(0);
            $table->boolean('category')->nullable();
            $table->timestamps();
        });

    }

    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
