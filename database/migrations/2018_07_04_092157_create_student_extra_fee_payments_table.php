<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentExtraFeePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_extra_fee_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->integer('activity_id');
            $table->integer('branch_id')->nullable();
            $table->text('extra_fee_id');
            $table->decimal('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_extra_fee_payments');
    }
}
