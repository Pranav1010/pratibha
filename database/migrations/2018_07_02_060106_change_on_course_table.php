<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeOnCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('school_fees');
            $table->integer('activity_id')->after('id');
            $table->integer('branch_id')->after('activity_id');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('courses', function (Blueprint $table) {
            $table->string('name')->after('id');
            $table->integer('school_fees')->after('name');
            $table->dropColumn('school_fees');
            $table->dropColumn('activity_id');
            $table->dropColumn('branch_id');
        });
    }
}
