<?php

use Illuminate\Database\Seeder;
use App\Course;
use App\Activity;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Activity::create([
            'id' => 1,
            'name' => 'Membership',
        ]);

        Course::create([
            'id' => 1,
            'activity_id' => 1,
            'branch_id' => 0,
        ]);


        \App\CourseFeeType::create([
            'course_id' => 1,
            'fee_type_id' => 2,
            'fees' => 500,
        ]);
    }
}
