<?php

use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Auth\Database\Menu;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Auth\Database\Permission;
use Illuminate\Database\Seeder;

class AdminTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create a user.
        Administrator::truncate();

        Administrator::create([
            'username' => 'superadmin',
            'password' => bcrypt('superadmin'),
            'name' => 'Super Administrator',
        ]);

        Administrator::create([
            'username' => 'admin',
            'password' => bcrypt('admin'),
            'name' => 'Administrator',
        ]);

        // create a role.
        Role::truncate();
        Role::insert([
            [
                'name' => 'Super Administrator',
                'slug' => 'super-administrator',
            ],
            [
                'name' => 'Administrator',
                'slug' => 'administrator',
            ],
            [
                'name' => 'Student',
                'slug' => 'student',
            ],
            [
                'name' => 'Branch Admin',
                'slug' => 'branch-admin'
            ],
            [
                'name' => 'Trainer',
                'slug' => 'trainer'
            ],
            [
                'name' => 'Supervisor',
                'slug' => 'supervisor'
            ],
            [
                'name' => 'School',
                'slug' => 'school'
            ],
        ]);

        // add role to user.
        Administrator::first()->roles()->save(Role::first());
        Administrator::where('username','admin')->first()->roles()->save(Role::where('slug','administrator')->first());

        //create a permission
        Permission::truncate();
        Permission::insert([
            [
                'name' => 'All permission',
                'slug' => '*',
                'http_method' => '',
                'http_path' => '*',
            ],
            [
                'name' => 'Dashboard',
                'slug' => 'dashboard',
                'http_method' => 'GET',
                'http_path' => "dashboard\r\n/\r\n",
            ],
            [
                'name' => 'Login',
                'slug' => 'auth.login',
                'http_method' => '',
                'http_path' => "auth/login\r\nauth/logout",
            ],
            [
                'name' => 'User setting',
                'slug' => 'auth.setting',
                'http_method' => 'GET,PUT',
                'http_path' => '/auth/setting',
            ],
            [
                'name' => 'Auth management',
                'slug' => 'auth.management',
                'http_method' => '',
                'http_path' => "auth/roles\r\nauth/permissions\r\nauth/users",
            ],
            [
                'name' => 'Masters',
                'slug' => 'masters',
                'http_method' => '',
                'http_path' => "cities*\r\nschools*\r\ncourses*\r\nbatches*\r\nbranches*\r\nsources*\r\nactivities*\r\nprofessions*\r\nbanks*\r\ninterestLevel*\r\nfollowUpStatus*\r\nfollowUpAction*\r\nbranch-extra-fee*\r\nstudent-extra-fee-payment*\r\nextra-fees*\r\nstudents*\r\nfees*\r\nmembership*\r\nnews*\r\nevents*\r\nmedia*\r\npayments*\r\nstudents-school*\r\nsearch-student*\r\ncourse-history*\r\ncheck-fees*\r\ncourse-stop*\r\ncourse-resume*\r\ncourse-pause*\r\nregister*\r\nstudent-detail*\r\ncompleted*\r\ncancelled*\r\nforwarded*\r\nauth/logoupload*\r\ndashboard*\r\nparents*",
            ],
            [
                'name' => 'Inquiry',
                'slug' => 'inquiry',
                'http_method' => '',
                'http_path' => "inquiries*\r\ninquiries/create\r\ninquiries/*/edit",
            ],
            [
                'name' => 'Follow Up',
                'slug' => 'follow_up',
                'http_method' => '',
                'http_path' => "followUp\r\nfollowUp\r\nfollowUp/create\r\nfollowUp/*/edit",
            ],
        ]);

        Role::first()->permissions()->save(Permission::first());
        foreach(Permission::where('id','!=','1')->where('id','!=','5')->get() as $permission)
        {
            Role::where('slug','administrator')->first()->permissions()->save($permission);
        }

        //Branch Admin Permission
        foreach(Permission::where('id','!=','1')->where('id','!=','5')->get() as $permission)
        {
            Role::where('slug','branch-admin')->first()->permissions()->save($permission);
        }

        // add default menus.
        Menu::truncate();
        Menu::insert([
            [
                'parent_id' => 0,
                'order' => 1,
                'title' => 'Dashboard',
                'icon' => 'fa-dashboard',
                'uri' => '/',
            ],
            [
                'parent_id' => 0,
                'order' => 2,
                'title' => 'Masters',
                'icon' => 'fa-bars',
                'uri' => '',
            ],
            [
                'parent_id' => 0,
                'order' => 26,
                'title' => 'Inquiries',
                'icon' => 'fa-list-alt',
                'uri' => '/inquiries',
            ],
            [
                'parent_id' => 0,
                'order' => 27,
                'title' => 'Enroll Students',
                'icon' => 'fa-graduation-cap',
                'uri' => '/students',
            ],
            [
                'parent_id' => 33,
                'order' => 29,
                'title' => 'Fees',
                'icon' => 'fa-rupee',
                'uri' => '/fees',
            ],
            [
                'parent_id' => 0,
                'order' => 31,
                'title' => 'Membership',
                'icon' => 'fa-credit-card',
                'uri' => '/membership',
            ],
            [
                'parent_id' => 0,
                'order' => 20,
                'title' => 'Admin',
                'icon' => 'fa-tasks',
                'uri' => '',
            ],
            [
                'parent_id' => 2,
                'order' => 3,
                'title' => 'Cities',
                'icon' => 'fa-building',
                'uri' => '/cities',
            ],
            [
                'parent_id' => 2,
                'order' => 4,
                'title' => 'Sources',
                'icon' => 'fa-circle',
                'uri' => '/sources',
            ],
            [
                'parent_id' => 2,
                'order' => 5,
                'title' => 'Branches',
                'icon' => 'fa-code-fork',
                'uri' => '/branches',
            ],
            [
                'parent_id' => 2,
                'order' => 6,
                'title' => 'Activities',
                'icon' => 'fa-book',
                'uri' => '/courses',
            ],

            [
                'parent_id' => 2,
                'order' => 9,
                'title' => 'Batches',
                'icon' => 'fa-users',
                'uri' => '/batches',
            ],
            [
                'parent_id' => 2,
                'order' => 10,
                'title' => 'Schools',
                'icon' => 'fa-university',
                'uri' => '/schools',
            ],
            [
                'parent_id' => 2,
                'order' => 11,
                'title' => 'Profession',
                'icon' => 'fa-suitcase',
                'uri' => '/professions',
            ],
            [
                'parent_id' => 2,
                'order' => 12,
                'title' => 'Parents',
                'icon' => 'fa-circle',
                'uri' => '/parents',
            ],
            [
                'parent_id' => 2,
                'order' => 13,
                'title' => 'Banks',
                'icon' => 'fa-university',
                'uri' => '/banks',
            ],
            [
                'parent_id' => 2,
                'order' => 14,
                'title' => 'Interest Level',
                'icon' => 'fa-level-up',
                'uri' => '/interestLevel',
            ],
            [
                'parent_id' => 2,
                'order' => 15,
                'title' => 'Follow Up Status',
                'icon' => 'fa-check-square-o',
                'uri' => '/followUpStatus',
            ],
            [
                'parent_id' => 2,
                'order' => 16,
                'title' => 'Follow Up Action',
                'icon' => 'fa-share-square',
                'uri' => '/followUpAction',
            ],
            [
                'parent_id' => 7,
                'order' => 21,
                'title' => 'Users',
                'icon' => 'fa-users',
                'uri' => 'auth/users',
            ],
            [
                'parent_id' => 7,
                'order' => 22,
                'title' => 'Roles',
                'icon' => 'fa-user',
                'uri' => 'auth/roles',
            ],
            [
                'parent_id' => 7,
                'order' => 23,
                'title' => 'Permission',
                'icon' => 'fa-ban',
                'uri' => 'auth/permissions',
            ],
            [
                'parent_id' => 7,
                'order' => 24,
                'title' => 'Menu',
                'icon' => 'fa-bars',
                'uri' => 'auth/menu',
            ],
            [
                'parent_id' => 7,
                'order' => 25,
                'title' => 'Operation log',
                'icon' => 'fa-history',
                'uri' => 'auth/logs',
            ],
            [
                'parent_id' => 2,
                'order' => 19,
                'title' => 'Extra Fee',
                'icon' => 'fa-rupee',
                'uri' => 'extra-fees',
            ],
            [
                'parent_id' => 11,
                'order' => 7,
                'title' => 'Add Activity',
                'icon' => 'fa-plus',
                'uri' => '/activities',
            ],
            [
                'parent_id' => 11,
                'order' => 8,
                'title' => 'Activity Used By Branch',
                'icon' => 'fa-check-square-o',
                'uri' => '/courses',
            ],
            [
                'parent_id' => 0,
                'order' => 32,
                'title' => 'News',
                'icon' => 'fa-newspaper-o',
                'uri' => '/news',
            ],
            [
                'parent_id' => 0,
                'order' => 33,
                'title' => 'Events',
                'icon' => 'fa-calendar-check-o',
                'uri' => '/events',
            ],
            [
                'parent_id' => 2,
                'order' => 17,
                'title' => 'Branch Extra Fee',
                'icon' => 'fa-money',
                'uri' => '/branch-extra-fee',
            ],
            [
                'parent_id' => 33,
                'order' => 30,
                'title' => 'Take Extra Fee',
                'icon' => 'fa-credit-card-alt',
                'uri' => '/student-extra-fee-payment',
            ],
            [
                'parent_id' => 2,
                'order' => 18,
                'title' => 'School Payment',
                'icon' => 'fa-bank',
                'uri' => '/payments',
            ],
            [
                'parent_id' => 0,
                'order' => 28,
                'title' => 'Fee Management',
                'icon' => 'fa-rupee',
                'uri' => '#',
            ],
        ]);

        // add role to menu.
        //Super Admin Menu Role
        foreach (Menu::all() as $menu)
        {
            $menu->roles()->save(Role::first());
        }
        //Admin Menu Role
        foreach (Menu::whereNotIn('id',[7,20,21,22,23,24])->get() as $menu)
        {
            $menu->roles()->save(Role::where('slug','administrator')->first());
        }

        //bran-admin Menu Role
        foreach (Menu::whereIn('id',[1,2,3,4,5,6,11,12,13,15,25,27,28,29,30,31,32,33])->get() as $menu)
        {
            $menu->roles()->save(Role::where('slug','branch-admin')->first());
        }
    }
}
