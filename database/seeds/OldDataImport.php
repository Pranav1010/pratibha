<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\AppLogin;
use App\AppUser;
use App\Student;
use App\Activity;
use App\Course;
use App\Batch;
use App\CourseStudent;
use App\Fee;
use App\FeeDetail;

class OldDataImport extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $database = DB::connection('mysql');
        $students = $database->table('student')->where('id',1)->get();
        $i=0;
        foreach ($students as $student)
        {
            $mobileNo = substr($student->contact_no,0,10);

            $userExist = AppUser::where('name','LIKE',$student->name)->first();
            if(isset($userExist))
            {
                echo "\nUser Exist :".$userExist->name.'('.$student->name."[Old Data])\n";
                continue;
            }

            $userExist = AppUser::where('contact_no',$mobileNo)->first();
            if(isset($userExist))
            {
                if($userExist->student->bdate == $student->dob)
                {
                    echo "\nUser Mobile:".$userExist->name.'('.$student->name."[Old Data]) \n";
                    continue;
                }
            }
            $appLogin = AppLogin::where('username',$mobileNo)->first();
            if(!isset($appLogin))
            {
                $appLogin = new AppLogin();
                if($mobileNo == "NA")
                {
                    $date = date_create($student->dob);
                    $appLogin->username = date_format($date,"dmY");
                }
                else {
                    $appLogin->username = $mobileNo;
                }
                $appLogin->password = bcrypt(config('app.default_password'));
                $appLogin->save();
            }
            $appUser = new AppUser();
            $appUser->app_login_id = $appLogin->id;
            $appUser->name = $student->name;
            $appUser->contact_no = $mobileNo;
            $appUser->address = $student->address;
            $appUser->role_id = 3;
            $appUser->relationship_id = 0;
            $appUser->fee_status = 3;
            $appUser->save();

            $newStudent = new Student();
            $newStudent->user_id = $appUser->id;
            $newStudent->gender = $student->gender;
            $newStudent->bdate = $student->dob;
            $newStudent->fee_status = 3;
            $newStudent->save();

            $appUser->relationship_id = $newStudent->id;
            $appUser->save();

            $activities = $database->table('student')->select('activity.activity_name','student_activity.from_date','student_activity.to_date','student_activity.fee','student_activity.entdate')->join('student_activity','student_activity.stud_id','=','student.stud_id')->join('stud_act_ids','stud_act_ids.stud_act_id','=','student_activity.stud_act_id')->join('activity','activity.act_id','=','stud_act_ids.act_id')->where('student.stud_id',$student->stud_id)->get();

            foreach ($activities as $activity)
            {
                $newActivity = Activity::where('name','LIKE','%'.$activity->activity_name.'%')->first();
                if(isset($newActivity))
                {
                   // echo "---------- \n Activity Found \n";
                    $course = Course::where('activity_id',$newActivity->id)->where('branch_id',1)->first();
                    $batch = Batch::where('course_id',$course->id)->where('name','LIKE','%EVENING%')->first();
                    if(!isset($batch))
                    {
                        $batch = Batch::where('course_id',$course->id)->where('name','LIKE','%MORNING%')->first();
                    }
                    $courseStudent = CourseStudent::where('course_id',$course->id)->where('student_id',$appUser->id)->first();
                    if(!isset($courseStudent))
                    {
                        $courseStudent = new CourseStudent();
                        $courseStudent->course_id = $course->id;
                        $courseStudent->student_id = $appUser->id;
                        $courseStudent->start_date = date('m-Y',strtotime($activity->from_date));
                        $courseStudent->end_date = date('m-Y',strtotime($activity->to_date));
                        $courseStudent->batch_id = $batch->id;
                        if(date('m-Y',strtotime($activity->to_date)) == "08-2018")
                        {
                            $courseStudent->status = 0;
                        }
                        else{
                            $courseStudent->status = 2;
                        }
                        $courseStudent->fee_type = 1;
                        $courseStudent->save();
                    }
                    else {
                        $courseStudent->end_date = date('m-Y',strtotime($activity->to_date));
                        if(date('m-Y',strtotime($activity->to_date)) == "08-2018")
                        {
                            $courseStudent->status = 0;
                        }
                        else{
                            $courseStudent->status = 2;
                        }
                        $courseStudent->save();
                    }

                    $fee = new Fee();
                    $fee->user_id = $appUser->id;
                    $fee->payment_date = $activity->entdate;
                    $fee->net_amount = $activity->fee;
                    $fee->lump_sum_amount = 0;
                    $fee->payment_mode = 0;
                    $fee->save();

                    $feeDetail = new FeeDetail();
                    $feeDetail->fee_id = $fee->id;
                    $feeDetail->course_id = $course->id;
                    $feeDetail->discount = 0;
                    $feeDetail->paid_amount = $activity->fee;
                    $feeDetail->save();
                }
                else {

                   // echo "---------- \n Activity Not Found :".$activity->activity_name."\n";
                    $activityId = null;
                    if($activity->activity_name == 'GARBA')
                    {
                        $createActivity = new Activity();
                        $createActivity->name = "GARBA";
                        $createActivity->save();

                        $course = new Course();
                        $course->activity_id = $createActivity->id;
                        $course->branch_id = 1;
                        $course->save();

                        $batch = new Batch();
                        $batch->name = 'EVENING';
                        $batch->course_id = $course->id;
                        $batch->save();

                        $activityId = $createActivity->id;
                    }
                    elseif($activity->activity_name == "HANDWRITING")
                    {
                        $activityId = Activity::where('name','HANDWRITTING IMPROVEMENT')->first()->id;
                    }
                    elseif($activity->activity_name == "DRAWING & PAINTING")
                    {
                        $activityId = Activity::where('name','PAINTING')->first()->id;
                    }
                    elseif($activity->activity_name == "CLASSICAL DANCE")
                    {
                        $activityId = Activity::where('name','DANCE')->first()->id;
                    }

                    $course = Course::where('activity_id',$activityId)->where('branch_id',1)->first();
                    $batch = Batch::where('course_id',$course->id)->where('name','LIKE','%EVENING%')->first();
                    if(!isset($batch))
                    {
                        $batch = Batch::where('course_id',$course->id)->where('name','LIKE','%MORNING%')->first();
                    }
                    $courseStudent = CourseStudent::where('course_id',$course->id)->where('student_id',$appUser->id)->first();
                    if(!isset($courseStudent))
                    {
                        $courseStudent = new CourseStudent();
                        $courseStudent->course_id = $course->id;
                        $courseStudent->student_id = $appUser->id;
                        $courseStudent->start_date = date('m-Y',strtotime($activity->from_date));;
                        $courseStudent->end_date = date('m-Y',strtotime($activity->to_date));
                        $courseStudent->batch_id = $batch->id;
                        if(date('m-Y',strtotime($activity->to_date)) == "08-2018")
                        {
                            $courseStudent->status = 0;
                        }
                        else{
                            $courseStudent->status = 2;
                        }
                        $courseStudent->fee_type = 1;
                        $courseStudent->save();
                    }
                    else {
                        $courseStudent->end_date = date('m-Y',strtotime($activity->to_date));
                        if(date('m-Y',strtotime($activity->to_date)) == "08-2018")
                        {
                            $courseStudent->status = 0;
                        }
                        else{
                            $courseStudent->status = 2;
                        }
                        $courseStudent->save();
                    }

                    $fee = new Fee();
                    $fee->user_id = $appUser->id;
                    $fee->payment_date = $activity->entdate;
                    $fee->net_amount = $activity->fee;
                    $fee->lump_sum_amount = 0;
                    $fee->payment_mode = 0;
                    $fee->save();

                    $feeDetail = new FeeDetail();
                    $feeDetail->fee_id = $fee->id;
                    $feeDetail->course_id = $course->id;
                    $feeDetail->discount = 0;
                    $feeDetail->paid_amount = $activity->fee;
                    $feeDetail->save();
                }
            }

            echo "\nCount:".$i." ID:".$student->stud_id.' = '.substr($student->contact_no,0,10)."\n";
            $i++;
        }
    }
}
