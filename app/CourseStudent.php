<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseStudent extends Model
{
    protected $guarded = [];
    public function course()
    {
    	return $this->belongsTo(Course::class,'course_id');
    }

    public function batch()
    {
    	return $this->belongsTo(Batch::class,'batch_id');
    }

    public function status()
    {
    	return $this->hasMany(CourseStatus::class,'course_student_id');
    }

    public function fees()
    {
        return $this->hasOne(Fee::class,'user_id','student_id');
    }
}
