<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseExtraFee extends Model
{
    protected $fillable = ['course_id','extra_fee_id','fees'];
}
