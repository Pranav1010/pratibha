<?php

namespace App;

use DateInterval;
use DateTime;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $guarded = [];

    public function parent()
    {
        return $this->belongsTo(Parents::class, 'parent_id');
    }

    public function school()
    {
        return $this->belongsTo(School::class, 'school_id');
    }

    public function profession()
    {
        return $this->belongsTo(Profession::class, 'guardian_profession');
    }

    public function user()
    {
        return $this->belongsTo(AppUser::class, 'user_id');
    }

    public function courseStudents()
    {
        return $this->hasMany(CourseStudent::class, "student_id", "user_id");
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public static function getMembershipStatus($user_id)
    {
        $student = Student::where("user_id", $user_id)->first();

        $membership_status = $student->membership_status;
        $membership_date = $student->membership_date;

        if ($membership_date) {

            if ($membership_status) {

                $date1 = new DateTime($membership_date);
                $interval = new DateInterval('P1Y');
                $date1->add($interval);
                $date2 = new DateTime("now", new \DateTimeZone("Asia/Kolkata"));

                $diff = $date1->diff($date2)->format("%a");

                if ($diff > 30) {
                    return 0;
                }

                return $diff;

            } else {

                return -1;

            }

        } else {

            return -2;

        }
    }
}
