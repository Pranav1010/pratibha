<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


class Course extends Model
{
    protected static $MEMBERSHIP_ID = 1;

    protected static function boot()
    {
        parent::boot();

        if(request()->segment(2) != self::$MEMBERSHIP_ID && request()->segment(2) != "edit") {
            static::addGlobalScope('removeMembership', function (Builder $builder) {
                $builder->where('courses.id', '!=', self::$MEMBERSHIP_ID);
            });
        }
    }

    protected $fillable = [
        'activity_id', 'branch_id','category'
    ];

    public function batch()
    {
        return $this->hasMany(Batch::class, 'course_id');
    }

    public static function termType($type)
    {
        return $type ?  "Long Term" : "Short Term";
    }

    public function courseStudent(){
        return $this->hasMany(CourseStudent::class);
    }

    public function inquiryCourse(){
        return $this->hasMany(InquiryCourse::class);
    }

    public function courseFeeType()
    {
        return $this->hasMany(CourseFeeType::class);
    }

    public function courseExtraFee()
    {
        return $this->belongsTo(CourseExtraFee::class);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }
    public  function batches()
    {
        return $this->hasMany(Batch::class);
    }
}
