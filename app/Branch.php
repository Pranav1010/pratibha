<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed name
 * @property mixed address
 * @property mixed contact_no
 * @property mixed city_id
 */
class Branch extends Model
{
    protected $fillable = [
        'name', 'address', 'contact_no', 'city_id'
    ];

    public function city(){
       return $this->belongsTo(City::class,'city_id');
   }

    public function details(){
        return $this->belongsTo('App\Branch');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function students()
    {
        return $this->hasMany(Student::class,'branch_id');
    }
    public function courses(){
        return $this->hasMany(Course::class);
    }
}
