<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    /*
     * START : Routes to add master data from modal
     */

    $router->post('/cities/add-city','CityController@addCity');
    $router->post('/professions/add-profession','ProfessionController@addProfession');
    $router->post('/sources/add-source','InquirySourceController@addSource');
    $router->post('/interestLevel/add-interest-level','InterestLevelController@addInterestLevel');
    $router->post('/followUpStatus/add-followup-status','FollowUpStatusController@addFollowupStatus');
    $router->post('/followUpAction/add-followup-action','FollowUpActionController@addFollowupAction');
    $router->post('/schools/add-school','SchoolController@addSchool');
    $router->post('/parents/add-parent','ParentController@addParent');

    /*
     * END : Routes to add master data from modal
     */

    $router->get('/', 'HomeController@index');
    $router->get('/dashboard', 'HomeController@index');
    $router->get('/auth/setting', 'AuthController@getSetting');
    $router->post('/auth/logoupload', 'AuthController@logoupload');
    $router->resource('/auth/users', 'UserController');
    $router->resource('/cities', 'CityController');

    $router->resource('/activities', ActivityController::class);
    $router->resource('/courses', 'CourseController');
    $router->resource('/sources', 'InquirySourceController');
    $router->resource('/branches', 'BranchController');
    $router->resource('/interestLevel', 'InterestLevelController');
    $router->resource('/followUpAction', 'FollowUpActionController');
    $router->resource('/followUpStatus', 'FollowUpStatusController');
    $router->resource('/fees', 'FeeController');
    $router->resource('/banks', 'BankController');

    $router->get('/completed', 'InquiryController@completedInquiries');
    $router->get('/cancelled', 'InquiryController@cancelledInquiries');
    $router->get('/forwarded', 'InquiryController@forwardedInquiries');

    $router->resource('/followUp', 'FollowUpController');
    $router->resource('/inquiries', 'InquiryController');

    $router->resource('batches',BatchController::class);
    $router->resource('schools',SchoolController::class);
    $router->resource('parents',ParentController::class);
    $router->get('students/refund','StudentController@refund');
    $router->post('students/refund','StudentController@refundPostAction');
    $router->resource('students',StudentController::class);
    $router->resource('professions',ProfessionController::class);
    $router->get('student-detail/{id}','StudentController@studentDetail');
    $router->get('register/{id}','InquiryController@registerForm');
    $router->get('course-pause/{id}','StudentController@coursePause');
    $router->get('course-resume/{id}','StudentController@courseResume');
    $router->get('course-stop/{id}','StudentController@courseStop');
    $router->get('check-fees/{id}/{userId}','StudentController@checkFeesBeforeCompleteCourse');
    $router->get('course-history/{id}','StudentController@courseHistory');
    $router->post('students/{id}/add-course','StudentController@addCourseToStudent');
    $router->get('search-student','StudentController@searchStudent');

    $router->get('/membership', "MembershipController@index");
    $router->resource('extra-fees',ExtraFeeController::class);
    $router->get('students/{id}/fee-type','StudentController@changeFeeTypeView');
    $router->post('students/edit-fee-type','StudentController@editFeeType');
    $router->post('batches/add','BatchController@addBatch');
    $router->resource('students-school',SchoolStudentController::class);
    $router->get('students-school/import','StudentController@import');

    $router->get('/payments/refund/{school_id}','PaymentController@refundForm');
    $router->post('/payments/refund/{school_id}','PaymentController@refundStore');
	$router->resource('/payments',"PaymentController");

	$router->resource('/events',EventController::class);
    $router->resource('/news',NewsController::class);
    $router->get('media/gallery/{eventId}','MediaController@gallery');
    $router->resource('/media',MediaController::class);

	$router->resource('/branch-extra-fee',BranchExtraFeeController::class);
	$router->get('/student-extra-fee-payment/selectbox/{id}','StudentExtraFeePaymentController@selectBox');
	$router->resource('/student-extra-fee-payment',StudentExtraFeePaymentController::class);




});
