<?php

namespace App\Admin\Controllers;

use App\Activity;
use App\Branch;
use App\Course;
use App\CourseExtraFee;
use App\CourseFeeType;
use App\ExtraFee;
use App\FeeType;
use App\Http\Controllers\Controller;
use App\User;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use App\Admin\Extensions\Tools\GridSearch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Mockery\Exception;
use App\Admin\Extensions\Tools\ExcelExport;

class CourseController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["
            $.getScript('/js/course.js')
            $('.col-md-8 .box-header .btn-group.pull-right a.btn.btn-sm.btn-success').attr('href', '/courses');
        "]);
    }

    public function index()
    {
        return Admin::content(function (Content $content) {


            $content->header('Activities');


                $content->row(function (Row $row)
                {
                    $row->column(12, $this->grid());
                });

        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {
            $feeTypes = config('app.fee_type');
            if(!Admin::user()->isAdministrator() AND !isRole('super-administrator'))
            {
                $branch= Branch::where('user_id',Admin::user()->id)->get();
            }
            $content->header('Activity');

            $content->row(view('course.course', compact(['feeTypes','branch'])));
        });
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $feeTypes = config('app.fee_type');
            if(!Admin::user()->isAdministrator())
            {
                $branch= Branch::where('user_id',Admin::user()->id)->get();
            }
            $course = Course::find($id);
            $feeTypeIds = $course->courseFeeType->pluck('fees', 'fee_type_id')->toArray();
            $content->header('Activity');
            $content->row(view('course.editCourse', compact(['feeTypes','course', 'feeTypeIds', 'branch'])));

        });
    }

    protected function grid($id = null)
    {
        return Admin::grid(Course::class, function (Grid $grid) use ($id) {

            if (!Admin::user()->isAdministrator() AND !isRole('super-administrator'))
            {
                $branchId = Branch::where('user_id',Admin::user()->id)->first()->id;
                $grid->model()->where('branch_id',$branchId);
            }
            $grid->model()->orderBy('id','desc');

            $grid->column('activity.name', 'Activity');

            if (Admin::user()->isAdministrator() || isRole('super-administrator'))
            {
                $grid->column('branch.name', 'Branch Name');
            }

            $grid->disableRowSelector();
//
//            if(Admin::user()->isAdministrator() || isRole('super-administrator'))
//            {
//                $grid->disableCreateButton();
//            }

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableEdit();
                $actions->prepend('<a href="/courses/' . $actions->getKey() . '/edit"><i class="fa fa-edit"></i></a>');

            });
            $grid->paginate(10);

            if(Admin::user()->isAdministrator() || isRole('super-administrator'))
            {
                $excel_headers = ["Activity Name","Branch Name"];
                $excel_columns = ["activity.name","branch.name"];
            }
            else
            {
                $excel_headers = ["Activity Name"];
                $excel_columns = ["activity.name"];
            }
            $grid->exporter(new ExcelExport("Courses", $excel_headers, $excel_columns));

            $grid->filter(function ($filter){

                $filter->disableIdFilter();

                $filter->where(function($q){
                    $q->where('activity_id',$this->input);
                }, "Activity")->select(Activity::where('id','!=','1')->orderBy('name','ASC')->pluck("name", "id")->toArray());


                if(Admin::user()->isAdministrator() || isRole('super-administrator')) {
                    $filter->where(function ($q) {
                        $q->where('branch_id', $this->input);
                    }, "Branch")->select(Branch::pluck("name", "id")->toArray());
                }


            });
        });
    }

    protected function form()
    {
        return Admin::form(Course::class, function (Form $form) {

            $form->text('name')->rules('required');
            $form->html('<label></label>', ' Fees Types');

            $feeTypes = config('app.fee_type');
            foreach ($feeTypes as $key => $type) {
                $form->html('
                    <div class="col-sm-7 checkbox-col"><input type="checkbox" class="icheckbox_minimal-blue"> ' . $type['name'] . '</div><div class="col-sm-5 checkbox-col"><input type="text" class="form-control " placeholder="Fees"></div>');
            }

            $form->saving(function (Form $form) {
                $form->name = strtoupper($form->name);
            });

            $form->saved(function () {
                admin_toastr(trans('admin.save_succeeded'));
                return redirect("/courses");
            });
            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableListButton();
            });
            $form->setWidth(9, 2);
        });
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:courses,activity_id,NULL,id,branch_id,'.$request->branchId,
            'branchId' => 'required|unique:courses,branch_id,NULL,id,activity_id,'.$request->name
        ]);
        $error = [];
        if(isset($request->branchId))
        {
            foreach ($request->fees as $key => $fees) {
                if ((isset($fees['check']) && $fees['fees'] == null) || ($fees['fees'] != null && !isset($fees['check'])))
                    $error['fees_' . $key] = ['You must select checkbox and enter fees'];

            }
            if ($request->extra_fees && count($request->extra_fees) > 0) {
                foreach ($request->extra_fees as $key => $fees) {
                    if ((isset($fees['check']) && $fees['fees'] == null) || ($fees['fees'] != null && !isset($fees['check'])))
                        $error['extra_fees_' . $key] = ['You must select checkbox and enter fees'];

                }
            }
        }

        $error1 = $validator->getMessageBag()->toArray();
        if ($validator->fails() || count($error) > 0) {
            return response()->json(array_merge($error, $error1), 406);
        } else {
            DB::beginTransaction();

            try {

                if(!isset($request->branchId))
                {
                    $activity = new Activity();
                    $activity->name =  ucwords($request->name);
                    $activity->save();
                }
                if(isset($request->branchId))
                {
                    $courseData = Course::create(['activity_id' => $request->name,'branch_id' => $request->branchId,'category' => $request->category]);
                    $course = $courseData->id;
                }

                if (isset($course)) {
                    foreach ($request->fees as $key => $fees) {
                        if ((isset($fees['check']) && $fees['fees'] != null)) {
                            $courseFeeType = CourseFeeType::create(['course_id' => $course, 'fee_type_id' => $fees['id'], 'fees' => $fees['fees']]);
                        }
                    }
                    if ($request->extra_fees && count($request->extra_fees) > 0) {
                        foreach ($request->extra_fees as $key => $fees) {
                            if ((isset($fees['check']) && $fees['fees'] != null)) {
                                $courseExtraFee = CourseExtraFee::create(['course_id' => $course, 'extra_fee_id' => $fees['id'], 'fees' => $fees['fees']]);
                            }
                        }
                    }

                }
                DB::commit();

            } catch (Exception $e) {
                DB::rollback();
            }
            if(isset($courseData))
            {
                return json_encode(['id' => $courseData->id, 'name' => $courseData->activity->name]);
            }
            else
            {
                return json_encode(['id' => $activity->id, 'name' => $activity->activity->name]);
            }

        }

    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        $error = [];
        foreach ($request->fees as $key => $fees) {
            if ((isset($fees['check']) && $fees['fees'] == null) || ($fees['fees'] != null && !isset($fees['check'])))
                $error['fees_' . $key] = ['You must select checkbox and enter fees'];

        }


        $error1 = $validator->getMessageBag()->toArray();
        if ($validator->fails() || count($error) > 0) {
            return response()->json(array_merge($error, $error1), 406);
        } else {
            DB::beginTransaction();

            try {

                $course = Course::find($id);
                $course->update(['activity_id' => $request->name,'branch_id' => $request->branchId ,'category' => $request->category]);
                $course->courseFeeType()->delete();
                if ($course)
                {
                    foreach ($request->fees as $key => $fees) {
                        if ((isset($fees['check']) && $fees['fees'] != null)) {
                            $courseFeeType = CourseFeeType::create(['course_id' => $id, 'fee_type_id' => $fees['id'], 'fees' => $fees['fees']]);
                        }
                    }
                }
                DB::commit();

            } catch (Exception $e) {
                DB::rollback();
            }
            return response()->json(['success' => true], 200);
        }
    }
}
