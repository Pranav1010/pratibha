<?php

namespace App\Admin\Controllers;

use App\FollowUp;
use App\Http\Controllers\Controller;
use App\Inquiry;
use App\Branch;
use Carbon\Carbon;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Illuminate\Support\Facades\Input;
use App\Admin\Extensions\Tools\ExcelExport;

class HomeController extends Controller
{
    public function index()
    {
        return Admin::content(function (Content $content) {

            $userId = Admin::user()->id;

            $content->header("Dashboard");

            if(isRole("administrator") || isRole("counselor") || isRole("super-administrator")){
                $totalInquiries = Inquiry::count();
                $completedInquiries = Inquiry::where("status", "=", 1)->count();
                $cancelledInquiries = Inquiry::where("status", "=", 2)->count();
            } else {
                $branch = Branch::where('user_id',Admin::user()->id)->first();
                $totalInquiries = Inquiry::where('branch_id', $branch->id)->count();
                $completedInquiries = Inquiry::where("status", 1)->where('branch_id', $branch->id)->count();
                $cancelledInquiries = Inquiry::where("status", 2)->where('branch_id', $branch->id)->count();
            }

            $content->row(view('home', compact('totalInquiries', 'completedInquiries', 'cancelledInquiries')));
            $content->row($this->grid());

        });
    }

    /**
     * Make a grid builder.
     *
     * @return \Encore\Admin\Grid
     */
    protected function grid()
    {
        return Admin::grid(FollowUp::class, function (Grid $grid) {

            if(isRole('branch-admin'))
            {
                $branch = Branch::where("user_id",Admin::user()->id)->first();
                $grid->model()->whereHas('inquiry',function ($query) use ($branch){
                    $query->where('branch_id',$branch->id);
                });
            }

            $grid->column( "inquiry.name","Inquiry");
            $grid->column('interestLevel.name', "Interest Level");
            $grid->column('followUpStatus.name','Follow Up Status');
            $grid->column('followUpAction.name','Follow Up Action');
            $grid->next_follow_up('Next Follow Up Date')->display(function ($date){
                return Carbon::createFromFormat('Y-m-d H:i:s',$date)->format('d/m/Y h:i a');
            });;
            $grid->comments('Comments');

            $grid->column("Follow","Total Follow Ups")->display(function(){
                return Inquiry::getNumberOfFollowUps($this->id);
            })->sortable();
            $grid->created_at("Days Past")->display(function(){
                return Inquiry::getDaysFromStartDateOfInquiry($this->created_at);
            })->sortable();

            if(isRole("administrator") || isRole("counselor")) {
                $grid->actions(function ($actions) {
                    $actions->disableDelete();
                    $actions->disableEdit();
                    $actions->prepend('<a href="/followUp/' . $actions->row->id . '/edit?inquiry_id=' . $actions->row->inquiry_id . '"><i class="fa fa-edit"></i></a>');
                });
            } else{
                $grid->disableActions();
            }

            $grid->model()->whereHas("inquiry", function($q){
                $q->where("is_registered", 0);
            });

            $grid->disableRowSelector();
            $grid->disableCreateButton();

            $excel_headers = ["Inquiry","Interest Level","Follow Up Status","Follow Up Action","Next Follow Up Date","Comments","Total Follow Ups","Days Past"];
            $excel_columns = ['inquiry.name','interest_level_id','follow_up_status_id','follow_up_action_id','next_follow_up','comments','updated_at','created_at'];
            $grid->exporter(new ExcelExport("Dashboard", $excel_headers, $excel_columns));

            $grid->filter(function ($filter){

                $filter->disableIdFilter();
                $filter->like('inquiry.name','Inquiry');
                $filter->like('interestLevel.name','Interest Level');
                $filter->like('followUpStatus.name','FollowUp Status');
                $filter->like('followUpAction.name','FollowUp Action');
                $filter->like('next_follow_up');
                $filter->like('comments');
            });
        });
    }
}
