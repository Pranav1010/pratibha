<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Tools\GridSearch;
use App\Profession;
use Illuminate\Support\Facades\Input;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Admin\Extensions\Tools\ExcelExport;

class ProfessionController extends Controller
{
    use ModelForm;

    public function index()
    {
        return Admin::content(function (Content $content) {

            Admin::script(["
                $('form').attr('action', '/professions');
            "]);

            $content->header('Profession');

            $content->row(function (Row $row) {
                $row->column(4, $this->form());
                $row->column(8, $this->grid());
            });
        });
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Professions');

            $content->row(function (Row $row) use ($id) {
                $row->column(4, $this->form($id)->edit($id));
                $row->column(8, $this->grid($id));
            });
        });
    }

    public function addProfession(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validate->fails()) {

            $error['error'] = $validate->getMessageBag();
            return response()->json($error, 406);

        } else {

            $profession = Profession::updateOrCreate(
                ["name" => ucwords($request->name)]
            );

            return json_encode($profession);
        }
    }

    protected function grid($id = null)
    {
        return Admin::grid(Profession::class, function (Grid $grid) use($id) {

            $grid->disableCreateButton();

            $grid->model()->orderBy('name','ASC');
            $grid->name()->sortable();
            $grid->disableRowSelector();
            $grid->disableFilter();

            if(isset($id))
            {
                $grid->tools(function ($tools){
                $tools->append('<div class="btn-group pull-right" style="margin-right: 10px"><a href="/professions" class="btn btn-sm btn-success"><i class="fa fa-save"></i>&nbsp;&nbsp;New</a></div>');
             });
            }

            $grid->actions(function ($action){
                $action->disableEdit();
                $action->prepend('<a href="/professions/'.$action->getKey().'/edit" class="btn grid-row-edit"><i class="fa fa-pencil"></i></a>');
            });

            $excel_headers = ["Profession Name"];
            $excel_columns = ["name"];
            $grid->exporter(new ExcelExport("Professions", $excel_headers, $excel_columns));

            $grid->tools(function ($tools) {
                $tools->append(new GridSearch());
            });

            $value = Input::get('search');

            if (!empty($value)) {

                $q = $grid->model();

                $q->where('name', "like", "%{$value}%");

            }

        });
    }

    protected function form($id = null)
    {
        return Admin::form(Profession::class, function (Form $form) use ($id) {

            $form->text('name', 'Name')->rules("required|unique:professions,name,$id")->attribute('autofocus');
            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableListButton();
            });

            $form->saved(function () {
                admin_toastr(trans('admin.save_succeeded'));
                return redirect("/professions");
            });

            $form->setWidth(8, 3);

            $form->saving(function ($form){
                $form->name = ucwords($form->name);
            });

        });
    }
}
