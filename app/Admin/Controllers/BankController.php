<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Tools\GridSearch;
use App\Bank;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;
use Illuminate\Support\Facades\Input;
use App\Admin\Extensions\Tools\ExcelExport;

class BankController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            Admin::script(["
                $('form').attr('action', '/banks');
            "]);

            $content->header('Banks');

            $content->row(function(Row $row) {
                $row->column(4, $this->form());
                $row->column(8, $this->grid());
            });
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            Admin::script(["
                $('.col-md-8 .box-header .btn-group.pull-right a.btn.btn-sm.btn-success').attr('href', '/banks');
            "]);

            $content->header('Banks');

            $content->row(function(Row $row) use ($id){
                $row->column(4, $this->form()->edit($id));
                $row->column(8, $this->grid($id));
            });
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid($id = null)
    {
        return Admin::grid(Bank::class, function (Grid $grid) use($id) {
            $grid->model()->orderBy('name','ASC');
            $grid->name('Name')->sortable();

            $grid->model()->orderBy("name");

            $grid->disableRowSelector();
            $grid->disableFilter();

            $grid->actions(function ($actions) {

                $actions->disableEdit();
                $actions->prepend('<a href="/banks/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');

            });

            if(!isset($id)){
                $grid->disableCreateButton();
            }

            $excel_headers = ["Bank Name"];
            $excel_columns = ["name"];
            $grid->exporter(new ExcelExport("Branches", $excel_headers, $excel_columns));

            $grid->tools(function ($tools) {
                $tools->append(new GridSearch());
            });

            $value = Input::get('search');

            if (!empty($value)) {

                $q = $grid->model();

                $q->where('name', "like", "%{$value}%");

            }

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Bank::class, function (Form $form) {

            $form->text('name')->rules("required")->rules(function ($form){

                // If it is not an edit state, add field unique verification
                if (!$id = $form->model()->id) {
                    return 'required|unique:banks,name';
                }
                return 'required|unique:banks,name,'.$form->model()->id;
            })->attribute('autofocus');

            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableListButton();
            });

            $form->saved(function () {
                admin_toastr(trans('admin.save_succeeded'));
                return redirect("/banks");
            });

            $form->setWidth(8, 3);

            $form->saving(function ($form){
                $form->name = ucwords($form->name);
            });
        });
    }
}
