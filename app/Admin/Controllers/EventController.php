<?php

namespace App\Admin\Controllers;

use App\Event;

use App\Gallery;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use File;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Branch;

class EventController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["
          
            $('<link/>', {
               rel: 'stylesheet',
               type: 'text/css',
               href: '/js/summernote/summernote.css'
            }).appendTo('head');
        
            $.getScript('/js/event.js');
            $(document).ready(function() {
              
              $('#start').datetimepicker({ format: 'DD/MM/YYYY h:mm a'});
              $('#end').datetimepicker({ format: 'DD/MM/YYYY h:mm a'});
         
            });
        
        "]);

    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('Events');
            $content->row(function (Row $row){
                if(!Admin::user()->isAdministrator() AND !isRole('super-administrator'))
                {
                    $branch= Branch::where('user_id',Admin::user()->id)->first();
                }
                $row->column(5, view('event.createEvent',compact('branch')));
                $row->column(7, $this->grid());
            });
        });
    }

    public function show($id)
    {
        $event = Event::find($id);
        $event->start_date = Carbon::createFromFormat('Y-m-d H:i:s',$event->start_date)->format('d/m/Y h:i a');
        $event->end_date = Carbon::createFromFormat('Y-m-d H:i:s',$event->end_date)->format('d/m/Y h:i a');
        return view('event.modal.body',compact('event'));
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            $event = Event::find($id);
            $content->header('Events');
            $content->row(function (Row $row) use($event) {
                $row->column(5, view('event.editEvent',compact('event')));
                $row->column(7, $this->grid());
            });
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Event::class, function (Grid $grid)
        {
            if(!Admin::user()->isAdministrator() AND !isRole('super-administrator'))
            {
                $branch= Branch::where('user_id',Admin::user()->id)->first();
                $grid->model()->where('branch_id',$branch->id);
            }
            $grid->disableExport();
            $grid->disableFilter();
            $grid->disableRowSelector();
            $grid->disableCreateButton();

            $grid->title('Event Name');
            $grid->organizer('Organizer');
            $grid->start_date('Start Date')->display(function ($start_date){
                return Carbon::createFromFormat('Y-m-d H:i:s',$start_date)->format('d/m/Y h:i a');
            });
            $grid->end_date('End Date')->display(function ($end_date){
                return Carbon::createFromFormat('Y-m-d H:i:s',$end_date)->format('d/m/Y h:i a');
            });

            $grid->actions(function ($actions) {
                $actions->disableEdit();
                $actions->prepend('<a href="/media?event=' . $actions->row->id . '" class="btn btn-info" data-toggle="tooltip" title="Event Images"><i class="fa fa-file-image-o"></i></a>');
                $actions->prepend('<a href="/events/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');
                $actions->prepend('<a href="#" data-id="' . $actions->row->id . '" class="btn open-branch-view-modal" style="background-color: #01a4f8; border: #01a4f8;"><i class="fa fa-eye"></i></a>');
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Event::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function store(Request $request)
    {
        $arr = [];
        $error = [];
        DB::beginTransaction();

        $validate = Validator::make($request->all(), [
            'name' => 'required',
            'organizer' => 'required',
            'start_date' => ['required',
                function($attribute, $value, $fail)
                {
                    $eventTimestamp = strtotime(Carbon::createFromFormat('d/m/Y h:i a', $value)->toDateTimeString());
                    $currentTimestamp = strtotime('now');
                    if ($eventTimestamp < $currentTimestamp) {
                        return $fail('Please Enter Future Date.');
                    }
                }
            ],
            'end_date' => ['required',
                function($attribute, $value, $fail) use ($request)
                {
                    if(isset($request->start_date))
                    {
                        $eventTimestamp = strtotime(Carbon::createFromFormat('d/m/Y h:i a', $value)->toDateTimeString());
                        $startEventTimestamp = strtotime(Carbon::createFromFormat('d/m/Y h:i a', $request->start_date)->toDateTimeString());
                        if ($eventTimestamp < $startEventTimestamp) {
                            return $fail('Please Enter Valid Date And Time.');
                        }
                    }
                }
            ],
            'location' => 'required',
            'description' => 'required'
        ]);

        if ($validate->fails()) {
            $error['event_details'] = $validate->getMessageBag();
        } else {
            $event = new Event();
            $event->title = $request->name;
            $event->organizer = $request->organizer;
            $event->start_date = Carbon::createFromFormat('d/m/Y h:i a', $request->start_date)->toDateTimeString();
            $event->end_date = Carbon::createFromFormat('d/m/Y h:i a', $request->end_date)->toDateTimeString();
            $event->location = $request->location;
            $event->description = $request->description;
            $event->branch_id = $request->branch_id;
            $event->save();

            $event_id = $event->id;


        }

        if (isset($request->videos))
        {

            foreach ($request->videos as $key => $video)
            {
                if (isset($event_id) && !empty($video))
                {
                    $gallary = new Gallery();
                    $gallary->event_id = $event_id;
                    $gallary->type = 'video';
                    $gallary->path = $video;
                    $gallary->save();
                }
            }
        }

        //Set response
        if (count($error) > 0) {
            DB::rollback();
            return response()->json($error, 500);

        } else {
            DB::commit();
            return response()->json("success", 200);
        }
    }

    public function update(Request $request)
    {
        $arr = [];
        $error = [];
        DB::beginTransaction();

        $validate = Validator::make($request->all(), [
            'name' => 'required',
            'organizer' => 'required',
            'start_date' => 'required',
            'end_date' => ['required',
                function($attribute, $value, $fail) use ($request)
                {
                    if(isset($request->start_date))
                    {
                        $eventTimestamp = strtotime(Carbon::createFromFormat('d/m/Y h:i a', $value)->toDateTimeString());
                        $startEventTimestamp = strtotime(Carbon::createFromFormat('d/m/Y h:i a', $request->start_date)->toDateTimeString());
                        if ($eventTimestamp < $startEventTimestamp) {
                            return $fail('Please Enter Valid Date And Time.');
                        }
                    }
                }
            ],
            'location' => 'required',
            'description' => 'required'
        ]);

        if ($validate->fails()) {
            $error['event_details'] = $validate->getMessageBag();
        } else {
            $event = Event::find($request->eventId);
            $event->title = $request->name;
            $event->organizer = $request->organizer;
            $event->start_date = Carbon::createFromFormat('d/m/Y h:i a', $request->start_date)->toDateTimeString();
            $event->end_date = Carbon::createFromFormat('d/m/Y h:i a', $request->end_date)->toDateTimeString();
            $event->location = $request->location;
            $event->description = $request->description;
            $event->save();

            $event_id = $event->id;
        }

        if (isset($request->videos) && isset($event))
        {
            foreach ($event->gallery as $file)
            {
                if($file->type == 'video')
                {
                    $file->delete();
                }
            }

            foreach ($request->videos as $key => $video)
            {
                if (isset($event_id) && !empty($video))
                {
                    $gallery = new Gallery();
                    $gallery->event_id = $event_id;
                    $gallery->type = 'video';
                    $gallery->path = $video;
                    $gallery->save();
                }
            }
        }

        //Set response
        if (count($error) > 0) {
            DB::rollback();
            return response()->json($error, 500);

        } else {
            DB::commit();
            return response()->json("success", 200);
        }

    }
}
