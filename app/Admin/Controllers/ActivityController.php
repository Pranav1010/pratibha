<?php

namespace App\Admin\Controllers;

use App\Activity;
use App\Admin\Extensions\Tools\GridSearch;
use Illuminate\Support\Facades\Input;

use App\Admin\Extensions\Tools\ExcelExport;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;


class ActivityController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            Admin::script(["
                $('form').attr('action', '/activities');
            "]);

            $content->header('Activities');

            $content->row(function(Row $row) {
                $row->column(4, $this->form("Add"));
                $row->column(8, $this->grid());
            });
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            Admin::script(["
                $('.col-md-8 .box-header .btn-group.pull-right a.btn.btn-sm.btn-success').attr('href', '/activities');
            "]);

            $content->header('Activity');

            $content->row(function(Row $row) use ($id){
                $row->column(4, $this->form()->edit($id));
                $row->column(8, $this->grid());
            });
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Activity::class, function (Grid $grid)  {

            $grid->model()->orderBy("name","ASC");

            $grid->name()->sortable();


            $grid->disableRowSelector();
            $grid->disableCreateButton();
            $grid->disableFilter();

            $grid->actions(function ($actions) {

                $actions->disableEdit();
                $actions->prepend('<a href="/activities/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');

            });

            // Search Filter -----
            $grid->paginate(10);

            $excel_headers = ["Activity Name"];
            $excel_columns = ["name"];
            $grid->exporter(new ExcelExport("Activities", $excel_headers, $excel_columns));

            $grid->tools(function ($tools) {
                $tools->append(new GridSearch());
            });

            $value = Input::get('search');

            if (!empty($value)) {

                $q = $grid->model();

                $q->where('name', "like", "%{$value}%");

            }
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($title = null)
    {
        return Admin::form(Activity::class, function (Form $form) use($title) {

            if(isset($title))
            {
                $form->setTitle($title);
            }

            $form->text("name")->rules(function ($form){

                // If it is not an edit state, add field unique verification
                if (!$id = $form->model()->id) {
                    return 'required|unique:activities,name';
                }
                return 'required|unique:activities,name,'.$form->model()->id;
            })->attribute('autofocus');

            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableListButton();
            });

            $form->setWidth(8, 3);

            $form->saved(function () {
                admin_toastr(trans('admin.save_succeeded'));
                return redirect("/activities");
            });

            $form->saving(function ($form){
                $form->name = ucwords($form->name);
            });
        });
    }
}
