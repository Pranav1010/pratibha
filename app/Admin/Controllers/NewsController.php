<?php

namespace App\Admin\Controllers;

use App\News;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Branch;
class NewsController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["
            $(document).ready(function() {
                
              $('#summernote').summernote({height: 250});
              $('#date').datetimepicker({ format: 'DD/MM/YYYY h:mm a'});
              $.getScript('/js/news.js');
            });
        "]);

        Admin::css(asset('/js/summernote/summernote.css'));
        Admin::js(asset('/js/summernote/summernote.js'));
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {

        return Admin::content(function (Content $content) {
            $content->header('News');
            $content->row(function (Row $row) {
                if(!Admin::user()->isAdministrator() AND !isRole('super-administrator'))
                {
                    $branch= Branch::where('user_id',Admin::user()->id)->first();
                }
                $row->column(7, view('news.createNews',compact('branch')));
                $row->column(5, $this->grid());
            });
        });
    }

    public function show($id)
    {
        $news = News::find($id);
        $news->date = Carbon::createFromFormat('Y-m-d H:i:s',$news->date)->format('d/m/Y h:i a');
        return view('news.modal.body',compact('news'));
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {
        $news = News::find($id);
        $content->header('News');
        $content->row(function (Row $row) use($news) {
            $row->column(7, view('news.editNews',compact('news')));
            $row->column(5, $this->grid());
        });
    });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(News::class, function (Grid $grid) {
            $grid->model()->orderBy('id','DESC');
            $grid->disableExport();
            $grid->disableFilter();
            $grid->disableRowSelector();
            $grid->disableCreateButton();

            if(!Admin::user()->isAdministrator() AND !isRole('super-administrator'))
            {
                $branch= Branch::where('user_id',Admin::user()->id)->first();
                $grid->model()->where('branch_id',$branch->id);
            }

            $grid->title('Title');
            $grid->author('Author');
            $grid->date('Date')->display(function ($date){
                return Carbon::createFromFormat('Y-m-d H:i:s',$date)->format('d/m/Y h:i a');
            })->sortable();


            $grid->actions(function ($actions) {
                $actions->disableEdit();
                $actions->prepend('<a href="/news/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');
                $actions->prepend('<a href="#" data-id="' . $actions->row->id . '" class="btn open-branch-view-modal" style="background-color: #01a4f8; border: #01a4f8;"><i class="fa fa-eye"></i></a>');
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(News::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
    public function store(Request $request)
    {
        $arr = [];
        $error = [];
        DB::beginTransaction();

        $validate = Validator::make($request->all(), [
            'title' => 'required',
            'author' => 'required',
            'date' => 'required',
            'description' => 'required'
        ]);

        if ($validate->fails()) {
            $error['news_details'] = $validate->getMessageBag();
        }
        else
        {
            $news = new News();
            $news->title = $request->title;
            $news->author = $request->author;
            $news->date = Carbon::createFromFormat('d/m/Y h:i a', $request->date)->toDateTimeString();
            $news->description = $request->description;
            $news->branch_id = $request->branch_id;
            $news->save();
        }

        //Set response
        if (count($error) > 0) {
            DB::rollback();
            return response()->json($error, 500);

        } else {
            DB::commit();
            return response()->json("success", 200);
        }
    }

    public function update(Request $request)
    {
        $arr = [];
        $error = [];
        DB::beginTransaction();

        $validate = Validator::make($request->all(), [
            'title' => 'required',
            'author' => 'required',
            'date' => 'required',
            'description' => 'required'
        ]);

        if ($validate->fails()) {
            $error['news_details'] = $validate->getMessageBag();
        }
        else
        {
            $news = News::find($request->newsId);
            $news->title = $request->title;
            $news->author = $request->author;
            $news->date = Carbon::createFromFormat('d/m/Y h:i a', $request->date)->toDateTimeString();
            $news->description = $request->description;
            $news->save();
        }

        //Set response
        if (count($error) > 0) {
            DB::rollback();
            return response()->json($error, 500);

        } else {
            DB::commit();
            return response()->json("success", 200);
        }
    }
}
