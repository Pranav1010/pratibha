<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Tools\FeeSearchButtons;
use App\Admin\Extensions\Tools\GridSearch;
use App\AppUser;
use App\Bank;
use App\CourseFeeType;
use App\CourseStudent;
use App\ExtraFee;
use App\ExtraFeeDetails;
use App\Fee;
use App\Branch;

use App\FeeDetail;
use App\PendingExtraFees;
use App\PendingFee;
use App\Student;
use App\StudentDiscount;
use App\User;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use DateTime;
use DateTimeZone;
use App\Admin\Extensions\Tools\ExcelExport;

class FeeController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["       
            $('.col-md-8 .box-header .btn-group.pull-right a').attr('href', '/fees');
            $('.payment_date').datepicker({'format':'dd-mm-yyyy', 'autoclose': true, 'todayHighlight': true});
            $.getScript('/js/fees.js');
            
            $('.form-history-back').on('click', function (event) {
                event.preventDefault();
                history.back(1);
            });
        "]);
    }

    public function index()
    {
        return Admin::content(function (Content $content) {

            Admin::script(["
                $('form').attr('action', '/fees');
            "]);

            $content->header('Fees');

            $paidAmount = Fee::paidAmount();
            $refundAmount = Fee::refundAmount();
            $totalFee = Fee::totalAmount();
            $totalDiscount = Fee::totalDiscount();
            $pendingAmount = Fee::pendingAmount();
            $totalAdvance = Fee::totalAdvance();

            $content->row(view("fees.infoboxes", compact("paidAmount",'refundAmount', "totalFee", "totalDiscount", "pendingAmount", "totalAdvance")));
            $content->row($this->grid());
            $content->row(view("fees.modal"));

        });
    }

    public function show($user_id)
    {
        $user = AppUser::where("id", $user_id)->first();

        $totalFees = Fee::totalAmount($user->id);
        $totalPendingFees = Fee::pendingAmount($user->id);
        $totalPaidFees = Fee::paidAmount($user->id);
        $totalRefund = Fee::refundAmount($user->id);
        $totalDiscount = Fee::totalDiscount($user->id);

        //dd($user,$totalFees,$totalPendingFees,$totalPaidFees,$totalDiscount);

        return view('fees.modal-body', compact('user', 'totalDiscount','totalRefund', 'totalPaidFees', 'totalPendingFees', 'totalFees'));
    }

    public function edit(Fee $fees)
    {
        return Admin::content(function (Content $content) use ($fees) {

            $content->header('Fees');

            $content->row(view('fees.editPayment', compact('fees')));

        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Take Fees');

            $user_id = Input::get("student_id");
            $user = AppUser::where("id", $user_id)->first();

            $pendingFees = PendingFee::where("user_id", $user_id)->groupBy("course_id")->orderBy('id')->get();

            $currentDate = new \DateTime("now", new \DateTimeZone("Asia/Kolkata"));
            $currentDate = $currentDate->format("d-m-Y");

            $banks = Bank::orderBy("name")->get();

            $content->row(view('fees.createPayment', compact("pendingFees", "user", "currentDate", "banks")));
            $content->row(view("fees.modal"));
        });
    }

    public function store(Request $request)
    {


        $lump_sum_amount = isset($request->lump_sum_amount) ? $request->lump_sum_amount : null;

        $rules = array();
        $messages = array();
        $rules["payment_date"] = "required";

        $flag = 0;
        $courseFlag = 0;
        //dd($request->all());
        if (isset($request->course)) {
            foreach ($request->course as $course) {
                if (isset($course['course_id'])) {
                    $courseFlag = 1;
                }
                if (isset($course['paid_amount'])) {
                    $flag = 1;
                }
                if (isset($course['extra'])) {
                    foreach ($course['extra'] as $extra) {
                        if (isset($extra['paid_amount'])) {
                            $flag = 1;
                        }
                    }
                }
            }


            if (!$courseFlag) {
                $rules["courses"] = "required";
                $messages["courses.required"] = "You must select at least one course.";
            } else {
                if (!$flag && $lump_sum_amount == null) {
                    $rules["lump_sum_amount"] = "required";
                } else if (!$flag && $lump_sum_amount != null) {
                    $netAmount = (int)$request->net_amount;
                    if ($request['lump_sum_amount'] < $netAmount) {
                        $rules['lump_sum_amount_min'] = "required";
                        $messages['lump_sum_amount_min.required'] = "Lumpsum amount must be greater then total paid amount";

                    }
                }
            }
        }

        if ($request->payment_mode == 1) {
            $rules["bank_name"] = "required";
            $rules["cheque_number"] = ['required', 'digits:6'];
        }
        //dd($rules,$messages);
        $request->validate($rules, $messages);

        DB::beginTransaction();

        try {

            $fee = new Fee();

            $fee->user_id = $request->user_id;
            $fee->payment_date = date("Y-m-d", strtotime($request->payment_date));
            $fee->net_amount = $request->net_amount;
            $fee->lump_sum_amount = $lump_sum_amount == null ? 0 : $lump_sum_amount;
            $fee->payment_mode = $request->payment_mode;

            if ($request->payment_mode == 1) {
                $fee->cheque_number = $request->cheque_number;
                $fee->bank_id = $request->bank_name;
            }

            $fee->remarks = $request->remarks;

            $fee->save();

            $i = 0;
            $reCalculatedPendingFee = array();
            $calculatedPendingFeeExtra = [];
            $currentMonth = date("m");

            if ($lump_sum_amount > 0) {
                $temp_amount = (int)$lump_sum_amount;
                $pendingFees = PendingFee::where("user_id", $request->user_id)->get();
                foreach ($pendingFees as $pendingFee) {

                    /*if ($temp_amount > 0) {

                        $pendingExtraFees = PendingExtraFees::where("user_id", $request->user_id)->where('course_id', $pendingFee->course_id)->get();
                        $k = 0;
                        foreach ($pendingExtraFees as $pendingExtra) {
                            $discount = optional($request->course[$i]['extra'][$k]['discount']) != null ? (int)$request->course[$i]['extra'][$k]['discount'] : 0;
                            if ($temp_amount <= $pendingExtra->fees - $discount) {
                                if ($temp_amount == $pendingExtra->fees - $discount) {
                                    $calculatedPendingFeeExtra[] = $pendingExtra->fees - $discount;
                                    $pendingExtra->delete();
                                } else {
                                    $pendingExtra->fees = $pendingExtra->fees - ($temp_amount + $discount);
                                    $pendingExtra->total_discount = $pendingExtra->total_discount + $discount;
                                    $calculatedPendingFeeExtra[] = $temp_amount;
                                    $pendingExtra->save();
                                }
                                $i++;
                                break;
                            }

                            $temp_amount = ($temp_amount + $discount) - $pendingExtra->fees;
                            $calculatedPendingFeeExtra[] = $pendingExtra->fees - $discount;
                            $pendingExtra->delete();

                            $k++;
                        }

                    } */

                    $discount = optional($request->course[$i]['discount']) != null ? (int)$request->course[$i]['discount'] : 0;

                    /*if ($temp_amount <= (int)$request->course[$i]['pending_amount'] - $discount) {
                        if ($i == 1)
                            if ($temp_amount == ((int)$request->course[$i]['pending_amount'] - $discount)) {
                                $reCalculatedPendingFee[] = $request->course[$i]['pending_amount'] - $discount;
                                $pendingFee->delete();
                            } else {
                                $pendingFee->fees = (int)$request->course[$i]['pending_amount'] - ($temp_amount + $discount);
                                $pendingFee->month = (int)$currentMonth;
                                $pendingFee->fees_type = (isset($request->course[$i]['fee_type'])) ? $request->course[$i]['fee_type'] : $pendingFee->fees_type;
                                $reCalculatedPendingFee[] = $temp_amount;
                                $pendingFee->total_discount = $pendingFee->total_discount + $discount;
                                $pendingFee->save();
                            }
                        $i++;
                        break;
                    }
                    */

                    $temp_amount = ($temp_amount + $discount) - (int)$request->course[$i]['pending_amount'];
                    $pendingFee->delete();
                    $reCalculatedPendingFee[] = (int)$request->course[$i]['pending_amount'] - $discount;
                    if ($pendingFee->course_id == 1) {
                        $student = Student::where("user_id", $request->user_id)->first();
                        $student->membership_date = date("Y-m-d");
                        $student->membership_status = 1;
                        $student->save();
                    }
                    $i++;
                }
            }

            $j = 0;
            foreach ($request->course as $course) {
                if (isset($course['course_id'])) {
                    $courseStudent = CourseStudent::where('course_id', $course['course_id'])->where('student_id', $request->user_id)->first();

                    if ($courseStudent && $courseStudent->fee_type == NULL && isset($course['fee_type'])) {
                        $courseStudent->fee_type = $course['fee_type'];
                        $courseStudent->save();
                    }
                    $fee_detail = new FeeDetail();
                    $fee_detail->fee_id = $fee->id;
                    $fee_detail->course_id = $course['course_id'];
                    $fee_detail->discount = $course['discount'] == null ? 0 : (int)$course['discount'];
                    if (isset($course['paid_amount']) && $course['paid_amount'] != null) {
                        $fee_detail->paid_amount = (int)$course['paid_amount'];
                    } else {
                        $fee_detail->paid_amount = $i != 0 ? $reCalculatedPendingFee[$j] : 0;
                    }

                    $fee_detail->save();
                    if (isset($course['extra'])) {
                        $o = 0;
                        foreach ($course['extra'] as $extra) {
                            ExtraFeeDetails::create([
                                'fee_detail_id' => $fee_detail->id,
                                'extra_fee_id' => $extra['id'],
                                'discount' => $extra['discount'],
                                'paid_amount' => (isset($course['paid_amount']) && $course['paid_amount'] != null) ? $extra['paid_amount'] : $calculatedPendingFeeExtra[$o]
                            ]);

                            $extraPending = PendingExtraFees::where('user_id', $request->user_id)
                                ->where('course_id', $course['course_id'])
                                ->where('extra_fee_type_id', $extra['id'])
                                ->first();
                            if ($extraPending) {
                                $amount = $extraPending->fees - $extra['paid_amount'] - (isset($extra['discount'])) ? $extra['discount'] : 0;
                                if ($amount) {
                                    $extraPending->fees = $amount;
                                    $extraPending->save();
                                } else {
                                    $extraPending->delete();
                                }
                            }
                            $o++;
                        }
                    }
                    $j++;

                    if ($i == 0) {
                        $pendingFee = PendingFee::where("user_id", $request->user_id)->where("course_id", $course['course_id'])->first();
                        $amount = $course['pending_amount'] - $course['paid_amount'] - $course['discount'];

                        if ($amount) {
                            $pendingFee->fees = $amount;
                            if (isset($course['fee_type'])) {
                                $pendingFee->fees_type = $course['fee_type'];
                            }
                            $date2 = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
                            $pendingFee->month = $date2->format('m');
                            $pendingFee->total_discount = $pendingFee->total_discount + $course['discount'];
                            $pendingFee->save();
                        } else {
                            $pendingExtraFee = PendingExtraFees::where('user_id', $request->user_id)->where('course_id', $course['course_id'])->get();
                            if ($pendingExtraFee->count() == 0) {
                                $pendingFee->delete();
                            } else {
                                $pendingFee->fees = $amount;
                                if (isset($course['fee_type'])) {
                                    $pendingFee->fees_type = $course['fee_type'];
                                }
                                $pendingFee->total_discount = $pendingFee->total_discount + $course['discount'];
                                $date2 = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
                                $pendingFee->month = $date2->format('m');
                                $pendingFee->save();
                            }
                            if ($course['course_id'] == 1) {
                                $student = Student::where("user_id", $request->user_id)->first();
                                $student->membership_date = date("Y-m-d");
                                $student->membership_status = 1;
                                $student->save();
                            }
                        }
                    }

                    if (isset($course["per_discount"])) {

                        $student_discount = StudentDiscount::updateOrCreate([
                            "user_id" => $request->user_id,
                            "course_id" => $course['course_id'],
                            "discount_amount" => $course['per_discount']
                        ]);
                        $student_discount->save();

                    } else {
                        StudentDiscount::where("user_id", $request->user_id)->where("course_id", $course['course_id'])->delete();
                    }
                }
            }

            $status = 1;

            $totalAmount = Fee::totalAmount($request->user_id);
            $pendingAmount = Fee::pendingAmount($request->user_id);
            $pendingRecord = PendingFee::where('fees_type', 0)->where('user_id', $request->user_id)->first();
            if ($pendingAmount == 0 && !$pendingRecord) {
                $status = 3;
            } else if ($totalAmount > $pendingAmount || $pendingRecord) {
                $status = 2;
            }
            $user = AppUser::where("id", $request->user_id)->first();
            $user->fee_status = $status;
            $student = Student::where('user_id',$user->id)->first();
            $student->advance_amount= ($request->advance > 0) ? $request->advance : 0;;
            $student->save();
            $user->save();


            DB::commit();

        } catch (Exception $e) {
            DB::rollback();
        }

        return redirect("/fees");
    }

    protected function grid()
    {
        return Admin::grid(Student::class, function (Grid $grid) {

            $q = $grid->model()->join('app_users','students.user_id','=','app_users.id');

            if (!Admin::user()->isAdministrator() AND !isRole('super-administrator')) {
                $branch = Branch::where('user_id',Admin::user()->id)->first();
                $q = $grid->model()->join('app_users','students.user_id','=','app_users.id')->where('students.branch_id',$branch->id);
            }

            $grid->column('user.name',"Student Name");


//            $grid->column("totalAmount", "Total Amount")->display(function () {
//                return Fee::totalAmount($this->id);
//            })->sortable();
            $grid->column("paidAmount", "Paid Amount")->display(function () {
                return Fee::paidAmount($this->user_id);
            });
            $grid->column("totalDiscount", "Total Discount")->display(function () {
                return Fee::totalDiscount($this->user_id);
            });
            $grid->column("advance", "Advance")->display(function () {
                return $this->advance_amount;
            });
            $grid->column("pendingAmount", "Pending Amount")->display(function () {
                return Fee::pendingAmount($this->user_id);
            });
            $grid->column("refundAmount", "Refund Amount")->display(function () {
                return Fee::refundAmount($this->user_id);
            });
            $grid->column("status", "Fee Status")->display(function () {
                return "<label class='label label-" . config("app.fee_status." . $this->fee_status . ".class") . "'>" . config("app.fee_status." . $this->fee_status . ".name") . "</label>";
            });

            $q->orderBy("students.id", "desc");

            $grid->disableRowSelector();
            $grid->disableCreateButton();

            $excel_headers = ["Student Name","Paid Amount","Total Discount","Advance","Pending Amount","Fee Status"];
            $excel_columns = ['user.name','student_category','second_contact_no','advance_amount','gender','fee_status'];
            $grid->exporter(new ExcelExport("Fees", $excel_headers, $excel_columns));

            $grid->filter(function ($filter){

                $filter->disableIdFilter();
                $filter->like('user.name','Student Name');

                $status = array();
                foreach (config('app.fee_status') as $key=>$value)
                {
                    $status[$key] = $value['name'];
                }
                $filter->where(function($q){
                    $q->where('app_users.fee_status',$this->input);
                }, "Fee Status")->select($status);

            });

            $grid->actions(function ($actions) {

                $pendingAmount = Fee::pendingAmount($actions->row->id);
                $pendingRecord = PendingFee::where('fees_type', 0)->where('user_id', $actions->row->id)->first();

                $actions->disableEdit();
                $actions->disableDelete();
                //$actions->prepend('<a href="/fees/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');
                if ($pendingAmount > 0 || $pendingRecord) {
                    $actions->prepend('<a href="/fees/create?student_id=' . $actions->row->id . '" data-toggle="tooltip" title="Take Fee" class="btn" style="background-color: #32c861; border: #32c861"><i class="fa fa-plus"></i></a>');
                }
                $actions->prepend('<a href="" data-id="' . $actions->row->id . '" data-toggle="tooltip" title="View Fee History" class="btn open-fee-history-modal" style="background-color: #01a4f8; border: #01a4f8;"><i class="fa fa-eye"></i></a>');

            });

        });
    }
}
