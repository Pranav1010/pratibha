<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Tools\GridSearch;
use App\City;
use App\Course;
use App\CourseStudent;
use App\CourseStudentSchool;
use App\ExtraFee;
use App\Parents;
use App\Profession;
use App\School;
use App\Student;
use App\User;

use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Mockery\Exception;
use Validator;
use Illuminate\Support\Facades\DB;


class SchoolStudentController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["
            $.getScript('/js/student-school.js');
        "]);
    }

    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('School Students');
            $content->row(function (Row $row) {
                $row->column(12, $this->grid());
                $row->column(12, view('student.modal'));
            });
        });
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('School Students');


            $professions = Profession::all();
            $cities = City::all();
            $courses = Course::all();
            $parents = Parents::all();
            $cities = City::all();
            $schools = School::all();
            $professions = Profession::all();
            $student = User::find($id);

            if ($student == null) {
                return redirect('students');
            }

            $studentCourses = CourseStudentSchool::where('user_id', $id)->pluck('course_id')->toArray();

            if (isset($student->student->bdate)) {
                $student->student->bdate = date('d/m/Y', strtotime($student->student->bdate));
            }

            $content->row(view('student.school.edit-student', compact('student', 'courses', 'parents', 'cities', 'professions', 'schools', 'studentCourses')));
            $content->row(view("addModal.addParent", compact("professions", "cities")));
            $content->row(view("addModal.addSchool", compact("cities")));
            $content->row(view("addModal.addCity"));
            $content->row(view("addModal.addProfession"));
        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('School Students');
            $courses = Course::all();
            $parents = Parents::all();
            $cities = City::all();
            $schools = School::all();
            $professions = Profession::all();
            $feeTypes = config('app.fee_type');
            $extraFees = ExtraFee::all();

            $content->row(view('student.school.add-student', compact(['courses', 'parents', 'cities', 'schools', 'professions'])));
            $content->row(view("addModal.addParent", compact("professions", "cities")));
            $content->row(view("addModal.addSchool", compact("cities")));
            $content->row(view("addModal.addCity"));
            $content->row(view("addModal.addProfession"));
        });
    }

    protected function grid()
    {
        return Admin::grid(User::class, function (Grid $grid) {

            $grid->disableExport();
            $grid->disableFilter();
            $grid->disableRowSelector();

            $grid->model()->orderBy('id', 'desc');
            $grid->model()->whereHas('student', function ($q) {
                $q->where('student_category', '!=', 0);
            });
            $grid->model()->whereHas("roles", function ($q) {
                $role = Role::where("slug", "student")->first();
                $id = ($role) ? $role->id : 0;
                $q->where('role_id', $id);
            });

            $grid->name('Name')->sortable();
            $grid->username('Username')->sortable();
            $grid->column('id', 'Activities')->display(function ($id) {

                $data = CourseStudentSchool::where('user_id',$id)->get();
                $string = '';
                if ($data->count() == 0)
                    return '-';
                foreach ($data as $key => $value) {
                        $string .= '<span class="label label-success" style="margin: 5px;">' . $value->course->name . '</span>';
                }
                return $string;

            });

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableEdit();
                $actions->disableDelete();
                $actions->prepend('<a href="/students-school/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');
                $actions->prepend('<a class="btn btn-primary openModal" data-id="' . $actions->row->id . '"><i class="fa fa-eye"></i></a>');
                $actions->append('<a class="btn btn-danger delete-row" onclick="deleteRow(this)" data-id="' . $actions->row->id . '"><i class="fa fa-trash"></i></a>');
            });

            // Search Filter -----

            $grid->tools(function ($tools) {
                $tools->append(new GridSearch());
//                $tools->prepend("<a href='" . url('/students') . "' class='btn btn-success pull-right'><i class=\"fa fa-save\"></i> New</a>");
            });

            $value = Input::get('search');
            $q = $grid->model();

            if (!empty($value)) {

                $q->whereHas("roles", function ($q) {
                    $role = Role::where("slug", "student")->first();
                    $id = ($role) ? $role->id : 0;
                    $q->where('role_id', $id);
                });

                $q->where(function ($query) use ($value) {
                    $query->where("name", "like", "%{$value}%")
                        ->orWhere("username", "like", "%{$value}%")
                        ->orWhereHas('userCourse.course', function ($qr) use ($value) {
                            $qr->where('name', "like", "%{$value}%");
                        });
                });

            }

            $grid->paginate(10);
        });
    }

    protected function form()
    {
        return Admin::form(User::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'parent' => 'required',
            'address' => 'required',
            'school' => 'required',
            'mobile' => 'required|unique:users,mobile',
            'courses' => 'required',
        ]);

        if ($validator->fails()) {
            $error = [];
            $error['error'] = $validator->getMessageBag();
            return response()->json($error, 406);
        } else {

            DB::beginTransaction();

            try {

                $user = new User();
                $user->name = $request->name;
                $user->mobile = $request->mobile;
                $user->email = $request->email;
                $user->city_id = $request->city;
                $user->address = $request->address;
                $user->username = $request->mobile;
                $user->password = bcrypt(config('app.default_password'));
                $user->save();

                $user->roles()->save(Role::where('slug', 'student')->first());

                $date = ($request->bdate != null) ? date('Y/m/d', strtotime($request->bdate)) : null;

                $student = new Student();
                $student->user_id = $user->id;
                $student->parent_id = $request->parent;
                $student->bdate = $date;
                $student->school_id = $request->school;
                $student->is_inquiry = ($request->inquiry_id) ? 1 : 0;
                $student->guardian_name = ($request->guardian_name) ? $request->guardian_name : null;
                $student->guardian_email = ($request->guardian_email) ? $request->guardian_email : null;
                $student->guardian_mobile = ($request->guardian_mobile) ? $request->guardian_mobile : null;
                $student->guardian_profession = ($request->guardian_profession) ? $request->guardian_profession : null;
                $student->aadhar_no = $request->aadhar_no;
                $student->second_contact_no = ($request->second_contact_no) ? $request->second_contact_no : null;
                $student->reference_no = date('y') . '' . date('m') . '' . $user->barnch_id;
                $student->student_category = 1;
                $student->save();

                $currentDate = new \DateTime("now", new \DateTimeZone("Asia/Kolkata"));
                $currentMonth = $currentDate->format("m");

                foreach ($request->courses as $key => $value) {
                    CourseStudentSchool::create([
                        'course_id' => $value,
                        'user_id' => $user->id
                    ]);
                }


                DB::commit();

                return response()->json(array('result' => "success", 'student_id' => $user->id), 200);

            } catch (Exception $exception) {
                DB::rollBack();
                return null;
            }

        }

    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'parent' => 'required',
            'address' => 'required',
            'school' => 'required',
            'courses' => 'required',
            'mobile' => 'required'
        ]);

        $error = $validator->getMessageBag()->toArray();

        if ($validator->fails()) {
            return response()->json($error, 406);
        } else {
            DB::beginTransaction();
            try {
                $user = User::find($id);
                $user->name = $request->name;
                $user->mobile = $request->mobile;
                $user->email = $request->email;
                $user->city_id = $request->city;
                $user->address = $request->address;
                $user->save();


                if (isset($request->bdate)) {
                    $date = date('Y/m/d', strtotime($request->bdate));
                } else {
                    $date = null;
                }

                $student = Student::where('user_id', $id)->first();
                $student->user_id = $user->id;
                $student->parent_id = $request->parent;
                $student->bdate = $date;
                $student->school_id = $request->school;
                $student->guardian_name = ($request->guardian_name) ? $request->guardian_name : null;
                $student->guardian_email = ($request->guardian_email) ? $request->guardian_email : null;
                $student->guardian_mobile = ($request->guardian_mobile) ? $request->guardian_mobile : null;
                $student->guardian_profession = ($request->guardian_profession) ? $request->guardian_profession : null;
                $student->guardian_relation = ($request->guardian_relation) ? $request->guardian_relation : null;
                $student->aadhar_no = $request->aadhar_no;
                $student->save();

                CourseStudentSchool::where('user_id', $user->id)->delete();
                foreach ($request->courses as $course) {
                    CourseStudentSchool::create([
                        'user_id' => $user->id,
                        'course_id' => $course
                    ]);
                }
                DB::commit();

                return response()->json(array('result' => "success", 'student_id' => $user->id), 200);

            } catch (Exception $exception) {
                DB::rollBack();
                return null;
            }


        }

    }

}
