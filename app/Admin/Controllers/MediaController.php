<?php

namespace App\Admin\Controllers;

use App\Event;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;
use App\Gallery;
use File;
use Image;
use Illuminate\Support\Facades\Validator;

class MediaController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["

            $('<link/>', {
               rel: 'stylesheet',
               type: 'text/css',
               href: '/tools/lightGallery/css/lightgallery.min.css'
            }).appendTo('head');
            
            $('<link/>', {
               rel: 'stylesheet',
               type: 'text/css',
               href: '/css/dropzone.css'
            }).appendTo('head');
        
            $.getScript('/tools/lightGallery/js/lightgallery-all.min.js');
            $.getScript('/js/dropzone.js');
            $.getScript('/js/blazy.min.js');
            
        "]);
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index(Request $request)
    {
        $eventId = $request->event;
        $event = Event::find($eventId);
        return Admin::content(function (Content $content) use ($eventId, $event) {
            $content->header('Media Gallery');
            $content->row(function (Row $row) use ($eventId, $event) {
                $row->column(12, view('media.index', compact('eventId', 'event')));
            });
        });
    }

    public function store(Request $request)
    {
        $error = [];
        $image = $request->file;
        $eventId = $request->eventId;
        $validate = Validator::make($request->all(), [
            'file' => 'image'
        ]);
        if ($validate->fails()) {
            $error['event_details'] = $validate->getMessageBag();
            return response('file must be image', 400);
        } else {
            if (isset($eventId)) {
                $filename = uniqid() . '.' . File::extension($image->getClientOriginalName());

                //Create thumbnail logic
                $destinationPath = public_path('upload/thumbnail');
                $img = Image::make($image->getRealPath());
                $img->resize(400, 300)->save($destinationPath . '/' . $filename);

                $image->move(public_path('upload/images'), $filename);

                $gallery = new Gallery();
                $gallery->event_id = $eventId;
                $gallery->type = 'image';
                $gallery->path = $filename;
                $gallery->save();


            }
        }

    }

    public function destroy($id)
    {
        $file = Gallery::find($id);

        File::delete(public_path('upload/images/') . $file->path);
        File::delete(public_path('upload/thumbnail/') . $file->path);

        $file->delete();
        return response('Deleted', 200);
    }

    public function gallery($id)
    {
        $event = Event::find($id);
        return view('media.gallery', compact('event'));
    }
}
