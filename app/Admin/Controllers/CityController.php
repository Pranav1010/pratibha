<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Tools\GridSearch;
use App\City;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Admin\Extensions\Tools\ExcelExport;

class CityController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["
            $('.col-md-8 .box-header .btn-group.pull-right a.btn.btn-sm.btn-success').attr('href', '/cities');
        "]);
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            Admin::script(["
                $('form').attr('action', '/cities');
            "]);

            $cities = City::all();
            $content->header('Cities');

            $content->row(function (Row $row) use ($cities) {
                $row->column(4, $this->form());
                $row->column(8, $this->grid());
            });
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Cities');

            $content->row(function (Row $row) use ($id) {
                $row->column(4, $this->form($id)->edit($id));
                $row->column(8, $this->grid($id));
            });

        });
    }

    public function addCity(Request $request)
    {

        $validate = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validate->fails()) {
            $error['error'] = $validate->getMessageBag();

            return response()->json($error, 406);
        } else {

            $city = City::updateOrCreate(
                ["name" => $request->name]
            );

            return json_encode($city);
        }


    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid($id = null)
    {
        return Admin::grid(City::class, function (Grid $grid) use ($id) {

            $grid->model()->orderBy('name','ASC');

            $grid->column('name')->sortable();

            if(!isset($id)) {
                $grid->disableCreateButton();
            }
            $grid->disableFilter();
            $grid->disableRowSelector();
            $grid->paginate(10);

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableEdit();
                $actions->prepend('<a href="/cities/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');

            });


            $excel_headers = ["City Name"];
            $excel_columns = ["name"];
            $grid->exporter(new ExcelExport("Cities Names", $excel_headers, $excel_columns));

            $grid->tools(function ($tools) {
                $tools->append(new GridSearch());
            });

            $value = Input::get('search');

            if (!empty($value)) {

                $q = $grid->model();

                $q->where('name', "like", "%{$value}%");

            }
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(City::class, function (Form $form) use($id) {

            $form->text('name')->rules("required|unique:cities,name,$id")->attribute('autofocus');

            $form->saved(function () {
                admin_toastr(trans('admin.save_succeeded'));
                return redirect("/cities");
            });
            $form->setWidth(8, 3);

            $form->tools(function (Form\Tools $tools) {

                // Disable back btn.
                $tools->disableBackButton();

                // Disable list btn
                $tools->disableListButton();

                // Add a button, the argument can be a string, or an instance of the object that implements the Renderable or Htmlable interface
                //$tools->add('<a class="btn btn-sm btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;delete</a>');
            });

            $form->saving(function ($form){
                $form->name = ucwords($form->name);
            });

        });
    }
}
