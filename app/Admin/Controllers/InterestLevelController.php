<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Tools\GridSearch;
use App\InterestLevel;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Admin\Extensions\Tools\ExcelExport;

class InterestLevelController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            Admin::script(["
                $('form').attr('action', '/interestLevel');
            "]);

            $content->header('Follow Up Interest Levels');

            $content->row(function(Row $row) {
                $row->column(4, $this->form());
                $row->column(8, $this->grid());
            });
        });
    }

    public function addInterestLevel(Request $request)
    {

        $validate = Validator::make($request->all(),
            [
                'name' => 'required'
            ],
            [
                'name.required' => "The Interest Level field is required."
            ]
        );

        if ($validate->fails()) {

            $error['error'] = $validate->getMessageBag();
            return response()->json($error, 406);

        } else {

            $interest_level = InterestLevel::updateOrCreate(
                ["name" => $request->name]
            );

            return json_encode($interest_level);
        }
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            Admin::script(["
                $('.col-md-8 .box-header .btn-group.pull-right a').attr('href', '/interestLevel');
            "]);

            $content->header('Follow Up Interest Levels');

            $content->row(function(Row $row) use ($id){
                $row->column(4, $this->form()->edit($id));
                $row->column(8, $this->grid($id));
            });
        });
    }

    protected function grid($id = null)
    {
        return Admin::grid(InterestLevel::class, function (Grid $grid)  use($id) {
            $grid->model()->orderBy('name','ASC');
            $grid->name('Name')->sortable();

            $grid->disableRowSelector();
            $grid->disableFilter();

            $grid->actions(function ($actions) {

                $actions->disableEdit();
                $actions->prepend('<a href="/interestLevel/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');

            });

            if(!isset($id)){
                $grid->disableCreateButton();
            }

            $excel_headers = ["Name"];
            $excel_columns = ["name"];
            $grid->exporter(new ExcelExport("Interest Level", $excel_headers, $excel_columns));

            $grid->tools(function ($tools) {
                $tools->append(new GridSearch());
            });

            $value = Input::get('search');

            if (!empty($value)) {

                $q = $grid->model();

                $q->where('name', "like", "%{$value}%");

            }


        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(InterestLevel::class, function (Form $form) {

            $form->text('name')->rules(function ($form){

                // If it is not an edit state, add field unique verification
                if (!$id = $form->model()->id) {
                    return 'required|unique:interest_levels,name';
                }
                return 'required|unique:interest_levels,name,'.$form->model()->id;
            })->attribute('autofocus');

            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableListButton();
            });

            $form->saved(function () {
                admin_toastr(trans('admin.save_succeeded'));
                return redirect("/interestLevel");
            });

            $form->setWidth(8, 3);
            $form->saving(function ($form){
                $form->name = ucwords($form->name);
            });
        });

    }
}
