<?php

namespace App\Admin\Controllers;

use App\Activity;
use App\Branch;
use App\BranchExtraFee;

use App\Course;
use App\User;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\ExtraFee;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Admin\Extensions\Tools\ExcelExport;

class BranchExtraFeeController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script("$.getScript('/js/branch-extra-fee.js');");
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {

        return Admin::content(function (Content $content) {
            $activities =null;
            $branch = Branch::where('user_id',Admin::user()->id)->first();
            if(isset($branch))
            {
                $activities = Activity::join('courses','courses.activity_id','=','activities.id')->select('activities.id','activities.name')->where('courses.branch_id',$branch->id)->get();
            }
            else
            {
                $activities = Activity::all();
            }
            $content->header('Branch Extra Fee');
            $content->row(function(Row $row) use($activities,$branch) {
                $row->column(4, view('branchExtraFee.create',compact('activities','branch')));
                $row->column(8, $this->grid());
            });


        });
    }

    public function store(Request $request)
    {
        $error = [];
        DB::beginTransaction();

        $validate = Validator::make($request->all(), [
            'extra_fee' => 'required|unique:branch_extra_fees,extra_fee_id,NULL,id,branch_id,'.$request->branch_id.',activity_id,'.$request->activity_name,
            'activity_name' => 'required|unique:branch_extra_fees,activity_id,NULL,id,branch_id,'.$request->branch_id.',extra_fee_id,'.$request->extra_fee,
            'branch_id' => 'required|unique:branch_extra_fees,branch_id,NULL,id,activity_id,'.$request->activity_name.',extra_fee_id,'.$request->extra_fee,
            'amount' => 'required'
        ]);

        if ($validate->fails())
        {
            $error['branch_extra_fee_details'] = $validate->getMessageBag();
        }
        else
        {
            $branchExtraFee = new BranchExtraFee();
            $branchExtraFee->extra_fee_id = $request->extra_fee;
            $branchExtraFee->activity_id = $request->activity_name;
            $branchExtraFee->amount = $request->amount;
            $branchExtraFee->branch_id = $request->branch_id;
            $branchExtraFee->save();

        }

        //Set response
        if (count($error) > 0)
        {
            DB::rollback();
            return response()->json($error, 500);

        }
        else
        {
            DB::commit();
            return response()->json("success", 200);
        }
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use($id) {
            $activities =null;
            $branchExtraFee = BranchExtraFee::find($id);
            if(isset($branchExtraFee))
            {
                $activities = Activity::join('courses','courses.activity_id','=','activities.id')->select('activities.id','activities.name')->where('courses.branch_id',$branchExtraFee->branch_id)->get();
            }
            $content->header('Extra Fee');
            $content->row(function(Row $row) use($activities,$branchExtraFee) {
                $row->column(4, view('branchExtraFee.edit',compact('activities','branchExtraFee')));
                $row->column(8, $this->grid());
            });
        });
    }

    public function update(Request $request,$id)
    {
        $error = [];
        DB::beginTransaction();

        $validate = Validator::make($request->all(), [
            'extra_fee' => 'required',
            'activity_name' => 'required',
            'branch_id' => 'required',
            'amount' => 'required'
        ]);

        if ($validate->fails())
        {
            $error['branch_extra_fee_details'] = $validate->getMessageBag();
        }
        else
        {
            $branchExtraFee = BranchExtraFee::find($id);
            $branchExtraFee->extra_fee_id = $request->extra_fee;
            $branchExtraFee->activity_id = $request->activity_name;
            $branchExtraFee->amount = $request->amount;
            $branchExtraFee->branch_id = $request->branch_id;
            $branchExtraFee->save();

        }

        //Set response
        if (count($error) > 0)
        {
            DB::rollback();
            return response()->json($error, 500);

        }
        else
        {
            DB::commit();
            return response()->json("success", 200);
        }
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(BranchExtraFee::class, function (Grid $grid) {

            $grid->disableRowSelector();
            $grid->disableCreateButton();

            if(!Admin::user()->isAdministrator() AND !isRole('super-administrator'))
            {
                $branch = Branch::where('user_id',Admin::user()->id)->first();
                $grid->model()->where("branch_id",$branch->id);
            }

            $grid->column('extraFee.name','Extra Fee')->sortable();

            $grid->column('activity.name','Activity Name')->sortable();

            $grid->amount()->sortable();

            if(Admin::user()->isAdministrator() || isRole('super-administrator'))
            {
                $grid->column('branch.name','Branch Name')->sortable();
            }

            $grid->actions(function ($action){
               $action->disableEdit();
               $action->prepend('<a href="/branch-extra-fee/' . $action->row->id . '/edit"><i class="fa fa-edit"></i></a>');
            });

            $grid->filter(function ($filter){

                $filter->disableIdFilter();

                $filter->where(function($q){
                    $q->where('extra_fee_id',$this->input);
                }, "Extra Fee")->select(ExtraFee::pluck("name", "id")->toArray());

                $filter->where(function($q){
                    $q->where('activity_id',$this->input);
                }, "Activity")->select(Activity::pluck("name", "id")->toArray());

                $filter->like("amount");

                if(Admin::user()->isAdministrator() || isRole('super-administrator'))
                {
                    $filter->where(function($q){
                        $q->where('branch_id',$this->input);
                    }, "Branch")->select(Branch::pluck("name", "id")->toArray());
                }

            });


            if(Admin::user()->isAdministrator() || isRole('super-administrator'))
            {
                $excel_headers = ["Extra Fee","Activity Name","Amount","Branch"];
                $excel_columns = ["extraFee.name","activity.name","amount","branch.name"];
            }
            else {
                $excel_headers = ["Extra Fee","Activity Name","Amount"];
                $excel_columns = ["extraFee.name","activity.name","amount"];
            }
            $grid->exporter(new ExcelExport("Branch Extra Fees", $excel_headers, $excel_columns));

        });
    }

    public function destroy($id)
    {
        BranchExtraFee::find($id)->delete();
        return response()->json(array('status' => true, 'message' => "Delete succeeded !"), 200);
    }


}
