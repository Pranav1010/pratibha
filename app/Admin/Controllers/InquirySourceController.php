<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Tools\GridSearch;
use App\InquirySource;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Admin\Extensions\Tools\ExcelExport;
class InquirySourceController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["
            $('.col-md-8 .box-header .btn-group.pull-right a.btn.btn-sm.btn-success').attr('href', '/sources');
        "]);
    }

    /**
     * Index interface.
     *
     * @return Content
     */

    public function index()
    {
        return Admin::content(function (Content $content) {

            Admin::script(["
                $('form').attr('action', '/sources');
            "]);

            $content->header('Sources');

            $content->row(function(Row $row){
                $row->column(4, $this->form());
                $row->column(8, $this->grid());
            });
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Sources');

            $content->row(function(Row $row) use($id){
                $row->column(4, $this->form()->edit($id));
                $row->column(8, $this->grid($id));
            });

            //$content->body($this->form()->edit($id));
        });
    }

    public function addSource(Request $request)
    {

        $validate = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if ($validate->fails()) {

            $error['error'] = $validate->getMessageBag();
            return response()->json($error, 406);

        } else {

            $source = InquirySource::updateOrCreate(
                ["name" => $request->name]
            );

            return json_encode($source);
        }
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid($id = null)
    {
        return Admin::grid(InquirySource::class, function (Grid $grid) use($id) {

            $grid->model()->orderBy('name','ASC');

            $grid->column('name','Source')->sortable();
            $grid->disableRowSelector();
            $grid->disableFilter();
            $grid->paginate(10);

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableEdit();
                $actions->prepend('<a href="/sources/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');

            });

            if(!isset($id)){
                $grid->disableCreateButton();
            }

            $grid->tools(function ($tools) {
                $tools->append(new GridSearch());
            });

            $value = Input::get('search');

            if (!empty($value)) {

                $q = $grid->model();

                $q->where('name', "like", "%{$value}%");

            }

            $excel_headers = ["Source Name"];
            $excel_columns = ["name"];
            $grid->exporter(new ExcelExport("Source Names", $excel_headers, $excel_columns));


        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(InquirySource::class, function (Form $form) {

            $form->text('name', "Source")->rules(function ($form){

                // If it is not an edit state, add field unique verification
                if (!$id = $form->model()->id) {
                    return 'required|unique:inquiry_sources,name';
                }
                return 'required|unique:inquiry_sources,name,'.$form->model()->id;
            })->attribute('autofocus');

            $form->saved(function () {
                admin_toastr(trans('admin.save_succeeded'));
                return redirect("/sources");
            });

            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableListButton();
            });

            $form->setWidth(8, 3);

            $form->saving(function ($form){
                $form->name = ucwords($form->name);
            });

        });
    }
}
