<?php

namespace App\Admin\Controllers;

use App\SchoolPaymentHistory;
use App\SchoolPayment;
use App\SchoolPendingPayment;
use App\School;
use App\Refund;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class PaymentController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["
            $('select').select2();
            $.getScript('/js/payment.js');
        "]);
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $totalPaidAmount = SchoolPaymentHistory::all()->sum('paid_amount');
            $totalPendingAmount = SchoolPendingPayment::all()->sum('amount');
            $totalDiscountAmount = SchoolPendingPayment::all()->sum('discount');
            $totalRefundAmount = Refund::all()->sum('amount');

            $content->header('School Payments');
            $content->row(view("payment.infobox", compact('totalPaidAmount','totalPendingAmount','totalDiscountAmount','totalRefundAmount')));
            $content->row($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('header');
            $content->description('description');
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content){

            $content->header('Take Payment');
            $school = School::find(request()->school_id);
            $payments = SchoolPendingPayment::where('school_id',$school->id)->get();
            $schoolPayment = SchoolPayment::find($school->id);
            $schoolPaymentHistory = SchoolPaymentHistory::where('school_id',$school->id)->orderBy('id','desc')->get();
            $totalPaidAmount = SchoolPaymentHistory::where('school_id',$school->id)->sum('paid_amount');
            $discount = SchoolPaymentHistory::where('school_id',$school->id)->sum('discount');
            $totalPendingAmount = SchoolPendingPayment::where('school_id',$school->id)->sum('amount');
            $totalAmount = $totalPaidAmount + $totalPendingAmount;


            $content->row(function(Row $row) use ($school,$payments,$schoolPayment,$schoolPaymentHistory,$totalPaidAmount,$totalPendingAmount,$totalAmount,$discount) 
            {
                $row->column(6, view('payment.index',compact('school', 'payments')));
                $row->column(5, view('payment.status',compact('school', 'payments','schoolPayment','schoolPaymentHistory','totalPaidAmount','totalPendingAmount','totalAmount','discount')));
            });
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(School::class, function (Grid $grid)  {

            $grid->name('School Name')->sortable();

            $grid->column('SchoolPaymentHistory.paid_amount','Paid Amount')->display(function($sum){
                return number_format(SchoolPaymentHistory::paidAmount($this->id),2);
            })->sortable();

            $grid->column('SchoolPaymentHistory.discount','Discount')->display(function(){
                return number_format(SchoolPaymentHistory::where("school_id", $this->id)->sum("discount"),2);
            })->sortable();

             $grid->column('SchoolPendingPayment.amount','Pending Amount')->display(function(){
                return number_format(SchoolPendingPayment::where("school_id", $this->id)->sum("amount"),2);
            })->sortable();

            $grid->column('amount','Refund Amount')->display(function(){
                return number_format(Refund::where('school_id',$this->id)->sum("amount"),2);

            });


            $grid->school_status('School Status')->display(function ($status) {

                if($status == "Green")
                {
                    $label = "success";
                }
                else if($status == "Black")
                {
                     $label = "default";
                }
                else if($status == "Orange")
                {
                     $label = "warning";
                }

                return '<span class="label label-'.$label.'">'.$status.'</span>';
            })->sortable();


            $currentFinancialYear = $this->currentFinancialYear();
            $grid->actions(function($actions) use ($currentFinancialYear) {
                $schoolPayment = SchoolPayment::where('school_id', $actions->row->id)->where('year', $currentFinancialYear)->first();
                $totalDueAmount = SchoolPendingPayment::where("school_id", $actions->row->id)->sum("amount");
                $totalPaidAmount = SchoolPaymentHistory::paidAmount($actions->row->id);

                if (isset($schoolPayment->total_amount))
                {
                    if($schoolPayment->total_amount - $totalDueAmount < 0)
                    {
                        $actions->prepend('<a href="#" class="btn btn-xs btn-primary" data-placement="left" title="Refund Details" data-toggle="popover" data-trigger="hover" data-content="Previous Year Payment still Pending."><i class="fa fa-repeat"></i></a>');
                    }
                    else if($totalPaidAmount <= 0 || $schoolPayment->total_amount - $totalDueAmount == 0)
                    {
                        $actions->prepend('<a href="#" class="btn btn-xs btn-primary" data-placement="left" title="Refund Details" data-toggle="popover" data-trigger="hover" data-content="Current Year Payment still Pending."><i class="fa fa-repeat"></i></a>');
                    }
                    else
                    {
                        $actions->prepend('<a href="/payments/refund/' . $actions->row->id .'" class="btn btn-xs btn-primary" title="Refund" data-toggle="tooltip"><i class="fa fa-repeat"></i></a>');
                    }
                }

                $actions->prepend('<a href="/payments/create?school_id=' . $actions->row->id .'" class="btn btn-xs btn-success" title="Take Payment" data-toggle="tooltip"><i class="fa fa-rupee"></i></a>');
                $actions->disableDelete();
                $actions->disableEdit();
            });

            $grid->disableRowSelector();
            $grid->disableCreateButton();
            $grid->disableFilter();
            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(SchoolPaymentHistory::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function store(Request $request)
    {
        $error = [];
        DB::beginTransaction();


        if($request->payment_mode == "Cheque")
        {
            $validate = Validator::make($request->all(), [
                'schoolId' => 'required',
                'date' => 'required',
                'paid_amount'=>'required',
                'payment_mode'=>'required',
                'bank_name'=>'required',
                'cheque_no'=>['required', 'digits:6']
            ]);
        }
        else
        {
            $validate = Validator::make($request->all(), [
            'schoolId' => 'required',
            'date' => 'required',
            'paid_amount'=>'required',
            'payment_mode'=>'required'
        ]);
        }

        if ($validate->fails()) 
        {
            $error['payment_details'] = $validate->getMessageBag();
        }
        else
        {

            $schoolPendingPayment = SchoolPendingPayment::where('school_id',$request->schoolId)->get();
            $paidAmount = $request->paid_amount;
            if(isset($request->discount))
            {
                $paidAmount = $paidAmount + $request->discount;
            }

            foreach ($schoolPendingPayment as $pendingAmount) 
            {
                $paidAmount = $paidAmount - $pendingAmount->amount;

                    if($paidAmount >= 0)
                    {
                        $pendingAmount->delete();
                    }
                    else
                    {
                        $pendingAmount->amount = abs($paidAmount);
                        $pendingAmount->save();
                        break;
                    }
            }

            $schoolPaymentHistory = new SchoolPaymentHistory;
            $schoolPaymentHistory->school_id = $request->schoolId;
            $schoolPaymentHistory->date = Carbon::createFromFormat('d/m/Y', $request->date)->format('Y.m.d');
            $schoolPaymentHistory->paid_amount = $request->paid_amount;
            $schoolPaymentHistory->discount = $request->discount;
            $schoolPaymentHistory->payment_mode = $request->payment_mode;
            $schoolPaymentHistory->bank_id = $request->bank_name;
            $schoolPaymentHistory->cheque_no = $request->cheque_no;
            $schoolPaymentHistory->remark = $request->remark;
            $schoolPaymentHistory->save();
        } 

        //Set response
        if (count($error) > 0) 
        {
            DB::rollback();
            return response()->json($error, 500);

        } 
        else 
        {
            DB::commit();
            return response()->json("success", 200);
        }
    }

    public function refundForm($school_id)
    {
        return Admin::content(function (Content $content) use ($school_id) {

            $content->header('Refund');
            $school = School::find($school_id);
            $payments = SchoolPendingPayment::where('school_id',$school->id)->get();
            $schoolPayment = SchoolPayment::where('school_id',$school->id)->where('year',$this->currentFinancialYear())->first();
            $schoolPaymentHistory = SchoolPaymentHistory::where('school_id',$school->id)->orderBy('id','desc')->get();
            $totalPaidAmount = SchoolPaymentHistory::where('school_id',$school->id)->sum('paid_amount');
            $refundHistory = Refund::where('school_id',$school->id)->orderBy('id','desc')->get();


            $content->row(function(Row $row) use ($school,$payments,$schoolPaymentHistory,$totalPaidAmount,$schoolPayment,$refundHistory)
            {
                $row->column(6, view('refund.index',compact('school', 'payments','schoolPaymentHistory','totalPaidAmount','schoolPayment')));
                $row->column(5, view('refund.status',compact('refundHistory')));
            });
        });
    }

    public function refundStore(Request $request)
    {
        $error = [];
        DB::beginTransaction();


        if($request->refund_mode == "Cheque")
        {
            $validate = Validator::make($request->all(), [
                'schoolId' => 'required',
                'date' => 'required',
                'refund_amount'=>'required',
                'refund_mode'=>'required',
                'bank_name'=>'required',
                'cheque_no'=>['required', 'digits:6']
            ]);
        }
        else
        {
            $validate = Validator::make($request->all(), [
                'schoolId' => 'required',
                'date' => 'required',
                'refund_amount'=>'required',
                'refund_mode'=>'required'
            ]);
        }

        if ($validate->fails())
        {
            $error['payment_details'] = $validate->getMessageBag();
        }
        else
        {
            $schoolRefund = new Refund;
            $schoolRefund->school_id = $request->schoolId;
            $schoolRefund->date = Carbon::createFromFormat('d/m/Y', $request->date)->format('Y.m.d');
            $schoolRefund->amount = $request->refund_amount;
            $schoolRefund->refund_mode = $request->refund_mode;
            $schoolRefund->bank_id = $request->bank_name;
            $schoolRefund->cheque_no = $request->cheque_no;
            $schoolRefund->remark = $request->remark;
            $schoolRefund->save();
        }

        //Set response
        if (count($error) > 0)
        {
            DB::rollback();
            return response()->json($error, 500);

        }
        else
        {
            DB::commit();
            return response()->json("success", 200);
        }
    }

    public function currentFinancialYear()
    {
        $now = Carbon::now();
        $year = $now->year;
        $month =$now->month;

        if($month < 4)
        {
            return ($year-1).'-'.$year;
        }
        else
        {
            return $year.'-'.($year+1);
        }
    }
}
