<?php

namespace App\Admin\Controllers;

use App\FeeType;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Widgets\Box;
use Encore\Admin\Widgets\Form as FormWidget;


class FeeTypesController extends Controller
{
    use ModelForm;

    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Fee Types');

            $content->row($this->inline());
            $content->row($this->grid());
        });
    }

    public function inline($id = null)
    {
        $form = new FormWidget();
        $data = FeeType::find($id);
        $form->attribute(['class' => 'form-inline inline-custom-form']);
        if ($id != null) {
            $form->action("/fee-types/$id");
            $form->text('name', '')->rules('required')->default($data->name)->placeholder('Enter Fee-type');
            $form->hidden('_method')->default('PUT');
        } else {
            $form->action('/fee-types');
            $form->text('name', '')->rules('required')->placeholder('Enter Fee-type');
        }

        $box = new Box('Add New', $form->render());
        $box->style('custom-form');
        return $box;
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Fee Types');

            $content->row($this->inline($id));
            $content->row($this->grid());
        });
    }


    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('header');
            $content->description('description');

            $content->body($this->form());
        });
    }


    protected function grid()
    {
        return Admin::grid(FeeType::class, function (Grid $grid) {

            $grid->model()->orderBy('id','Desc');
            $grid->name();
            $grid->disableRowSelector();
            $grid->disableExport();
            $grid->disableFilter();
            $grid->disableCreateButton();
        });
    }

    protected function form()
    {
        return Admin::form(FeeType::class, function (Form $form) {

            $form->text('name', 'Name')->rules('required');
            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableListButton();
            });
            $form->disableReset();
        });
    }
}
