<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Tools\GridSearch;
use App\AppLogin;
use App\AppUser;
use App\Batch;
use App\Branch;
use App\City;
use App\Course;
use App\CourseFeeType;
use App\CourseStatus;
use App\CourseStudent;
use App\ExtraFee;
use App\Fee;
use App\FeeDetail;
use App\Http\Controllers\Controller;
use App\Inquiry;
use App\Parents;
use App\PendingExtraFees;
use App\PendingFee;
use App\Profession;
use App\School;
use App\Student;
use App\StudentCourseExtraFees;
use App\StudentCourseFeeTypes;
use App\StudentDiscount;
use App\User;
use DateTime;
use DateTimeZone;
use Encore\Admin\Auth\Database\Role;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\MessageBag;
use Mockery\Exception;
use function Sodium\compare;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Admin\Extensions\Tools\ExcelExport;

class StudentController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["
            $.getScript('/js/student.js');

            $('<link/>', {
               rel: 'stylesheet',
               type: 'text/css',
               href: '/css/student.css'
            }).appendTo('head');
            
            $('.parent').closest('.form-group').append(\"<a href='#' class='btn btn-default add-button' data-modal='add-parent-modal' onclick='openModal(this, event)'><i class='fa fa-plus'></i></a>\");
            $('.school').closest('.form-group').append(\"<a href='#' class='btn btn-default add-button' data-modal='add-school-modal' onclick='openModal(this, event)'><i class='fa fa-plus'></i></a>\");
            $('.city_id').closest('.form-group').append(\"<a href='#' class='btn btn-default add-button' data-modal='add-city-modal' onclick='openModal(this, event)'><i class='fa fa-plus'></i></a>\");
            $('.professions').closest('.form-group').append(\"<a href='#' class='btn btn-default add-button' data-modal='add-profession-modal' onclick='openModal(this, event)'><i class='fa fa-plus'></i></a>\");
            
            /*setTimeout(function(){
                $('.mobile').inputmask('9999999999');
            
                $('input[type=\"checkbox\"].minimal').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                });
            },500);*/
            
            $('.form-history-back').on('click', function (event) {
                event.preventDefault();
                history.back(1);
            });
            
        "]);
        /*Admin::css(asset('/css/student.css'));*/
    }

    //region Main Methods

    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('Students');

            $content->row(function (Row $row) {
                // $row->column(5, $this->form());
//                $row->column(5, $this->getCourseDateForm());
                $row->column(12, $this->grid());
                $row->column(12, $this->getModel());
            });
        });
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Students');

            $content->row(function (Row $row) use ($id) {
                // $row->column(4, $this->form($id)->edit($id));
                $row->column(12, $this->editStudentForm($id));
//                $row->column(7, $this->grid());
                $row->column(12, $this->getModel());
            });

            $professions = Profession::all();
            $cities = City::all();
            $content->row(view("addModal.addParent", compact("professions", "cities")));
            $content->row(view("addModal.addSchool", compact("cities")));
            $content->row(view("addModal.addCity"));
            $content->row(view("addModal.addProfession"));
        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Students');
            $content->row($this->getCourseDateForm());
            $professions = Profession::all();
            $cities = City::all();
            $feeTypes = config('app.fee_type');
            $extraFees = ExtraFee::all();
            if(isRole('branch-admin'))
            {
                $branch = Branch::where('user_id',Admin::user()->id)->first();
                if($branch)
                {
                    $course = Course::where('branch_id',$branch->id);
                }
            }
            else{
                $course = Course::all();
            }
            if(isRole('branch-admin'))
            {
                $branch = Branch::where('user_id',Admin::user()->id)->first();
            }

            $content->row(view("addModal.addParent", compact("professions", "cities","branch")));
            $content->row(view("addModal.addSchool", compact("cities")));
            $content->row(view("addModal.addCity"));
            $content->row(view("addModal.addActivity",compact('feeTypes','extraFees')));
            $content->row(view("addModal.addBatch",compact('courses')));
            $content->row(view("addModal.addProfession"));
        });
    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'gender' => 'required',
            //'parent' => 'required',
            //'address' => 'required',
            //'school' => 'required',
            'mobile' => 'required'
        ]);

        $errors = [];

        $courses = array_filter($request->courses);
        $batches = array_filter($request->batches);
        $dates = array_filter($request->date);

        if (count($courses) > 0) {
            if (count(array_unique($courses)) < count($courses)) {
                $errors['course'][] = 'Duplicate values in courses.';
            }

            if (count($courses) != count($batches)) {
                $errors['batch'][] = 'Please select batches first.';
            }

            if (count($courses) != count($dates)) {
                $errors['date'][] = 'Please let us know from when you will start your course.';
            }
        }

        $error1 = $validator->getMessageBag()->toArray();

        if ($validator->fails() || count($errors) > 0) {
            return response()->json(['error' => array_merge($errors, $error1)], 406);
        } else {

            $user = AppUser::find($id);
            $user->name = ucwords($request->name);
            $user->contact_no = $request->mobile;
            $user->email = $request->email;
            $user->city_id = $request->city;
            $user->address = $request->address;
            $user->save();

            if (count($courses) > 0) {
                $amount = Fee::paidAmount($id);
                if ($amount == 0) {
                    $user->fee_status = 1;
                    $user->save();
                } else {
                    $user->fee_status = 2;
                    $user->save();
                }
            }

            if (isset($request->bdate)) {
                $date = date('Y/m/d', strtotime($request->bdate));
            } else {
                $date = null;
            }

            $student = Student::where('user_id', $id)->first();
            $student->user_id = $user->id;
            $student->parent_id = $request->parent;
            $student->gender = $request->gender;
            $student->bdate = $date;
            $student->school_id = $request->school;
            $student->guardian_name = ($request->guardian_name) ? ucwords($request->guardian_name) : null;
            $student->guardian_email = ($request->guardian_email) ? $request->guardian_email : null;
            $student->guardian_mobile = ($request->guardian_mobile) ? $request->guardian_mobile : null;
            $student->guardian_profession = ($request->guardian_profession) ? $request->guardian_profession : null;
            $student->guardian_relation = ($request->guardian_relation) ? $request->guardian_relation : null;
            $student->aadhar_no = $request->aadhar_no;
            $student->branch_id = $request->branch_name;
            $student->save();

            $courseStudent = CourseStudent::where('student_id', $id)->where('status', '!=', 2)->get();
            $pendingFees = PendingFee::where('user_id', $id)->get();

            $studentCourse = CourseStudent::where('student_id', $id)->where('status', '!=', 2)->get();

            foreach ($studentCourse as $course) {
                $feeDetailCourse = ($course->fees) ? $course->fees->feeDetails->pluck('course_id')->toArray() : [];
                if (!in_array($course->course_id, $feeDetailCourse))
                    $course->delete();
            }

            PendingFee::where('user_id', $id)->where("course_id", "!=", 1)->delete();


            foreach ($courses as $key => $value) {

                $studentCourse = new CourseStudent();
                $studentCourse->student_id = $user->id;
                $studentCourse->course_id = $value;
                $studentCourse->status = (count($courseStudent) > 0 && isset($courseStudent->where('course_id', $value)->first()->status)) ? $courseStudent->where('course_id', $value)->first()->status : 0;
                $studentCourse->batch_id = $batches[$key];
                $studentCourse->start_date = $dates[$key];
                $courseFeeTypeData = CourseFeeType::where('course_id', $value)->get();
                $studentCourse->fee_type = ($courseFeeTypeData->count() > 1) ? null : $courseFeeTypeData[0]->fee_type_id;
                $studentCourse->save();

                $time = strtotime('01-' . $dates[$key]);
                $month = date("m", $time);
                $year = date("Y", $time);

                $newPendingFees = new PendingFee();
                $newPendingFees->user_id = $user->id;
                $newPendingFees->course_id = $value;
                $newPendingFees->month = $month;
                $newPendingFees->year = $year;
                $courseFeeTypes = Course::find($value)->courseFeeType;
                $newPendingFees->fees_type = ($courseFeeTypes->count() == 1) ? $courseFeeTypes[0]->fee_type_id : 0;
                $newPendingFees->fees = ($courseFeeTypes->count() == 1) ? $courseFeeTypes[0]->fees : 0;
                $newPendingFees->save();

                PendingExtraFees::where('course_id', $value)->where('user_id', $user->id)->delete();
//                if (Course::find($value)->courseExtraFee->count() > 0) {
//                    foreach (Course::find($value)->courseExtraFee as $extraFees) {
//                        PendingExtraFees::create([
//                            'user_id' => $user->id,
//                            'course_id' => $extraFees->course_id,
//                            'extra_fee_type_id' => $extraFees->extra_fee_id,
//                            'fees' => $extraFees->fees
//                        ]);
//                    }
//                }


            }


            if ($request->inquiry_id) {
                $inq = Inquiry::find($request->inquiry_id);
                $inq->is_registered = 1;
                $inq->status = 1;
                $inq->save();
            }

            return response()->json(array('result' => "success", 'student_id' => $user->id), 200);
        }

    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'gender' => 'required',
//            'parent' => 'required',
//            'address' => 'required',
//            'school' => 'required',
            'isMember' => 'required',
            'mobile' => 'required',
        ], [
            'isMember.required' => 'Is Membership Fees Paid ?'
        ]);
        $errors = [];

        if (count($request->courses) > 0)
            $courses = array_filter($request->courses);
        else
            $courses = [];

        if ($request->batches != null && count($request->batches) > 0)
            $batches = array_filter($request->batches);
        else
            $batches = [];

        if (count($request->date) > 0)
            $dates = array_filter($request->date);
        else
            $dates = [];

        if (count($courses) > 0) {
            if (count(array_unique($courses)) < count($courses)) {
                $errors['course'][] = 'Duplicate values in courses.';
            }

            if (count($courses) != count($batches)) {
                $errors['batch'][] = 'Please select batches first.';
            }

            if (count($courses) > count($dates)) {
                $errors['date'][] = 'Please let us know from when you will start your course.';
            }
        }


        $error1 = $validator->getMessageBag()->toArray();

        if ($validator->fails() || count($errors) > 0) {
            return response()->json(['error' => array_merge($errors, $error1)], 406);
        } else {

            DB::beginTransaction();

            try {

                $appLogin = AppLogin::where('username',$request->mobile)->first();
                if(!isset($appLogin))
                {
                    $appLogin = new AppLogin();
                    $appLogin->username = $request->mobile;
                    $appLogin->password = bcrypt(config('app.default_password'));
                    $appLogin->save();
                }

                $appUser = new AppUser();
                $appUser->name = ucwords($request->name);
                $appUser->contact_no = $request->mobile;
                $appUser->email = $request->email;
                $appUser->city_id = $request->city;
                $appUser->address = $request->address;
                $appUser->app_login_id = $appLogin->id;
                $appUser->role_id = Role::select('id')->where('slug','student')->first()->id;
                $appUser->branch_id = Admin::user()->branch_id;
                $appUser->save();




                $date = ($request->bdate != null) ? date('Y/m/d', strtotime($request->bdate)) : null;
                // $date = date_format($date, "Y/m/d");

                $student = new Student();
                $student->user_id = $appUser->id;
                $student->parent_id = $request->parent;
                $student->gender = $request->gender;
                $student->bdate = $date;
                $student->school_id = $request->school;
                $student->is_inquiry = ($request->inquiry_id) ? 1 : 0;
                $student->guardian_name = ($request->guardian_name) ? $request->guardian_name : null;
                $student->guardian_email = ($request->guardian_email) ? $request->guardian_email : null;
                $student->guardian_mobile = ($request->guardian_mobile) ? $request->guardian_mobile : null;
                $student->guardian_profession = ($request->guardian_profession) ? $request->guardian_profession : null;
                $student->aadhar_no = $request->aadhar_no;
                $student->second_contact_no = ($request->second_contact_no) ? $request->second_contact_no : null;
                $student->reference_no = date('y') . '' . date('m') . '' . $appUser->barnch_id;
                $student->student_category = 0;
                $student->branch_id = $request->branch_name;
                $student->save();


                $appUser->relationship_id = $student->id;
                $appUser->save();


                $currentDate = new \DateTime("now", new \DateTimeZone("Asia/Kolkata"));
                $currentMonth = $currentDate->format("m");

                foreach ($courses as $key => $value) {

                    if (isset($dates[$key])) {
                        $date = date('Y/m/d', strtotime($dates[$key]));
                        // $date = date_format($date, "Y/m/d");
                    } else {
                        $date = null;
                    }

                    $studentCourse = new CourseStudent();
                    $studentCourse->student_id = $appUser->id;
                    $studentCourse->course_id = $value;
                    $studentCourse->batch_id = $batches[$key];
                    $studentCourse->start_date = $dates[$key];
                    $courseFeeTypeData = CourseFeeType::where('course_id', $value)->get();
                    $studentCourse->fee_type = ($courseFeeTypeData->count() > 1) ? null : $courseFeeTypeData[0]->fee_type_id;
                    $studentCourse->save();

                    $time = strtotime('01-' . $dates[$key]);
                    $month = date("m", $time);
                    $year = date("Y", $time);

                    $date1 = new DateTime("01-" . $dates[$key]);
                    $date2 = new DateTime("now", new DateTimeZone("Asia/Kolkata"));

                    $diff = $date1->diff($date2)->format("%r%m");
                    $year_diff = $date1->diff($date2)->format("%r%y");

                    $months = 0;

                    if ($year_diff > 0) {
                        $months = $year_diff * 12;
                    }

                    $diff += $months;

//                    for ($i = 0; $i <= $diff; $i++) {
                    $courseFeeTypes = Course::find($value)->courseFeeType;

                    $pendingFees = new PendingFee();
                    $pendingFees->user_id = $appUser->id;
                    $pendingFees->course_id = $value;
                    $pendingFees->month = $month;
                    $pendingFees->year = $year;
                    $pendingFees->fees_type = ($courseFeeTypes->count() == 1) ? $courseFeeTypes[0]->fee_type_id : 0;
//                    if (Course::find($value)->courseExtraFee->count() > 0) {
//                        foreach (Course::find($value)->courseExtraFee as $extraFees) {
//                            PendingExtraFees::create([
//                                'user_id' => $user->id,
                    $pendingFees->fees = ($courseFeeTypes->count() == 1) ? $courseFeeTypes[0]->fees : 0;
//                                'course_id' => $extraFees->course_id,
//                                'extra_fee_type_id' => $extraFees->extra_fee_id,
//                                'fees' => $extraFees->fees
//                            ]);
//                        }
//                    }

//                        if ($month + $i == 12) {
//                            $month = $i * -1;
//                        }
//                    }
                    $pendingFees->save();
                }

                if ($request->isMember == 0) {

                    $pendingFees = new PendingFee();
                    $pendingFees->user_id = $appUser->id;
                    $pendingFees->course_id = 1;
                    $pendingFees->month = date("m");
                    $pendingFees->year = date("Y");
                    $pendingFees->fees = Course::withoutGlobalScope("removeMembership")->find(1)->courseFeeType[0]->fees;
                    $pendingFees->fees_type = 2;
                    $pendingFees->save();

                } else {

                    $fees = Fee::create([
                        "user_id" => $appUser->id,
                        "payment_date" => $currentDate->format("Y-m-d H:i:s"),
                        "net_amount" => 0,
                        "lump_sum_amount" => 0,
                        "payment_mode" => 0,
                    ]);

                    FeeDetail::create([
                        "fee_id" => $fees->id,
                        "course_id" => 1,
                        "paid_amount" => ($request->isMember == 1) ? Course::withoutGlobalScope("removeMembership")->find(1)->courseFeeType[0]->fees : 0,
                        "discount" => ($request->isMember == 2) ? Course::withoutGlobalScope("removeMembership")->find(1)->courseFeeType[0]->fees : 0,
                    ]);

                    $student->membership_date = $currentDate->format("Y-m-d H:i:s");
                    $student->membership_status = 1;
                    $student->save();

                    $pendingFees = PendingFee::where("user_id", $appUser->id)->where("month", "<=", $currentMonth)->get();

                    if (count($pendingFees)) {
                        $appUser->fee_status = 2;
                    } else {
                        $appUser->fee_status = 3;
                    }

                    $appUser->save();

                }

                if ($request->inquiry_id) {
                    $inq = Inquiry::find($request->inquiry_id);
                    $inq->is_registered = 1;
                    $inq->status = 1;
                    $inq->save();
                }

                DB::commit();

                return response()->json(array('result' => "success", 'student_id' => $appUser->id), 200);

            } catch (Exception $exception) {
                DB::rollBack();
                return null;
            }

        }

    }

    public function destroy($id)
    {
        AppUser::find($id)->roles()->dissociate();
        CourseStudent::where('student_id', $id)->delete();
        Student::where('user_id', $id)->delete();
        PendingFee::where('user_id', $id)->delete();
        PendingExtraFees::where('user_id', $id)->delete();
        AppUser::find($id)->delete();

        return response()->json(array('status' => true, 'message' => "Delete succeeded !"), 200);
    }

    //endregion

    public function getCourseDateForm()
    {

        $cities = City::all();
        $professions = Profession::all();
        if(isRole('branch-admin'))
        {
            $branch = Branch::where('user_id',Admin::user()->id)->first();
            $course = Course::where('branch_id',$branch->id)->get();
            $parents = Parents::where('branch_id',$branch->id)->get();
            $schools = School::where('branch_id',$branch->id)->get();
        }
        else {
            $course = Course::all();
            $parents = Parents::all();
            $schools = School::all();
        }
        return view('student.course-date', compact(['course', 'parents', 'cities', 'schools', 'professions','branch']));
    }

    public function getModel()
    {
        return view('student.modal');
    }

    public function editStudentForm($id)
    {

        if(isRole('branch-admin'))
        {
            $branch = Branch::where('user_id',Admin::user()->id)->first();
            $course = Course::where('branch_id',$branch->id)->get();
            $parents = Parents::where('branch_id',$branch->id)->get();
            $schools = School::where('branch_id',$branch->id)->get();
        }
        else{
            $course = Course::all();
            $parents = Parents::all();
            $schools = School::all();
        }


        $cities = City::all();
        $professions = Profession::all();

        $student = AppUser::find($id);

        if ($student == null) {
            return redirect('students');
        }
        $studentCourses = (isset($student->userCourse)) ? $student->userCourse->where('status', '!=', 2) : [];
        $combo = [];
        $i = 0;
        foreach ($studentCourses as $key => $value) {

            $combo[$i]['course_id'] = $value->course_id;
            $combo[$i]['batch_id'] = $value->batch_id;
            $combo[$i]['start_date'] = ($value->start_date != '') ? date('m/Y', strtotime('01-' . $value->start_date)) : null;

            $combo[$i]['batches'] = Batch::where('course_id', $value->course_id)->pluck('name', 'id')->toArray();

            $fees = Fee::where('user_id', $id)->whereHas('feeDetails', function ($q) use ($value) {
                $q->where('course_id', $value->course_id);
            })->get();

            if ($fees->count() > 0)
                unset($combo[$i]);
            else
                $i++;

        }

        if (isset($student->student->bdate)) {

            $student->student->bdate = date('dd-mm-Y', strtotime($student->student->bdate));

        }

        return view('student.student-edit', compact('combo', 'student', 'course', 'parents', 'cities', 'schools', 'professions'));
    }

    public function studentDetail($id)
    {

        $student = AppUser::find($id);

        $courses = CourseStudent::where('student_id', $id)->get();

        foreach ($courses as $key => $value) {

            if ($value->status == 0) {
                if (date('m/Y') < date("m/Y", strtotime('01-' . $value->start_date))) {
                    $currentMonth = date("m");
                    $givenMonth = substr($value->start_date, 0, 2);
                    $count = $givenMonth - $currentMonth;
                    //$count = date("m/Y", strtotime('01-' . $value->start_date)) - date('m/Y');
                    $value->display_status = ($count == 1) ? 'Will start next month' : 'Will start on ' . date("M", strtotime('01-' . $value->start_date));

                } else {
                    $value->display_status = config('app.status')[$value->status];
                }
            } else {

                $value->display_status = config('app.status')[$value->status];

            }

        }

        return view('student.details', compact(['student', 'courses']));

    }

    public function coursePause($id)
    {
        $courseStudent = CourseStudent::find($id);
        if (date('Y/m/09') == date('Y/m/d') && $courseStudent->course->term_type == 1) {
            PendingFee::where('course_id', $courseStudent->course_id)->where('user_id', $courseStudent->student_id)->first()->delete();
        }
        $status = new CourseStatus();
        $status->course_student_id = $id;
        $status->start_date = date('Y/m/d');
        $status->save();
        $courseStudent->update(['status' => 1]);

        return response()->json(array('status' => true, 'message' => "Course Paused !"), 200);
    }

    public function courseResume($id)
    {
        $status = CourseStatus::where('course_student_id', $id)->orderBy('id', 'desc')->first();

        $status->end_date = date('Y/m/d');
        $status->save();
        CourseStudent::find($id)->update(['status' => 0]);

        if (strtotime($status->start_date) <= strtotime(date('Y/m/01'))) {
            $fees = PendingFee::insert([
                'user_id' => $status->courseStudent->student_id,
                'course_id' => $status->courseStudent->course_id,
                'month' => date('m'),
                'year' => date('Y'),
                'fees' => $status->courseStudent->course->fees
            ]);
            if (!$fees)
                return response()->json(array('status' => false, 'message' => "Error In Pending Fees!"), 500);
        }

        return response()->json(array('status' => true, 'message' => "Course Resumed !"), 200);
    }

    public function checkFeesBeforeCompleteCourse($id, $UserId)
    {
        $course = CourseStudent::find($id);
        $pending = PendingFee::where('user_id', $UserId)->where('course_id', $course->course_id)->first();

        if ($pending) {
            return 'false';
        } else {
            return 'true';
        }
    }

    public function courseStop($id)
    {
        $course = CourseStudent::find($id);

        $course->end_date = date('m-Y');
        $course->status = 2;
        $course->save();

        PendingFee::where('user_id', $course->student_id)->where('course_id', $course->course_id)->delete();

        StudentDiscount::where('user_id', $course->student_id)->where('course_id', $course->course_id)->delete();

        return response()->json(array('status' => true, 'message' => "Course Stopped !"), 200);
    }

    public function courseHistory($id)
    {
        $statuses = CourseStatus::where('course_student_id', $id)->get();

        if ($statuses->count() > 0)
            $courseStudent = $statuses[0]->courseStudent;
        else
            $courseStudent = CourseStudent::find($id);

        $course = CourseStudent::find($id)->course;

        return view('student.status-history', compact(['statuses', 'course', 'courseStudent']));
    }

    public function addCourseToStudent(Request $request, $id)
    {
        $courses = array_filter($request->courses);
        $batches = array_filter($request->batches);
        $dates = array_filter($request->date);

        foreach ($courses as $key => $value) {

            $studentCourse = new CourseStudent();
            $studentCourse->student_id = Student::find($id)->user_id;
            $studentCourse->course_id = $value;
            $studentCourse->batch_id = $batches[$key];
            $studentCourse->start_date = $dates[$key];
            $studentCourse->save();

            $time = strtotime('01-' . $dates[$key]);
            $month = date("m", $time);
            $year = date("Y", $time);

            $pendingFees = new PendingFee();
            $pendingFees->user_id = $id;
            $pendingFees->course_id = $value;
            $pendingFees->month = $month;
            $pendingFees->year = $year;
            $pendingFees->fees = Course::find($value)->fees;
            $pendingFees->save();

        }

        if ($request->inquiry_id) {
            $inq = Inquiry::find($request->inquiry_id);
            $inq->is_registered = 1;
            $inq->save();
        }

        return response()->json(array('result' => "success", 'student_id' => $id), 200);

    }

    public function searchStudent(Request $request)
    {
        $id = $request->get('no');

        $user = AppUser::where('user', $id)->first();

        if ($user)
            return response()->json(array('result' => "success", 'user' => $user), 200);
        else
            return response()->json(array('result' => "not found"), 404);

    }

    public function changeFeeTypeView($id)
    {
        Admin::script(["
            $.getScript('/js/fee_type.js');
            "]);
        return Admin::content(function (Content $content) use ($id) {
            $user = AppUser::find($id);
            $studentCourse = CourseStudent::where('student_id', $id)->get();
            $courses = [];
            foreach ($studentCourse as $course) {
                if ($course->course->courseFeeType->count() > 1) {
                    $pending = PendingFee::where('user_id', $id)->where('course_id', $course->course_id)->first();
                    if ($pending && $pending->count() > 0)
                        $course->pendingFees = true;
                    $courses[] = $course;
                }
            }

            $content->header('Change Fee Type');
            $content->row(view('student.fee_type', compact(['courses', 'user'])));

        });
    }

    public function editFeeType(Request $request)
    {
        try {
            foreach ($request->course as $course) {
                if ($course['feeType']) {
                    $courseStudent = CourseStudent::find($course['courseStudentId']);
                    $courseStudent->fee_type = $course['feeType'];
                    $courseStudent->save();

                    $pending = PendingFee::where('course_id', $courseStudent['course_id'])->where('user_id', $courseStudent['student_id'])->first();
                    if ($pending) {
                        $courseFees = CourseFeeType::where('course_id', $courseStudent['course_id'])->where('fee_type_id', $course['feeType'])->first();
                        $pending->fees = $courseFees->fees;
                        $pending->fees_type = $course['feeType'];
                        $pending->save();
                    }
                }

            }

            admin_toastr(trans('admin.save_succeeded'));

            return redirect(url('students'));
        } catch (Exception $exception) {

        }

    }

    public function import($download = null)
    {
        if ($download) {
            return response()->download(public_path('master/student_bulk_upload.xls'));
        }

        return Admin::content(
            function (Content $content) {
                $content->header('Student Upload');
                $content->body(view('student.school.import'));
            });
    }

    protected function grid()
    {
        return Admin::grid(AppUser::class, function (Grid $grid) {

            $branch = null;
            $grid->disableRowSelector();

            $grid->model()->orderBy('id', 'desc');

            if (isRole('branch-admin'))
            {
                $branch = Branch::where('user_id',Admin::user()->id)->first();

                $grid->model()->whereHas('student',function ($q) use ($branch) {
                    $q->where('branch_id',$branch->id);
                });
            }

            $grid->model()->whereHas('student', function ($q) {
                $q->where('student_category', '!=', 1);
            });

            $grid->model()->whereHas("roles", function ($q)
            {
                $role = Role::where("slug", "student")->first();
                $id = ($role) ? $role->id : 0;
                $q->where('role_id', $id);
            });

            $grid->name('Name')->sortable();
            $grid->contact_no('Mobile')->sortable();
            $grid->column('id', 'Activities')->display(function ($id) {

                $data = CourseStudent::where('student_id', $id)->where('status', '!=', 2)->get();
                $string = '';
                if ($data->count() == 0)
                    return '-';
                foreach ($data as $key => $value) {
                    if ($value->status == 1)
                        $string .= '<span class="span-grid label" style="background-color: #f18917;">' . $value->course->activity->name . '</span>';
                    else
                        $string .= '<span class="span-grid label label-success">' . $value->course->activity->name . '</span>';
                }
                return $string;

            });

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableEdit();
                $actions->disableDelete();
                $actions->prepend('<a href="/students/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');
                $actions->prepend('<a class="btn btn-primary openModal" data-id="' . $actions->row->id . '"><i class="fa fa-eye"></i></a>');
                $actions->append('<a class="btn btn-danger delete-row" data-id="' . $actions->row->id . '"><i class="fa fa-trash"></i></a>');
                $actions->append('<a href="/students/' . $actions->row->id . '/fee-type" class="btn btn-success btn-sm" data-id="' . $actions->row->id . '" title="Change Fees Type"><i class="fa fa-exchange"></i></a>');
            });

            $excel_headers = ["Student Name","Mobile No","Activities Name"];
            $excel_columns = ["name","contact_no","id"];
            $grid->exporter(new ExcelExport("Students", $excel_headers, $excel_columns));

            $grid->filter(function ($filter) use($branch){

                $filter->disableIdFilter();
                $filter->like('name');
                $filter->like('contact_no','Mobile No');
                if(isset($branch))
                {
                    $filter->where(function($q){
                        $q->whereHas('student',function ($q2){
                            $q2->whereHas('courseStudents',function ($q3){
                                $q3->where('course_id',$this->input);
                            });
                        });
                    }, "Course")->select(Course::join('activities','activities.id','=','courses.activity_id')->where('branch_id',$branch->id)->select('activities.name','courses.id')->pluck("name", "id")->toArray());
                }
                else{
                    $filter->where(function($q){
                        $q->whereHas('student',function ($q2){
                            $q2->whereHas('courseStudents',function ($q3){
                                $q3->where('course_id',$this->input);
                            });
                        });
                    }, "Course")->select(Course::join('activities','activities.id','=','courses.activity_id')->select('activities.name','courses.id')->pluck("name", "id")->toArray());

                }


            });

            $grid->paginate(10);
        });
    }

    public function refund(Request $request)
    {
        $userId = $request->query('userid');
        $courseId = $request->query('courseid');
        $student = AppUser::where('id',$userId)->first();
        $courseStudent = CourseStudent::where('id',$courseId)->first();
        $feeType = config('app.fee_type.'.$courseStudent->fee_type.'.name');
        $totalPaidFee = Fee::join('fee_details','fee_details.fee_id','=','fees.id')->select('paid_amount')->where('user_id',$student->id)->where('course_id',$courseStudent->course_id)->sum('paid_amount');
        $pendingFee = PendingFee::where('user_id',$student->id)->where('course_id',$courseStudent->course_id)->sum('fees');

        return Admin::content(function (Content $content) use($student,$courseStudent,$feeType,$totalPaidFee,$pendingFee) {
            $content->header('Students');

            $content->row(function (Row $row) use($student,$courseStudent,$feeType,$totalPaidFee,$pendingFee){
                $row->column(4,view('student.refund',compact('student','courseStudent','feeType','totalPaidFee','pendingFee')));
            });
        });
    }

    public function refundPostAction(Request $request)
    {
        $error = [];
        $validate = Validator::make($request->all(),
            [
                "userId" => 'required|numeric',
                "courseId" => 'required|numeric',
                "refundAmount" => 'required|numeric'
            ],
            [
                "city_id.required" => "The Refund Amount field is required."
            ]
        );

        if ($validate->fails()) {

            $error['error'] = $validate->getMessageBag();
            return response()->json($error, 406);

        }
        else{
            $userId = $request->get('userId');
            $courseId = $request->get('courseId');
            $refundAmount = $request->get('refundAmount');

            $fee_info = Fee::create([
                "user_id" => $userId,
                "payment_date" => date("Y-m-d H:i:s"),
                "net_amount" => $refundAmount,
                "lump_sum_amount" => 0,
                "payment_mode" => 0,
                "payment_type" => 1
            ]);

            FeeDetail::create([
                "fee_id" => $fee_info->id,
                "course_id" => $courseId,
                "discount" => 0,
                "paid_amount" => $refundAmount
            ]);

            $pendingFee = PendingFee::where('user_id',$userId)->where('course_id',$courseId)->get();
            if(isset($pendingFee))
            {
                $pendingFee->delete();
            }
            return "success";
        }
    }

}
