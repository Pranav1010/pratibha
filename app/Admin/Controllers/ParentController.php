<?php

namespace App\Admin\Controllers;

use App\Branch;
use App\Parents;
use App\Profession;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Layout\Row;
use App\City;
use App\Admin\Extensions\Tools\GridSearch;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Auth\Database\Role;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Admin\Extensions\Tools\ExcelExport;

class ParentController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["
            $('.city_id').closest('.form-group').append(\"<a href='#' class='btn btn-default add-button' data-modal='add-city-modal' onclick='openModal(this, event)'><i class='fa fa-plus'></i></a>\");
            $('.profession').closest('.form-group').append(\"<a href='#' class='btn btn-default add-button' data-modal='add-profession-modal' onclick='openModal(this, event)'><i class='fa fa-plus'></i></a>\");
            $.getScript('/js/parent.js');
        "]);

    }

    public function index()
    {
        return Admin::content(function (Content $content) {

            Admin::script(["
                $('form').attr('action', '/parents');
            "]);

            $content->header('Parents');

            $content->row(function (Row $row) {
//                $row->column(5, $this->form());
                $row->column(12, $this->grid());
                $row->column(12,view('parents.model'));
            });
        });
    }

    public function show($id)
    {
        $parents = Parents::find($id);
        return view('parents.model-body',compact('parents'));
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Parents');
            $content->row(view("addModal.addCity"));
            $content->row(function (Row $row) use ($id) {
                $row->column(12, $this->form($id)->edit($id));

//                $row->column(7, $this->grid());
            });
        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Parents');
//            $content->description('description');

            $content->row($this->form());
            $content->row(view("addModal.addCity"));
            $content->row(view("addModal.addProfession"));
        });
    }

    public function addParent(Request $request)
    {

        $validate = Validator::make($request->all(),
            [
                'name' => 'required',
                'address' => 'required',
                'branch_id' => 'required',
            ],
            [
                //'parent.profession.required' => "The Profession field is required",
            ]
        );

        if ($validate->fails()) {

            $error['error'] = $validate->getMessageBag();
            return response()->json($error, 406);

        } else {

            DB::beginTransaction();

            try {

                $parent = Parents::updateOrCreate(
                    [
                        "father_mobile" => $request->mobile,
                        "father_name" => ucwords($request->name),
                        "father_email" => $request->email,
                        "city_id" => $request->city_id,
                        "address" => $request->address,
                        "father_profession" => $request->parent['profession'],
                        "mother_name" => ucwords($request->parent['mother_name']),
                        "mother_email" => $request->parent['mother_email'],
                        "mother_mobile" => $request->parent['mother_mobile'],
                        "mother_profession" => $request->parent['mother_profession'],
                        "father_blood_group" => $request->parent['father_blood_group'],
                        "mother_blood_group" => $request->parent['mother_blood_group'],
                        "aadhar_no_father" => $request->parent['aadhar_no_father'],
                        "aadhar_no_mother" => $request->parent['aadhar_no_mother'],
                        "branch_id" => $request->branch_id
                    ]
                );

                DB::commit();

                return json_encode(["id" => $parent->id, "name" => $parent->father_name, "mobile" => $parent->father_mobile]);

            } catch (Exception $exception) {

                DB::rollBack();
                return null;

            }
        }

    }

    protected function grid()
    {
        return Admin::grid(Parents::class, function (Grid $grid) {

            $grid->disableRowSelector();

            // Only Parents ------
            $grid->model()->orderBy('id', 'desc');

            if(isRole('branch-admin'))
            {
                $branch = Branch::where('user_id',Admin::user()->id)->first();
                $grid->model()->where('branch_id',$branch->id);
            }

            $grid->father_name()->sortable();

            $grid->father_mobile();

            $grid->mother_name();

            $grid->column('city.name', 'City')->display(function ($name) {
                return ($name) ? $name : '-';
            });

            $grid->actions(function (Grid\Displayers\Actions $actions) {
                $actions->disableEdit();
                $actions->prepend('<a href="/parents/' . $actions->row->id . '/edit"><i class="fa fa-edit"></i></a>');
                $actions->prepend('<a href="#" data-id="' . $actions->row->id . '" class="btn open-branch-view-modal" style="background-color: #01a4f8; border: #01a4f8;"><i class="fa fa-eye"></i></a>');
            });

            $excel_headers = ["Father Name","Father Mobile","Mother Name","City Name"];
            $excel_columns = ["father_name","father_mobile","mother_name","city.name"];
            $grid->exporter(new ExcelExport("Parents", $excel_headers, $excel_columns));

            $grid->filter(function ($filter){

                $filter->disableIdFilter();

                $filter->like("father_name");
                $filter->like("father_mobile");
                $filter->like("mother_name");


                $filter->where(function($q){
                    $q->where('city_id',$this->input);
                }, "City")->select(City::pluck("name", "id")->toArray());
            });

            $grid->paginate(10);
        });
    }

    protected function form($id = null)
    {
        return Admin::form(Parents::class, function (Form $form) use ($id) {

            $form->tools(function ($tools){
                $tools->disableListButton();
            });

            $form->html(' <h4>Father\'s Information</h4>', '<img src="' . url("img/male.png") . '" width="25px">');

            $form->text('father_name', 'Name')->rules('required')->attribute('autofocus');
            $form->mobile('father_mobile', 'Mobile')->options(['mask' => '9999999999'])->rules(function ($form){

                // If it is not an edit state, add field unique verification
                if (!$id = $form->model()->id) {
                    return 'required|numeric|unique:parents,father_mobile';
                }
                return 'required|numeric|unique:parents,father_mobile,'.$form->model()->id;
            });
            $form->text('father_email', 'Email');
            $form->select('father_profession', 'Profession')->setElementClass(['profession'])->options(Profession::pluck('name', 'id'))->placeholder('Input Profession');
            $form->select('father_blood_group', 'Blood Group')->options(config('app.blood_group'));
            $form->text('aadhar_no_father', 'Aadhar No');

            $form->html('<h4> Mother\'s Information </h4>', '<img src="' . url("img/user.png") . '" width="25px">');

            $form->text('mother_name', 'Name');
            $form->mobile('mother_mobile', 'Mobile')->options(['mask' => '9999999999']);
//            $form->html('<input type="checkbox" name="primary_mother" id="primary_mother"></div>','Make Primary Contact');
            $form->text('mother_email', 'Email');
            $form->select('mother_profession', 'Profession')->setElementClass(['profession'])->options(Profession::pluck('name', 'id'))->placeholder('Input Profession');
            $form->select('mother_blood_group', 'Blood Group')->options(config('app.blood_group'));
            $form->text('aadhar_no_mother', 'Aadhar No');

            $form->html('<hr>');
            $form->html('<h4> Location </h4>', '<img src="' . url("img/location.png") . '" width="25px">');

            $form->select('city_id', 'City')->options(City::pluck('name', 'id'));
            $form->textarea('address', 'Address')->rules('required');

            if(Admin::user()->isAdministrator() || isRole('super-administrator'))
            {
                $form->select('branch_id', 'Branch')->options(Branch::pluck('name', 'id'))->rules('required');
            }
            else
            {
                $branch = Branch::where('user_id',Admin::user()->id)->first();
                $form->hidden('branch_id')->default($branch->id);
            }

            $form->saving(function ($form){
                $form->father_name = ucwords($form->father_name);
                $form->mother_name = ucwords($form->mother_name);

            });

        });
    }

    public function update($id)
    {
        return $this->form($id)->update($id);
    }
}
