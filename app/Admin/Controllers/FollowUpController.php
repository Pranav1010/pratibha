<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\Tools\FollowUpGridSearch;
use App\Admin\Extensions\Tools\GridSearch;
use App\Admin\Extensions\Tools\ViewInquiry;
use App\FollowUp;
use App\FollowUpAction;
use App\FollowUpStatus;
use App\Http\Controllers\Controller;
use App\Inquiry;
use App\InterestLevel;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;

class FollowUpController extends Controller
{
    use ModelForm;

    public function __construct()
    {
        Admin::script(["
            $.getScript('/js/script.js');
            $('input').css('width', '100%');
            $('.interest_level_id').closest('.form-group').append(\"<a href='#' class='btn btn-default add-button' data-modal='add-interest-level-modal' onclick='openModal(this, event)'><i class='fa fa-plus'></i></a>\");
            $('.follow_up_status_id').closest('.form-group').append(\"<a href='#' class='btn btn-default add-button' data-modal='add-followup-status-modal' onclick='openModal(this, event)'><i class='fa fa-plus'></i></a>\");
            $('.follow_up_action_id').closest('.form-group').append(\"<a href='#' class='btn btn-default add-button' data-modal='add-followup-action-modal' onclick='openModal(this, event)'><i class='fa fa-plus'></i></a>\");
            
            $('h1').append(\"<a class='btn btn-sm btn-primary pull-right' style='margin-right: 25px' href='/inquiries'><i class='fa fa-arrow-left'></i>&nbsp;&nbsp;Back</a>\");
        "]);
    }

    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Follow Ups');

            Admin::script(["
                $('form').attr('action', '/followUp');
            "]);

            $content->row(function (Row $row) {
                $row->column(5, $this->form());
                $row->column(7, $this->grid());
            });

            $content->row(view("inquiry.modal-view"));
            $content->row(view("addModal.addInterestLevel"));
            $content->row(view("addModal.addFollowupStatus"));
            $content->row(view("addModal.addFollowupAction"));
        });
    }


    public function edit(FollowUp $followUp)
    {
        return Admin::content(function (Content $content) use ($followUp) {

            Admin::script(["
                $('.col-md-8 .box-header .btn-group.pull-right a').attr('href', '/followUp');
            "]);

            $content->header('Follow Ups');

            $content->row(function (Row $row) use ($followUp) {
                $row->column(4, $this->form($followUp->id)->edit($followUp->id));
                $row->column(8, $this->grid($followUp->id));
            });
        });
    }

    protected function grid($id = null)
    {
        return Admin::grid(FollowUp::class, function (Grid $grid) use ($id) {

            $grid->column('interestLevel.name', 'Interest Level');
            $grid->column('followUpStatus.name', 'Follow Up Status');
            $grid->column('followUpAction.name', 'Follow Up Action');
            $grid->next_follow_up('Next Follow Up Date')->display(function ($next_follow_up){
                return Carbon::createFromFormat('Y-m-d H:i:s',$next_follow_up)->format('d/m/Y h:i a');
            });
            $grid->comments('Comments');

            if (isRole("administrator") || isRole("counselor") || isRole("receptionist")) {
                $grid->actions(function ($actions) {
                    $actions->disableDelete();
                    $actions->disableEdit();
                    $actions->prepend('<a href="/followUp/' . $actions->row->id . '/edit?inquiry_id=' . $actions->row->inquiry_id . '"><i class="fa fa-edit"></i></a>');
                });
            } else {
                $grid->disableActions();
            }

            $value = Input::get('search');

            $q = $grid->model();

            $q->orderBy("id", "desc");

            if (!empty($value)) {

                $q->orWhereHas("interestLevel", function ($query) use ($value) {
                    $query->where("name", "like", "%{$value}%");
                });

                $q->orWhereHas("followUpStatus", function ($query) use ($value) {
                    $query->where("name", "like", "%{$value}%");
                });

                $q->orWhereHas("followUpAction", function ($query) use ($value) {
                    $query->where("name", "like", "%{$value}%");
                });

                $q->orWhere("next_follow_up", "like", "%{$value}%");

                $q->orWhere("comments", "like", "%{$value}%");

            }

            $inquiry_id = Input::get('inquiry_id');

            $q->where("inquiry_id", $inquiry_id)->whereHas("inquiry", function ($que) {
                $que->where("is_registered", 0);
            });

            $grid->disableExport();
            $grid->disableFilter();
            $grid->disableRowSelector();

            if (!isset($id)) {
                $grid->disableCreateButton();
            }

            $grid->tools(function ($tools) {
                $tools->append(new GridSearch());
            });

            $grid->tools(function ($tools) {
                $tools->append(new ViewInquiry());
            });

        });
    }

    protected function form($id = null)
    {
        return Admin::form(FollowUp::class, function (Form $form) use ($id) {

            $inquiry_id = Input::get('inquiry_id');

            $form->hidden('inquiry_id', "Interest Level")->default($inquiry_id);
            $form->select('interest_level_id', "Interest Level")->options(InterestLevel::pluck("name", "id"))->rules('required');
            $form->select('follow_up_status_id', "Status")->options(FollowUpStatus::pluck("name", "id"))->rules('required');
            $form->select('follow_up_action_id', "Action")->options(FollowUpAction::pluck("name", "id"))->rules('required');
            $form->datetime('next_follow_up')->format("DD/MM/YYYY hh:mm a")->rules(['required',function($attribute, $value, $fail)
                                                                                                    {
                                                                                                        $eventTimestamp = strtotime(Carbon::createFromFormat('d/m/Y h:i a', $value)->toDateTimeString());
                                                                                                        $currentTimestamp = strtotime('now');
                                                                                                        if ($eventTimestamp < $currentTimestamp) {
                                                                                                            return $fail('Please Enter Future Date.');
                                                                                                        }
                                                                                                    }]);
            $form->textarea('comments')->rows(3);

            //$form->setWidth(8, 3);

            $form->saving(function ($form){
                $form->next_follow_up = Carbon::createFromFormat('d/m/Y h:i a',$form->next_follow_up)->format('Y-m-d H:i:s');
            });

            $form->saved(function () use ($inquiry_id) {
                admin_toastr(trans('admin.save_succeeded'));
                return redirect("/followUp?inquiry_id=" . $inquiry_id);
            });

            $form->tools(function (Form\Tools $tools) {
                $tools->disableBackButton();
                $tools->disableListButton();
            });
        });
    }
}
