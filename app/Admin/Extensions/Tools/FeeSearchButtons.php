<?php
/**
 * Created by PhpStorm.
 * User: PRANAV
 * Date: 02-Apr-18
 * Time: 11:57 AM
 */

namespace App\Admin\Extensions\Tools;

use Encore\Admin\Admin;
use Encore\Admin\Grid\Tools\AbstractTool;
use Illuminate\Support\Facades\Request;

class FeeSearchButtons extends AbstractTool
{
    protected function script()
    {
        $url = Request::fullUrlWithQuery(['type' => '_type_']);

        return <<<EOT
            $('.fee-filter').on('click', function() {
            
                var type = $(this).data('type');
                var url = "$url".replace("_type_", type);
                $.pjax({container:'#pjax-container', url: url });
            
            });
EOT;
    }

    public function render()
    {
        Admin::script($this->script());

        return view('vendor.admin.tools.feeSearchButtons');
    }
}