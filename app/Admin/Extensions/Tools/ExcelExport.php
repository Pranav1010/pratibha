<?php
/**
 * Created by PhpStorm.
 * User: PRANAV
 * Date: 03-Jul-18
 * Time: 3:15 PM
 */

namespace App\Admin\Extensions\Tools;

use App\Batch;
use App\Course;
use App\Fee;
use App\FollowUp;
use Encore\Admin\Grid\Exporters\AbstractExporter;
use Maatwebsite\Excel\Facades\Excel;
use App\CourseStudent;
use  App\Student;
use Carbon\Carbon;

class ExcelExport extends AbstractExporter
{

    public $filename, $headers, $columns;

    public function __construct($filename, $headers, $columns)
    {
        $this->filename = $filename;
        $this->headers = $headers;
        $this->columns = $columns;
    }

    public function export()
    {
        Excel::create($this->filename, function ($excel) {

            $excel->sheet('SHEET-1', function ($sheet) {

                //Add header as a first row.
                //$sheet->prependRow(1, $this->headers);


                $rows = collect($this->getData())->map(function ($item) {
                    if($this->filename === "Inquiries") {
                        $courseNames = Course::join('inquiry_courses','inquiry_courses.course_id','=','courses.id')->join('activities','activities.id','=','courses.activity_id')->select('activities.name')->pluck("activities.name")->toArray();
                        $item['courses'] = implode(", ", $courseNames);
                    }
                    elseif($this->filename === "Batches")
                    {
                        $activity = new Batch();
                        $item['course_id'] = $activity->activityName($item['course_id']);
                    }
                    elseif($this->filename === "Students")
                    {
                        $data = CourseStudent::where('student_id', $item['id'])->where('status', '!=', 2)->get();
                        $string = '';

                            foreach ($data as $key => $value)
                            {
                                if ($value->status == 1)
                                    $string .= $value->course->name;
                                else
                                    $string .= $value->course->name;
                            }
                            $item['id'] = $string;

                    }
                    elseif($this->filename === "Membership")
                    {
                        $status =  Student::getMembershipStatus($item["user_id"]);
                        $result ='';
                        if($status >= 0)
                        {
                            if($status){
                                $result = ($status + 1);
                            } else{
                                $result = 'Active';
                            }
                        }
                        else{
                            if($status == -2){
                                $result = 'New Student';
                            } else {
                                $result = 'Expired';
                            }
                        }

                        $item['membership_status'] = $result;
                    }
                    elseif($this->filename === "Dashboard")
                    {
                        $followUp = FollowUp::find($item['id']);
                        $item['interest_level_id'] = $followUp->interestLevel->name;
                        $item['follow_up_status_id'] = $followUp->followUpStatus->name;
                        $item['follow_up_action_id'] = $followUp->followUpAction->name;
                        $item['updated_at'] = $followUp->getNumberOfFollowUps($item['id']);
                        $date = Carbon::parse($item['created_at']);
                        $item['created_at'] = $followUp->getDaysFromStartDateOfInquiry($date);

                    }
                    elseif($this->filename === "Fees")
                    {
                        $fee = new Fee();
                        $item['student_category'] = $fee->paidAmount($item['id']);
                        $item['second_contact_no'] = $fee->totalDiscount($item['id']);
                        $item['gender'] = $fee->pendingAmount($item['id']);
                        $item['fee_status'] = config("app.fee_status." . $item['fee_status'] . ".name");
                    }

                    $item = array_dot($item);
                    return $item;
                })->toArray();


                $sheet->loadView('excel.export')->with("rows", $rows)
                    ->with("headers", $this->headers)
                    ->with("columns", $this->columns);

            });

        })->export('xls');
    }

}