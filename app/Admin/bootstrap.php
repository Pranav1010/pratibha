<?php

/**
 * Laravel-admin - admin builder based on Laravel.
 * @author z-song <https://github.com/z-song>
 *
 * Bootstraper for Admin.
 *
 * Here you can remove builtin form field:
 * Encore\Admin\Form::forget(['map', 'editor']);
 *
 * Or extend custom form field:
 * Encore\Admin\Form::extend('php', PHPEditor::class);
 *
 * Or require js and css assets:
 * Admin::css('/packages/prettydocs/css/styles.css');
 * Admin::js('/packages/prettydocs/js/main.js');
 *
 */

Encore\Admin\Form::forget(['map', 'editor']);


$script = <<<SCRIPT

$().ready(function() 
{
    
function matchStart(params, data)
{
    params.term = params.term || '';
    if (data.text.toUpperCase().indexOf(params.term.toUpperCase()) == 0) {
        return data;
    }
    return false;
}


$('select').select2(
    {
        matcher: function(params, data)
        {
            return matchStart(params, data);
        }
    }
);

});

    //Delete Row Operation
    $(".grid-row-delete").addClass("delete-row");
    $(".grid-row-delete").addClass("btn");
    $(".grid-row-delete").removeClass("grid-row-delete");

$('.delete-row').click(function(){
    var url = window.location.pathname.split("/");
    
    if(url[1] == 'auth')
    {
        var controller = 'auth/users';
    }
    else
    {
        var controller = url[1];
    }
    
    var id = $(this).data('id');

    swal({
      title: "Do you want to delete this item?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Confirm",
      closeOnConfirm: false,
      cancelButtonText: "Cancel"
    },
    function(){
        $.ajax({
            method: 'post',
            url: '/'+ controller +'/'+ id,
            data: {
                _method:'delete',
                _token:LA.token,
            },
            success: function (data) {
                $.pjax.reload('#pjax-container');

                if (typeof data === 'object') {
                    if (data.status) {
                        swal(data.message, '', 'success');
                    } else {
                        swal(data.message, '', 'error');
                    }
                }
            }
        });
    });
    
    
    
    
});

//tabindex for select2
setTimeout(function(){
    $('select').removeAttr('tabindex');
},200);

$(document).on('focus','select',function(){ 
      
      
       if($(this).hasClass( "form-control"))
       {
            $(this).select2('open'); 
       }
       
    });

$('select').on("select2:close", function (e) {
   
  var formGroup = $(this).parents('.form-group');
  
  var nextFormGroup = formGroup.next();
  
  
  selectFocus(nextFormGroup);
  
  
  function selectFocus(nextFormGroup)
  {
      if(nextFormGroup.has('textarea,input[type=text],input[type=checkbox],input[type=radio],select,button').length > 0)
      {
        nextFormGroup.find('textarea,input[type=text],input[type=checkbox],input[type=radio],select,button').focus();
      }  
      else
      {
        nextFormGroup.next().find('textarea,input[type=text],input[type=checkbox],input[type=radio],select,button').focus();
      }
  }
});

SCRIPT;

Admin::script($script);