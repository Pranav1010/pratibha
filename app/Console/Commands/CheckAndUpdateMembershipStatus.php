<?php

namespace App\Console\Commands;

use App\Course;
use App\Fee;
use App\FeeDetail;
use App\PendingFee;
use App\Student;
use App\StudentDiscount;
use App\User;
use DateTime;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CheckAndUpdateMembershipStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CheckAndUpdateMembershipStatus:updateMembership';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check and update membership status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $students = Student::where("membership_status", 1)->whereHas("courseStudents", function($q){
            $q->where("status", 0);
        })->get();

        DB::transaction(function () use ($students) {

            foreach ($students as $student) {
                $membershipDate = $student->membership_date;
                $membership_discount = StudentDiscount::where("user_id", $student->user_id)->where("course_id", 1)->value("discount_amount");
                $membership_discount = $membership_discount ? $membership_discount : 0;

                date_default_timezone_set('Asia/Kolkata');
                if ($membershipDate) {

                    $date1 = new DateTime($membershipDate);
                    $date2 = new DateTime(date("Y-m-d"));

                    $diff = $date2->diff($date1);

                    $year = $diff->y;

                    if ($year > 0) {

                        $student->membership_status = 0;
                        $student->save();

                        $advance = $student->user->advance_amount;
                        $membership_fee = Course::withoutGlobalScope("removeMembership")->find(1)->fees;

                        if ($advance > 0) {

                            $fees = Fee::create([
                                "user_id" => $student->user_id,
                                "payment_date" => date("Y-m-d"),
                                "net_amount" => 0,
                                "lump_sum_amount" => 0,
                                "payment_mode" => 0,
                            ]);

                            if (($advance + $membership_discount) >= $membership_fee) {

                                $user = User::find($student->user_id);
                                $user->advance_amount = $advance + $membership_discount - $membership_fee;
                                $user->save();

                                FeeDetail::create([
                                    "fee_id" => $fees->id,
                                    "course_id" => 1,
                                    "discount" => $membership_discount,
                                    "paid_amount" => $membership_fee - $membership_discount
                                ]);

                            } else {

                                $user = User::find($student->user_id);
                                $user->advance_amount = 0;
                                $user->save();

                                FeeDetail::create([
                                    "fee_id" => $fees->id,
                                    "course_id" => 1,
                                    "discount" => $membership_discount,
                                    "paid_amount" => $advance - $membership_discount
                                ]);

                                PendingFee::create([
                                    "user_id" => $student->user_id,
                                    "course_id" => 1,
                                    "month" => date("m"),
                                    "year" => date("Y"),
                                    "fees" => $membership_fee - $advance - $membership_discount
                                ]);
                            }
                        } else {

                            PendingFee::create([
                                "user_id" => $student->user_id,
                                "course_id" => 1,
                                "month" => date("m"),
                                "year" => date("Y"),
                                "fees" => $membership_fee
                            ]);
                        }
                    }
                }
            }
        });
    }
}
