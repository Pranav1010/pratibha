<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Laravel\Dusk\DuskServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use Illuminate\Support\Facades\Auth;
use Validator;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        $this->registerValidators();
        Schema::defaultStringLength(191);

        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            if(Auth::user()->id === 1){
                $event->menu->add([
                    'text' => 'Manage Staff',
                    'url' => 'users',
                    'icon' => 'user',
                ]);
            }
        });
    }

    protected function registerValidators()
    {
        # Custom validator to check greater than
        Validator::extend('is_user', function ($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();
            if($data['username'] == null)
                return true;
            else{
                $user = User::where('username',$data['username'])->first();
                if($user)
                    return true;
                else
                    return false;
            }
        });

        Validator::replacer('is_user', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':field', 'usename' , $message);
        });
    }

    public function register()
    {
        //
    }
}
