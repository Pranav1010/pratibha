<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        return view('role.role', compact('roles'));
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => [
                'unique:roles,name',
                'required'
            ]

        ]);

        Role::create(request(['name','description']));

        return redirect()->back()->with('data', ['type' => 'success', 'message' => 'Added Successfully']);
    }


    public function show($id)
    {

    }

    public function edit(Role $role)
    {
        return view('role.editRole', compact('role'));
    }

    public function update(Role $role)
    {
        $this->validate(request(), [
            'name' => [
                'required',
                Rule::unique('roles')->ignore($role->id)
            ],

        ]);

        $role->name = request()->name;
        $role->description = request()->description;

        $role->save();

        return redirect('/roles')->with('data', ['type' => 'success', 'message' => 'Updated Successfully']);
    }

    public function destroy(Role $role)
    {
        $role->delete();
        return redirect('/roles')->with('data', ['type' => 'error', 'message' => 'Deleted Successfully']);

    }
}
