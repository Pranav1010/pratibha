<?php

namespace App\Http\Controllers;

use App\Area;
use App\FollowUp;
use App\Inquiry;
use App\User;
use App\Reason;
use App\InquiryWiseUser;
use Illuminate\Support\Facades\Response;
use Session;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Request;

class InquiriesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Session::has('user_id')){
            $userId = Session::get('user_id');
        }
        else {
            $userId = Auth::id();
        }

        if ($userId == 1) {
            $inquiries = Inquiry::leftJoin('reasons', function($join){
                $join->whereRaw("inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = inquiries.id)");
            })->join("inquiry_wise_users", "inquiries.id", "=", "inquiry_wise_users.inquiry_id")->select('inquiries.*', 'reasons.reason AS newReason', 'reasons.assignor_id', 'inquiry_wise_users.id AS inquiry_wise_user_id')->orderBy('status', 'asc')->orderBy('inquiries.updated_at', 'desc')->get();
        } else {
            $inquiries = Inquiry::leftJoin('reasons', function($join){
                $join->whereRaw("inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = inquiries.id)");
            })->join("inquiry_wise_users", "inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->select('inquiries.*', 'reasons.reason AS newReason', 'reasons.assignor_id', 'inquiry_wise_users.id AS inquiry_wise_user_id')->orderBy('status', 'asc')->orderBy('updated_at', 'desc')->get();
        }

        //dd($inquiries[0]);

	/*foreach($inquiries as $inquiry){
		dd($inquiry);
	}*/
        return view('inquiry.inquiries', compact('inquiries', 'userId'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('inquiry.createInquiry', compact('users', 'courses', 'branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

    	if(!empty(request()->otherSource))
    	    $source = request()->otherSource;
    	else
    	    $source = request()->source;

        $inquiry = Inquiry::create([
            'branch_id' => request()->branch_id,
            'course_id' => request()->course_id,
            'name' => request()->name,
            'email' => request()->email,
            'mobile' => request()->mobile,
            'city' => request()->city,
            'area' => request()->area,
            'pincode' => request()->pincode,
            'country' => request()->country,
            'source' => $source,
            'remarks' => request()->remarks
        ]);

    	if(count(request()->user_id) > 1 || !empty(request()->reason)){

            if(!empty(request()->reason)){
                $reason = request()->reason;
            } else {
                $reason = "Reason not available.";
            }

    		Reason::create([
    		    'assignor_id' => Auth::id(),
    		    'inquiry_id' => $inquiry->id,
    	            'reason' => $reason
    		]);

    	}

        foreach (request()->user_id as $user_id) {

            InquiryWiseUser::create([
                'inquiry_id' => $inquiry->id,
                'user_id' => $user_id
            ]);

        }

        return redirect('/inquiries');
    }

    /**
     * Display the specified resource.
     *
     * @param  Inquiry $inquiry
     * @return \Illuminate\Http\Response
     */
    public function show(Inquiry $inquiry)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Inquiry $inquiry
     * @return \Illuminate\Http\Response
     */
    public function edit(Inquiry $inquiry)
    {
        $users = InquiryWiseUser::where('inquiry_id', '=', $inquiry->id)->pluck('user_id')->toArray();

        return view('inquiry.editInquiry', compact('inquiry', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Inquiry $inquiry
     * @return \Illuminate\Http\Response
     */
    public function update(Inquiry $inquiry)
    {
    	if(!empty(request()->otherSource))
    	    $source = request()->otherSource;
    	else
    	    $source = request()->source;

        $inquiry->update([
            'branch_id' => request()->branch_id,
            'course_id' => request()->course_id,
            'name' => request()->name,
            'email' => request()->email,
            'mobile' => request()->mobile,
            'city' => request()->city,
            'area' => request()->area,
            'pincode' => request()->pincode,
            'country' => request()->country,
            'source' => $source,
            'remarks' => request()->remarks,
            'status' => request()->status
        ]);

        InquiryWiseUser::where('inquiry_id', '=', $inquiry->id)->delete();

        foreach (request()->user_id as $user_id) {

            InquiryWiseUser::create([
                'inquiry_id' => $inquiry->id,
                'user_id' => $user_id
            ]);

        }

    	if(count(request()->user_id) > 1 || !empty(request()->reason)){

            if(!empty(request()->reason)){
                $reason = request()->reason;
            } else {
                $reason = "Reason not available.";
            }

            Reason::create([
                'assignor_id' => Auth::id(),
                'inquiry_id' => $inquiry->id,
                    'reason' => $reason
            ]);

        }

        return redirect('/inquiries');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Inquiry $inquiry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inquiry $inquiry)
    {
        if(Session::has('user_id')){
            $userId = Session::get('user_id');
        }
        else {
            $userId = Auth::id();
        }

        $inquiry_wise_user_id = InquiryWiseUser::where('inquiry_id', '=', $inquiry->id)->where('user_id', '=', $userId)->value('id');
        InquiryWiseUser::where('inquiry_id', '=', $inquiry->id)->where('user_id', '=', $userId)->delete();
        FollowUp::where('inquiry_wise_user_id', '=', $inquiry_wise_user_id)->delete();
        $inquiry->delete();

        return redirect('/inquiries');
    }

    public function completedInquiries()
    {
    	if(Session::has('user_id')){
            $userId = Session::get('user_id');
        }
        else {
            $userId = Auth::id();
        }

    	if($userId != 1){
        	$inquiries = Inquiry::leftJoin('reasons', function($join){
                $join->whereRaw("inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = inquiries.id)");
            })->join("inquiry_wise_users", "inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->where('inquiries.status', '=', 1)->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();
        }else{
        	$inquiries = Inquiry::leftJoin('reasons', function($join){
                $join->whereRaw("inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = inquiries.id)");
            })->where('inquiries.status', '=', 1)->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();
        }
        return view('/inquiries', compact("inquiries", "userId"));
    }

    public function cancelledInquiries()
    {
        if(Session::has('user_id')){
            $userId = Session::get('user_id');
        }
        else {
            $userId = Auth::id();
        }

    	if($userId != 1){
        	$inquiries = Inquiry::leftJoin('reasons', function($join){
                $join->whereRaw("inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = inquiries.id)");
            })->join("inquiry_wise_users", "inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->where('inquiries.status', '=', 2)->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();
        }else{
            $inquiries = Inquiry::leftJoin('reasons', function($join){
                $join->whereRaw("inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = inquiries.id)");
            })->where('inquiries.status', '=', 2)->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();
        	/*$inquiries = Inquiry::where("status", "=", 2)->get();*/
        }
        return view('/inquiries', compact("inquiries", "userId"));
    }

    public function forwardedInquiries()
    {
        if(Session::has('user_id')){
            $userId = Session::get('user_id');
        }
        else {
            $userId = Auth::id();
        }

        if($userId != 1){
            $inquiries = Inquiry::leftJoin('reasons', function($join){
                $join->whereRaw("inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = inquiries.id)");
            })->join("inquiry_wise_users", "inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->groupBy('reasons.inquiry_id')->orderBy('status', 'asc')->orderBy('inquiries.updated_at', 'desc')->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();
        }else{
            $inquiries = Inquiry::leftJoin('reasons', function($join){
                $join->whereRaw("inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = inquiries.id)");
            })->groupBy('reasons.inquiry_id')->orderBy('status', 'asc')->orderBy('inquiries.updated_at', 'desc')->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();
            /*$inquiries = Inquiry::join('reasons', 'inquiries.id', '=', 'reasons.inquiry_id')->where('inquiries.status', '=', 0)->groupBy('reasons.inquiry_id')->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get();*/
        }
        return view('/inquiries', compact("inquiries", "userId"));
    }


    /** *******************************************************
     * Methods for followup
     ******************************************************* */

    /**
     * Show the form for creating a new follow up.
     *
     * @param  Inquiry $inquiry
     * @return \Illuminate\Http\Response
     */
    public function followUp(Inquiry $inquiry)
    {
        $inquiry_wise_user_id = request()->inquiry_wise_user_id;
        if(Session::has('user_id')){
            $userId = Session::get('user_id');
        }
        else {
            $userId = Auth::id();
        }

        $followUps = FollowUp::join('inquiry_wise_users','follow_ups.inquiry_wise_user_id','=','inquiry_wise_users.id')->where('inquiry_wise_users.inquiry_id', '=', $inquiry->id)->orderBy('follow_ups.id', 'desc')->select('follow_ups.*', 'inquiry_wise_users.user_id')->get();

        $inquiry->userName = Inquiry::getUserNames($inquiry->id, $userId);

        $reasons = Reason::join('inquiries', 'reasons.inquiry_id', '=', 'inquiries.id')->where('reasons.inquiry_id', '=', $inquiry->id)->select('reasons.*')->orderBy('reasons.id', 'desc')->get();

        return view('inquiry.followUp', compact('inquiry', 'followUps', 'reasons', 'inquiry_wise_user_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  Inquiry $inquiry
     * @return \Illuminate\Http\Response
     */
    public function createFollowUp(Inquiry $inquiry)
    {
        /*$inquiry_wise_user_id = InquiryWiseUser::where('inquiry_id', '=', $inquiry->id)->where('user_id', '=', Auth::id())->value('id');*/
        $inquiry_wise_user_id = request()->inquiry_wise_user_id;

        $followUps = FollowUp::join('inquiry_wise_users','follow_ups.inquiry_wise_user_id','=','inquiry_wise_users.id')->where('inquiry_wise_users.inquiry_id', '=', $inquiry->id)->orderBy('follow_ups.id', 'desc')->select('follow_ups.*', 'inquiry_wise_users.user_id')->get();

        return view('inquiry.createFollowUp', compact('inquiry', 'inquiry_wise_user_id','followUps'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function storeFollowUp()
    {

        FollowUp::create([
            'inquiry_wise_user_id' => request()->inquiry_wise_user_id,
            'interest_level' => request()->interest_level,
            'status' => request()->status,
            'action' => request()->action,
            'next_follow_up' => date('Y-m-d H:i:s', strtotime(request()->next_follow_up)),
            'comments' => request()->comments
        ]);

        return redirect('/inquiries/' . request()->inquiry_id . '/followUp');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  FollowUp $followUp
     * @return \Illuminate\Http\Response
     */
    public function editFollowUp(FollowUp $followUp)
    {
        $inquiry = $followUp->inquiryWiseUser->inquiry;
        //$inquiry_wise_user_id = InquiryWiseUser::where('inquiry_id', '=', $inquiry->id)->where('user_id', '=', Auth::id())->value('id');
        return view('inquiry.editFollowUp', compact('followUp', 'inquiry'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  FollowUp $followUp
     * @return \Illuminate\Http\Response
     */
    public function updateFollowUp(FollowUp $followUp)
    {
        $followUp->inquiry_wise_user_id = request()->inquiry_wise_user_id;
        $followUp->interest_level = request()->interest_level;
        $followUp->status = request()->status;
        $followUp->action = request()->action;
        $followUp->next_follow_up = date('Y-m-d H:i:s', strtotime(request()->next_follow_up));
        $followUp->comments = request()->comments;

        $followUp->save();

        return redirect('/inquiries/' . $followUp->inquiryWiseUser->inquiry_id . '/followUp');
    }

    public function getArea($id){

        $area = Area::where('city_id',$id)->get();

        return Response::json($area);

    }
}