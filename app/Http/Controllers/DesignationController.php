<?php

namespace App\Http\Controllers;

use App\Designation;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class DesignationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $designations = Designation::all();

        return view('designation.designation', compact('designations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => [
                'required',
                'unique:designations,name',

            ],
            'department_id' => [
                'required'
            ]
        ]);

        $designation = Designation::where('department_id', $request->department_id)->where('name', $request->name)->get();

        if ($designation->isEmpty()) {

            Designation::create(request(['name', 'department_id']));

            return redirect()->back()->with('data', ['type' => 'success', 'message' => 'Added Successfully']);

        } else {

            return redirect()->back()->with('data', ['type' => 'warning', 'message' => 'Designation already exists for the country']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Course $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Designation $designation)
    {
        return view('designation.editDesignation', compact('designation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Course $course
     * @return \Illuminate\Http\Response
     */
    public function update(Designation $designation)
    {
        $this->validate(request(), [
            'name' => [
                'required',
                Rule::unique('cities')->ignore($designation->id)

            ],
        ]);

        $designations = Designation::where('department_id', request()->department_id)->where('name', request()->name)->where('id', '!=', $designation->id)->first();


        if (!isset($designations)) {
            $designation->name = request()->name;
            $designation->department_id = request()->department_id;

            $designation->save();
            return redirect('/designations')->with('data', ['type' => 'success', 'message' => 'Updated Successfully']);


        } else {

            return redirect()->back()->with('data', ['type' => 'warning', 'message' => 'Designation already exists.']);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Course $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Designation $designation)
    {
        $designation->delete();
        return redirect('/designations');
    }
}
