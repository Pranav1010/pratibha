<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CityController extends Controller
{
  
    public function index()
    {
        $cities = City::all();
        return view('city.city', compact('cities'));
    }

  
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => [
                'unique:cities,name',
                'required'
            ]

        ]);

        City::create(request(['name']));

        return redirect()->back()->with('data', ['type' => 'success', 'message' => 'Added Successfully']);
    }

  
    public function show($id)
    {
        
    }

    public function edit(City $city)
    {
        return view('city.editCity', compact('city'));
    }

    public function update(City $city)
    {
        $this->validate(request(), [
            'name' => [
                'required',
                Rule::unique('cities')->ignore($city->id)
            ],

        ]);

        $city->name = request()->name;

        $city->save();

        return redirect('/cities')->with('data', ['type' => 'success', 'message' => 'Updated Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        $city->delete();
        return redirect('/cities')->with('data', ['type' => 'error', 'message' => 'Deleted Successfully']);

    }
}
