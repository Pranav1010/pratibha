<?php

namespace App\Http\Controllers;

use App\FollowUp;
use App\Inquiry;
use App\User;
use App\VisaInquiry;
use Session;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
    	if(isset($user->id) && Auth::id() == 1){


    		$startDate = isset(request()->start)?request()->start:false;
            $endDate = isset(request()->end)?request()->end:false;

            Session::put('user_id', $user->id);
            if($user->id !== 1){
                $totalInquiries = VisaInquiry::join('inquiry_wise_users','visa_inquiries.id','=','inquiry_wise_users.inquiry_id')->where('inquiry_wise_users.user_id', '=', $user->id)->count();
                $completedInquiries = VisaInquiry::join('inquiry_wise_users','visa_inquiries.id','=','inquiry_wise_users.inquiry_id')->where("visa_inquiries.status", "=", 1)->where('inquiry_wise_users.user_id', '=', $user->id)->count();
                $cancelledInquiries = VisaInquiry::join('inquiry_wise_users','visa_inquiries.id','=','inquiry_wise_users.inquiry_id')->where("visa_inquiries.status", "=", 2)->where('inquiry_wise_users.user_id', '=', $user->id)->count();
                $allUsers = User::orderBy('name')->get();
                $assignedInquiries = VisaInquiry::leftJoin('reasons', function($join){
                    $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
                })->join("inquiry_wise_users", "visa_inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $user->id)->groupBy('reasons.inquiry_id')->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get()->count();
            } else{
                $totalInquiries = VisaInquiry::count();
                $completedInquiries = VisaInquiry::where("status", "=", 1)->count();
                $cancelledInquiries = VisaInquiry::where("status", "=", 2)->count();
                $assignedInquiries = VisaInquiry::leftJoin('reasons', function($join){
                    $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
                })->groupBy('reasons.inquiry_id')->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get()->count();
            }
            $followUps = FollowUp::getUserFollowUps($user->id, $startDate, $endDate);
            $userId = $user->id;

            $startDate = $startDate ? date('d-m-Y', strtotime($startDate)) : date('d-m-Y');
            $endDate = $endDate ? date('d-m-Y', strtotime($endDate)) : date('d-m-Y');
        
    	} else {

            if(Session::has('user_id')){
                    $userId = Session::get('user_id');
            }
            else {
                    $userId = Auth::id();
            }
            
            if($userId !== 1){
                $totalInquiries = VisaInquiry::join('inquiry_wise_users','visa_inquiries.id','=','inquiry_wise_users.inquiry_id')->where('inquiry_wise_users.user_id', '=', $userId)->count();
                $completedInquiries = VisaInquiry::join('inquiry_wise_users','visa_inquiries.id','=','inquiry_wise_users.inquiry_id')->where("visa_inquiries.status", "=", 1)->where('inquiry_wise_users.user_id', '=', $userId)->count();
                $cancelledInquiries = VisaInquiry::join('inquiry_wise_users','visa_inquiries.id','=','inquiry_wise_users.inquiry_id')->where("visa_inquiries.status", "=", 2)->where('inquiry_wise_users.user_id', '=', $userId)->count();
                $allUsers = User::orderBy('name')->get();
                $inquiries = VisaInquiry::join('inquiry_wise_users','visa_inquiries.id','=','inquiry_wise_users.inquiry_id')->where('inquiry_wise_users.user_id', '=', $userId)->pluck('visa_inquiries.id');
                $assignedInquiries = VisaInquiry::leftJoin('reasons', function($join){
                    $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
                })->join("inquiry_wise_users", "visa_inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->groupBy('reasons.inquiry_id')->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get()->count();
            } else{
                $totalInquiries = VisaInquiry::count();
                $completedInquiries = VisaInquiry::where("status", "=", 1)->count();
                $cancelledInquiries = VisaInquiry::where("status", "=", 2)->count();
                $assignedInquiries = VisaInquiry::leftJoin('reasons', function($join){
                    $join->whereRaw("visa_inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = visa_inquiries.id)");
                })->groupBy('reasons.inquiry_id')->select('visa_inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get()->count();
            }
            $followUps = FollowUp::getUserFollowUps($userId);

            $startDate = date('d-m-Y');
            $endDate = date('d-m-Y');
        
    	}

        return view('home', compact('totalInquiries', 'followUps', 'completedInquiries', 'cancelledInquiries', 'userId', 'startDate','endDate', 'assignedInquiries'));

    }

    public function older(){

        if(Session::has('user_id')){
                $userId = Session::get('user_id');
        }
        else {
                $userId = Auth::id();
        }
        
        if($userId !== 1){
            $totalInquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where('inquiry_wise_users.user_id', '=', $userId)->count();
            $completedInquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where("inquiries.status", "=", 1)->where('inquiry_wise_users.user_id', '=', $userId)->count();
            $cancelledInquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where("inquiries.status", "=", 2)->where('inquiry_wise_users.user_id', '=', $userId)->count();
            $allUsers = User::orderBy('name')->get();
            $inquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where('inquiry_wise_users.user_id', '=', $userId)->pluck('inquiries.id');
            $assignedInquiries = Inquiry::leftJoin('reasons', function($join){
                $join->whereRaw("inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = inquiries.id)");
            })->join("inquiry_wise_users", "inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->groupBy('reasons.inquiry_id')->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get()->count();
        } else{
            $totalInquiries = Inquiry::count();
            $completedInquiries = Inquiry::where("status", "=", 1)->count();
            $cancelledInquiries = Inquiry::where("status", "=", 2)->count();
            $assignedInquiries = Inquiry::leftJoin('reasons', function($join){
                $join->whereRaw("inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = inquiries.id)");            
            })->groupBy('reasons.inquiry_id')->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get()->count();
        }

        $date = date('Y-m-d', strtotime('-2 day'));
        $followUps = FollowUp::getDateWiseFollowUps($userId, $date);

        $startDate = date('d-m-Y');
        $endDate = date('d-m-Y');

        return view('home', compact('totalInquiries', 'followUps', 'completedInquiries', 'cancelledInquiries', 'userId', 'startDate','endDate', 'assignedInquiries'));

    }

    public function yesterday(){

        if(Session::has('user_id')){
                $userId = Session::get('user_id');
        }
        else {
                $userId = Auth::id();
        }
        
        if($userId !== 1){
            $totalInquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where('inquiry_wise_users.user_id', '=', $userId)->count();
            $completedInquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where("inquiries.status", "=", 1)->where('inquiry_wise_users.user_id', '=', $userId)->count();
            $cancelledInquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where("inquiries.status", "=", 2)->where('inquiry_wise_users.user_id', '=', $userId)->count();
            $allUsers = User::orderBy('name')->get();
            $inquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where('inquiry_wise_users.user_id', '=', $userId)->pluck('inquiries.id');
            $assignedInquiries = Inquiry::leftJoin('reasons', function($join){
                $join->whereRaw("inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = inquiries.id)");
            })->join("inquiry_wise_users", "inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->groupBy('reasons.inquiry_id')->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get()->count();
        } else{
            $totalInquiries = Inquiry::count();
            $completedInquiries = Inquiry::where("status", "=", 1)->count();
            $cancelledInquiries = Inquiry::where("status", "=", 2)->count();
            $assignedInquiries = Inquiry::leftJoin('reasons', function($join){
                $join->whereRaw("inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = inquiries.id)");            
            })->groupBy('reasons.inquiry_id')->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get()->count();
        }
        
        $date = date('Y-m-d', strtotime('yesterday'));
        $followUps = FollowUp::getDateWiseFollowUps($userId, $date);

        $startDate = date('d-m-Y');
        $endDate = date('d-m-Y');

        return view('home', compact('totalInquiries', 'followUps', 'completedInquiries', 'cancelledInquiries', 'userId', 'startDate','endDate', 'assignedInquiries'));

    }
    
    public function today(){

        if(Session::has('user_id')){
                $userId = Session::get('user_id');
        }
        else {
                $userId = Auth::id();
        }
        
        if($userId !== 1){
            $totalInquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where('inquiry_wise_users.user_id', '=', $userId)->count();
            $completedInquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where("inquiries.status", "=", 1)->where('inquiry_wise_users.user_id', '=', $userId)->count();
            $cancelledInquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where("inquiries.status", "=", 2)->where('inquiry_wise_users.user_id', '=', $userId)->count();
            $allUsers = User::orderBy('name')->get();
            $inquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where('inquiry_wise_users.user_id', '=', $userId)->pluck('inquiries.id');
            $assignedInquiries = Inquiry::leftJoin('reasons', function($join){
                $join->whereRaw("inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = inquiries.id)");
            })->join("inquiry_wise_users", "inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->groupBy('reasons.inquiry_id')->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get()->count();
        } else{
            $totalInquiries = Inquiry::count();
            $completedInquiries = Inquiry::where("status", "=", 1)->count();
            $cancelledInquiries = Inquiry::where("status", "=", 2)->count();
            $assignedInquiries = Inquiry::leftJoin('reasons', function($join){
                $join->whereRaw("inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = inquiries.id)");            
            })->groupBy('reasons.inquiry_id')->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get()->count();
        }

        $date = date('Y-m-d');
        $followUps = FollowUp::getDateWiseFollowUps($userId, $date);

        $startDate = date('d-m-Y');
        $endDate = date('d-m-Y');

        return view('home', compact('totalInquiries', 'followUps', 'completedInquiries', 'cancelledInquiries', 'userId', 'startDate','endDate', 'assignedInquiries'));

    }

    public function tomorrow(){

        if(Session::has('user_id')){
                $userId = Session::get('user_id');
        }
        else {
                $userId = Auth::id();
        }
        
        if($userId !== 1){
            $totalInquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where('inquiry_wise_users.user_id', '=', $userId)->count();
            $completedInquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where("inquiries.status", "=", 1)->where('inquiry_wise_users.user_id', '=', $userId)->count();
            $cancelledInquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where("inquiries.status", "=", 2)->where('inquiry_wise_users.user_id', '=', $userId)->count();
            $allUsers = User::orderBy('name')->get();
            $inquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where('inquiry_wise_users.user_id', '=', $userId)->pluck('inquiries.id');
            $assignedInquiries = Inquiry::leftJoin('reasons', function($join){
                $join->whereRaw("inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = inquiries.id)");
            })->join("inquiry_wise_users", "inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->groupBy('reasons.inquiry_id')->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get()->count();
        } else{
            $totalInquiries = Inquiry::count();
            $completedInquiries = Inquiry::where("status", "=", 1)->count();
            $cancelledInquiries = Inquiry::where("status", "=", 2)->count();
            $assignedInquiries = Inquiry::leftJoin('reasons', function($join){
                $join->whereRaw("inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = inquiries.id)");            
            })->groupBy('reasons.inquiry_id')->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get()->count();
        }

        $date = date('Y-m-d', strtotime('tomorrow'));
        $followUps = FollowUp::getDateWiseFollowUps($userId, $date);

        $startDate = date('d-m-Y');
        $endDate = date('d-m-Y');

        return view('home', compact('totalInquiries', 'followUps', 'completedInquiries', 'cancelledInquiries', 'userId', 'startDate','endDate', 'assignedInquiries'));

    }
    
    public function newer(){

        if(Session::has('user_id')){
                $userId = Session::get('user_id');
        }
        else {
                $userId = Auth::id();
        }
        
        if($userId !== 1){
            $totalInquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where('inquiry_wise_users.user_id', '=', $userId)->count();
            $completedInquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where("inquiries.status", "=", 1)->where('inquiry_wise_users.user_id', '=', $userId)->count();
            $cancelledInquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where("inquiries.status", "=", 2)->where('inquiry_wise_users.user_id', '=', $userId)->count();
            $allUsers = User::orderBy('name')->get();
            $inquiries = Inquiry::join('inquiry_wise_users','inquiries.id','=','inquiry_wise_users.inquiry_id')->where('inquiry_wise_users.user_id', '=', $userId)->pluck('inquiries.id');
            $assignedInquiries = Inquiry::leftJoin('reasons', function($join){
                $join->whereRaw("inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = inquiries.id)");
            })->join("inquiry_wise_users", "inquiries.id", "=", "inquiry_wise_users.inquiry_id")->where('inquiry_wise_users.user_id', '=', $userId)->groupBy('reasons.inquiry_id')->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get()->count();
        } else{
            $totalInquiries = Inquiry::count();
            $completedInquiries = Inquiry::where("status", "=", 1)->count();
            $cancelledInquiries = Inquiry::where("status", "=", 2)->count();
            $assignedInquiries = Inquiry::leftJoin('reasons', function($join){
                $join->whereRaw("inquiries.id = reasons.inquiry_id AND reasons.id IN (SELECT max(id) as id FROM reasons WHERE inquiry_id = inquiries.id)");            
            })->groupBy('reasons.inquiry_id')->select('inquiries.*', 'reasons.reason as newReason', 'reasons.assignor_id')->get()->count();
        }

        $date = date('Y-m-d', strtotime('+2 day'));
        $followUps = FollowUp::getDateWiseFollowUps($userId, $date);

        $startDate = date('d-m-Y');
        $endDate = date('d-m-Y');

        return view('home', compact('totalInquiries', 'followUps', 'completedInquiries', 'cancelledInquiries', 'userId', 'startDate','endDate', 'assignedInquiries'));

    }
}
