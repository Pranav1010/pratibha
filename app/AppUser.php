<?php

namespace App;

use Encore\Admin\Auth\Database\Role;
use Illuminate\Database\Eloquent\Model;

class AppUser extends Model
{
    public function student()
    {
        return $this->hasOne(Student::class,'user_id');
    }

    public function roles()
    {
        return $this->belongsTo(Role::class, "role_id");
    }

    public function fees(){
        return $this->hasMany(Fee::class, "user_id");
    }

    public function userCourse()
    {
        return $this->hasMany(CourseStudent::class,'student_id');
    }
    public function login()
    {
        return $this->belongsTo(StudentLogin::class);
    }
}
