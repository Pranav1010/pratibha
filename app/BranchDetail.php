<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchDetail extends Model
{
    protected $fillable = ['branch_id','name', 'mobile', 'email'];

    public function item()
    {
        return $this->hasMany('App\BranchDetail');
    }
}
