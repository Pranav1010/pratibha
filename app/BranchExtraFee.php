<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchExtraFee extends Model
{
    public function extraFee()
    {
        return $this->belongsTo(ExtraFee::class);
    }
    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }
}
