<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolPendingPayment extends Model
{
    public function school()
    {
    	return $this->belongsTo('App\School','school_id');
    }
}
