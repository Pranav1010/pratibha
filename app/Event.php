<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use File;

class Event extends Model
{
    public function gallery()
    {
        return $this->hasMany(Gallery::class);
    }

    public function delete()
    {

        foreach ($this->gallery as $file)
        {
            if($file->type == 'image')
            {
                File::delete(public_path('upload/images/').$file->path);
                File::delete(public_path('upload/thumbnail/').$file->path);
                $file->delete();
            }
        }

        foreach ($this->gallery as $file)
        {
            if($file->type == 'video')
            {
                $file->delete();
            }
        }
        return parent::delete();
    }

    public function interests()
    {
        return $this->hasMany(EventInterest::class);
    }
}
