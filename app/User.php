<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Auth\Database\Administrator;
use App\Parents;
use App\CourseStudent;

class User extends Administrator
{
    protected $fillable = ['username', 'password', 'name', 'avatar', 'branch_id', 'email', 'mobile', 'address', 'city_id', 'advance_amount', 'fee_status' ];

    public function parent()
    {
    	return $this->hasOne(Parents::class,'user_id');
    }

    public function student()
    {
    	return $this->hasOne(Student::class,'user_id');
    }

    public function userCourse()
    {
    	return $this->hasMany(CourseStudent::class,'student_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class,'city_id');
    }

    public function fees(){
        return $this->hasMany(Fee::class);
    }

    public function pendingFees(){
        return $this->hasMany(PendingFee::class);
    }

    public function school()
    {
        return $this->hasOne(School::class,'user_id');
    }
}
