<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PendingExtraFees extends Model
{
    protected $fillable = ['user_id','course_id','extra_fee_type_id','fees'];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function extraFee()
    {
        return $this->belongsTo(ExtraFee::class , 'extra_fee_type_id');
    }
}
