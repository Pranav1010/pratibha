<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseFeeType extends Model
{
    protected $fillable = ['course_id','fee_type_id','fees'];

    public function feeType()
    {
        return $this->belongsTo(FeeType::class);
    }
}
