<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Encore\Admin\Facades\Admin;

class Fee extends Model
{
    protected $fillable = [
        'user_id', 'lump_sum_amount', 'net_amount', 'payment_date', 'payment_type', 'cheque_number', 'bank_id', 'remarks'
    ];

    protected $dates = ['payment_date', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo(AppUser::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }

    public function feeDetails()
    {
        return $this->hasMany(FeeDetail::class);
    }

    public static function totalAmount($user_id = null)
    {

        $date = new \DateTime("now", new \DateTimeZone("Asia/Kolkata"));
        $currentMonth = $date->format("m");

        if ($user_id) {

            $totalPaid = FeeDetail::whereHas("Fee", function ($q) use ($currentMonth, $user_id) {
                $q->where(DB::raw("SUBSTRING(payment_date, 6, 2)"), $currentMonth)->where("user_id", $user_id);
            })->sum("paid_amount");

            $totalPending = PendingFee::where("user_id", $user_id)->sum('fees');

            $totalExtraPaid = ExtraFeeDetails::whereHas("feeDetail", function ($q) use ($currentMonth, $user_id) {
                $q->whereHas("Fee", function ($q) use ($currentMonth, $user_id) {
                    $q->where(DB::raw("SUBSTRING(payment_date, 6, 2)"), $currentMonth)->where("user_id", $user_id);
                });
            })->sum("paid_amount");

            $totalExtraPending = PendingExtraFees::where("user_id", $user_id)->sum("fees");

            return $totalPaid + $totalPending + $totalExtraPaid + $totalExtraPending;

        } else {

            $totalPaid = FeeDetail::whereHas("Fee", function ($q) use ($currentMonth, $user_id) {
                $q->where(DB::raw("SUBSTRING(payment_date, 6, 2)"), $currentMonth);
            })->sum("paid_amount");

            $totalPending = PendingFee::sum("fees");

            $totalExtraPaid = ExtraFeeDetails::whereHas("feeDetail", function ($q) use ($currentMonth) {
                $q->whereHas("Fee", function ($q) use ($currentMonth) {
                    $q->where(DB::raw("SUBSTRING(payment_date, 6, 2)"), $currentMonth);
                });
            })->sum("paid_amount");

            $totalExtraPending = PendingExtraFees::sum("fees");

            return $totalPaid + $totalPending + $totalExtraPaid + $totalExtraPending;

        }

    }

    public static function paidAmount($user_id = null)
    {

        $date = new \DateTime("now", new \DateTimeZone("Asia/Kolkata"));
        $currentMonth = $date->format("m");

        if ($user_id) {
            $feeDetail = FeeDetail::whereHas("Fee", function ($q) use ($user_id, $currentMonth) {
                $q->where(DB::raw("MONTH(payment_date)"), $currentMonth)->where("user_id", $user_id)->where("payment_type","0");
            })->sum("paid_amount");

            $extraFeeDetail = ExtraFeeDetails::whereHas("feeDetail", function ($q) use ($currentMonth, $user_id) {
                $q->whereHas("Fee", function ($q) use ($currentMonth, $user_id) {
                    $q->where(DB::raw("SUBSTRING(payment_date, 6, 2)"), $currentMonth)->where("user_id", $user_id);
                });
            })->sum("paid_amount");

            return $feeDetail + $extraFeeDetail;
        }
        else {
            $branch = null;
            if(isRole('branch-admin'))
            {
                $branch = Branch::where('user_id',Admin::user()->id)->first();
            }
            if(isset($branch))
            {
                $feeDetail = FeeDetail::whereHas("Fee", function ($q) use ($branch,$currentMonth) {
                    $q->where(DB::raw("MONTH(payment_date)"), $currentMonth)->where("payment_type","0")->whereHas('user',function($q) use($branch){
                        $q->whereHas('student',function ($q) use($branch) {
                            $q->where('branch_id',$branch->id);
                        });
                    });
                })->sum("paid_amount");
            }
            else {
                    $feeDetail = FeeDetail::whereHas("Fee", function ($q) use ($currentMonth) {
                        $q->where(DB::raw("MONTH(payment_date)"), $currentMonth)->where("payment_type","0");
                    })->sum("paid_amount");
            }
            return $feeDetail;
        }

    }

    public static function refundAmount($user_id = null)
    {

        $date = new \DateTime("now", new \DateTimeZone("Asia/Kolkata"));
        $currentMonth = $date->format("m");

        if ($user_id) {
            $feeDetail = FeeDetail::whereHas("Fee", function ($q) use ($user_id, $currentMonth) {
                $q->where(DB::raw("MONTH(payment_date)"), $currentMonth)->where("user_id", $user_id)->where("payment_type","1");
            })->sum("paid_amount");

            $extraFeeDetail = ExtraFeeDetails::whereHas("feeDetail", function ($q) use ($currentMonth, $user_id) {
                $q->whereHas("Fee", function ($q) use ($currentMonth, $user_id) {
                    $q->where(DB::raw("SUBSTRING(payment_date, 6, 2)"), $currentMonth)->where("user_id", $user_id);
                });
            })->sum("paid_amount");

            return $feeDetail + $extraFeeDetail;
        }
        else {
            $branch = null;
            if(isRole('branch-admin'))
            {
                $branch = Branch::where('user_id',Admin::user()->id)->first();
            }
            if(isset($branch))
            {
                $feeDetail = FeeDetail::whereHas("Fee", function ($q) use ($branch,$currentMonth) {
                    $q->where(DB::raw("MONTH(payment_date)"), $currentMonth)->where("payment_type","1")->whereHas('user',function($q) use($branch){
                        $q->whereHas('student',function ($q) use($branch) {
                            $q->where('branch_id',$branch->id);
                        });
                    });
                })->sum("paid_amount");
            }
            else {
                $feeDetail = FeeDetail::whereHas("Fee", function ($q) use ($currentMonth) {
                    $q->where(DB::raw("MONTH(payment_date)"), $currentMonth)->where("payment_type","1");
                })->sum("paid_amount");
            }
            return $feeDetail;
        }

    }

    public static function totalDiscount($user_id = null)
    {

        $currentMonth = date("m");

        if ($user_id) {
            return FeeDetail::whereHas("Fee", function ($q) use ($user_id, $currentMonth) {
                $q->where("user_id", $user_id)->where(DB::raw("MONTH(payment_date)"), $currentMonth);
            })->sum("discount");
        } else {
            $branch = null;
            if(isRole('branch-admin'))
            {
                $branch = Branch::where('user_id',Admin::user()->id)->first();
                return FeeDetail::whereHas("Fee", function ($q) use ($branch, $currentMonth) {
                    $q->where(DB::raw("MONTH(payment_date)"), $currentMonth)->whereHas('user',function($q) use($branch){
                        $q->whereHas('student',function ($q) use($branch) {
                            $q->where('branch_id',$branch->id);
                        });
                    });
                })->sum("discount");
            }
            else{
                return FeeDetail::whereHas("Fee", function ($q) use ($currentMonth) {
                    $q->where(DB::raw("MONTH(payment_date)"), $currentMonth);
                })->sum("discount");
            }
        }

    }

    public static function pendingAmount($user_id = null)
    {
        if ($user_id) {
            $totalPending = PendingFee::where("user_id", $user_id)->sum('fees');
        } else {
            $branch = null;
            if(isRole('branch-admin'))
            {
                $branch = Branch::where('user_id',Admin::user()->id)->first();
                $totalPending = PendingFee::whereHas('user',function ($q) use($branch){
                    $q->whereHas('student',function ($q) use($branch){
                        $q->where('branch_id',$branch->id);
                    });
                })->sum('fees');
            }
            else {
                    $totalPending = PendingFee::sum('fees');
                }

        }
//        $totalPending = 0;
//        foreach ($pendingFee as $pending) {
//            $diff = getMonthDiff($pending->month, $pending->year);
//            if($pending->fees_type == 1)
//            {
//                $totalPending = $totalPending + ($pending->fees * $diff);
//            }else{
//                $totalPending = $totalPending + ($pending->fees);
//            }
//
//        }

        return $totalPending;
    }

    public static function totalAdvance()
    {
        $branch = null;
        if(isRole('branch-admin'))
        {
            $branch = Branch::where('user_id',Admin::user()->id)->first();

            $advanceAmount = Student::where('branch_id',$branch->id)->sum("advance_amount");

            return $advanceAmount;
        }
        else {
            return Student::sum("advance_amount");
        }


    }

    public function getFeeExtraDetailFees($id)
    {
        $fee = Fee::find($id);
        $feeDetails = $fee->feeDetails;
        $extraTotal = 0;
        foreach ($feeDetails as $detail)
        {
           $extraTotal = $extraTotal + $detail->extraFeeDetail->sum('paid_amount');
        }
        return $extraTotal;
    }
}