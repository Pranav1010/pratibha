<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventInterest extends Model
{
    public function event()
    {
        $this->belongsTo(Event::class);
    }
}
