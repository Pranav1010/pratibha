<?php

namespace App;

use Carbon\Carbon;
use Encore\Admin\Auth\Database\Administrator;
use Illuminate\Database\Eloquent\Model;

class FollowUp extends Model
{
    protected $fillable = [
        'inquiry_id', 'interest_level_id', 'follow_up_status_id', 'follow_up_action_id', 'next_follow_up', 'comments'
    ];

    protected $dates = ['next_follow_up', 'created_at', 'updated_at'];

    public function inquiry()
    {
        return $this->belongsTo(Inquiry::class);
    }

    public function interestLevel()
    {
        return $this->belongsTo(InterestLevel::class);
    }

    public function followUpStatus()
    {
        return $this->belongsTo(FollowUpStatus::class);
    }

    public function followUpAction()
    {
        return $this->belongsTo(FollowUpAction::class);
    }

    public static function getUserFollowUps($userId, $startDate = false, $endDate = false)
    {

        $inquiries = Inquiry::where('status', '=', '0')->pluck('id')->toArray();
        
        if(isRole("administrator")){

            $followUpIds = FollowUp::whereHas("inquiry", function($query) use ($userId){
                $query->where("user_id", $userId);
            })->whereIn("inquiry_id", $inquiries)->select(\DB::raw("max(id) as id"))->get();

        }else{

            $followUpIds = FollowUp::select(\DB::raw("max(id) as id"))->whereIn('inquiry_id', $inquiries)->get();

        }

        if($startDate && $endDate){
            
            return FollowUp::whereIn('id',$followUpIds)->whereDate('next_follow_up', '>=', $startDate)->whereDate('next_follow_up', '<=', $endDate)->orderBy("next_follow_up")->get();
        
        } else {

            return FollowUp::whereIn('id',$followUpIds)->orderBy("next_follow_up")->get();

        }
    }

    public static function getDateWiseFollowUps($userId, $date){

        $today = date('Y-m-d');
        $inquiries = Inquiry::where('status', '=', '0')->pluck('id')->toArray();

        if(isRole("administrator")){

            $followUpIds = FollowUp::whereHas("inquiries", function($query) use ($userId){
                $query->where("user_id", $userId);
            })->whereIn("inquiry_id", $inquiries)->select(\DB::raw("max(id) as id"))->get();

        }else{

            $followUpIds = FollowUp::select(\DB::raw("max(id) as id"))->whereIn('inquiry_id', $inquiries)->get();

        }

        return FollowUp::whereIn('id',$followUpIds)->whereDate('next_follow_up', '=', $date)->orderBy("next_follow_up")->get();

    }

    public static function getNumberOfFollowUps($inquiry_id){

        return FollowUp::where('inquiry_id', $inquiry_id)->count();

    }

    public static function getDaysFromStartDateOfInquiry($creationDate){

        $date1=date_create($creationDate->format('Y-m-d'));
        $date2=date_create(Carbon::now('Asia/Kolkata')->format('Y-m-d'));
        $diff=date_diff($date1,$date2);
        return $diff->format("%a");

    }
}
