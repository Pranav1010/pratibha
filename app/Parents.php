<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parents extends Model
{

    protected $guarded = [];

    public function fatherProfession()
    {
        return $this->belongsTo(Profession::class,'father_profession');
    }

    public function motherProfession()
    {
        return $this->belongsTo(Profession::class,'mother_profession');
    }
    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
