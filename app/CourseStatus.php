<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseStatus extends Model
{
    public function courseStudent()
    {
        return $this->belongsTo(CourseStudent::class,'course_student_id');
    }
}
