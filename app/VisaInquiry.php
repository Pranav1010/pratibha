<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class VisaInquiry extends Model
{
    protected $guarded = [];

    public function inquiryUser()
    {
        return $this->hasMany(InquiryWiseUser::class,'inquiry_id','id')->with('user');
    }

    public function reasons()
    {
        return $this->hasMany(Reason::class);
    }
      public function source()
        {
            return $this->belongsTo(InquirySource::class);
        }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function preferedCountry()
    {
        return $this->hasMany('App\CountryProgram', 'inquiry_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function program()
    {
        return $this->belongsTo(Course::class);
    }

     public static function getUserNames($inquiry_id)
    {
        $inquiry_wise_user = InquiryWiseUser::where('inquiry_id', '=', $inquiry_id)->orderBy('id', 'DESC')->first();
        $userNames = InquiryWiseUser::orderBy('inquiry_wise_users.id', 'DESC')->join('users', 'users.id', '=', 'inquiry_wise_users.user_id')->where('inquiry_wise_users.id', $inquiry_wise_user->id)->pluck('users.name')->first();
        return $userNames;
    }

    public static function getNumberOfFollowUps($inquiry_id)
    {
        $inquiriesWithUsers = InquiryWiseUser::where('inquiry_id', '=', $inquiry_id)->pluck('id')->toArray();
        return FollowUp::whereIn('inquiry_wise_user_id', $inquiriesWithUsers)->count();
    }

    public static function getDaysFromStartDateOfInquiry($creationDate){

        $date1= \DateTime::createFromFormat('Y-m-d H:i:s',$creationDate);
        $date2=date_create(Carbon::now('Asia/Kolkata')->format('Y-m-d'));
        $diff=date_diff($date1,$date2);
        return $diff->format("%a");

    }
}
