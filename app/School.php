<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{

    protected $fillable = ["name","address","contact_no","city_id","school_status","user_id","branch_id"];

    public function city(){
    	return $this->belongsTo(City::class,'city_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function schoolPaymentHistory()
    {
    	return $this->hasMany(SchoolPaymentHistory::class);
    }

    public function schoolPendingPayment()
    {
    	return $this->hasMany(SchoolPendingPayment::class);
    }
}
