<?php


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


//Auth::routes();


//Route::get('/', 'HomeController@index');
/*Route::get('/home', 'HomeController@index');
Route::get('/home/yesterday', 'HomeController@yesterday');
Route::get('/home/older', 'HomeController@older');
Route::get('/home/today', 'HomeController@today');
Route::get('/home/tomorrow', 'HomeController@tomorrow');
Route::get('/home/newer', 'HomeController@newer');*/
Route::get('/userWise/{user}', 'HomeController@index');
//Route::resource('/branches', 'BranchesController');
//Route::resource('/courses', 'CoursesController');
//Route::resource('/inquiries', 'InquiriesController');
Route::resource('/users', 'UsersController');
//Route::get('/completed', 'InquiriesController@completedInquiries');
//Route::get('/cancelled', 'InquiriesController@cancelledInquiries');
//Route::get('/forwarded', 'InquiriesController@forwardedInquiries');
//Route::get('/inquiries/{inquiry}/followUp', 'InquiriesController@followUp');
//Route::post('/inquiries/followUp', 'InquiriesController@storeFollowUp');
//Route::get('/inquiries/{inquiry}/followUp/create', 'InquiriesController@createFollowUp');
//Route::get('/inquiries/followUp/{followUp}/edit', 'InquiriesController@editFollowUp');
//Route::put('/inquiries/followUp/{followUp}', 'InquiriesController@UpdateFollowUp');
//Route::resource('/cities', 'CityController');
//Route::resource('/countries', 'CountryController');
//Route::resource('/areas', 'AreaController');
/*Route::resource('/roles', 'RoleController');*/
Route::resource('/departments', 'DepartmentController');
Route::resource('/designations', 'DesignationController');
//new route
/*Route::get('/completed', 'InquiryController@completedInquiries');
Route::get('/cancelled', 'InquiryController@cancelledInquiries');
Route::get('/forwarded', 'InquiryController@forwardedInquiries');
Route::get('/inquiries/{inquiry}/followUp', 'InquiryController@followUp');
Route::post('/inquiries/followUp', 'InquiryController@storeFollowUp');
Route::get('/inquiries/{inquiry}/followUp/create', 'InquiryController@createFollowUp');
Route::get('/inquiries/followUp/{followUp}/edit', 'InquiryController@editFollowUp');
Route::put('/inquiries/followUp/{followUp}', 'InquiryController@UpdateFollowUp');

Route::get('inquiries/ajax-area/{id}', 'InquiryController@getArea');
Route::get('inquiries/ajax-friend-list/', 'InquiryController@autoCompleteFriend');*/
//Route::get('inquiries/ajax-country/{id}', 'InquiriesController@getCountry');


//Route::put('/branches/{branch_id}/edit', 'BranchesController@update');
//Route::get('branches/{id}/load-{action}','BranchesController@loadModal');
/*Route::resource('/sources', 'InquirySourceController');*/


//Route::resource('/inquiries', 'InquiryController');
/*Route::post('inquiries/personal-information/', 'InquiryController@store');*/
/*Route::post('inquiries/academic-information/{id}', 'InquiryController@setAcademicInformation');
Route::post('inquiries/exam-information/{id}', 'InquiryController@setExamInformation');
Route::post('inquiries/work-experience-information/{id}', 'InquiryController@setWorkExperienceInformation');
Route::post('inquiries/travel-history-information/{id}', 'InquiryController@setTravelHistoryInformation');
Route::post('inquiries/counselling-summary-information/{id}', 'InquiryController@setCounsellingSummaryInformation');
Route::post('inquiries/dependent-information/{id}', 'InquiryController@setDependentInformation');
Route::post('inquiries/dependent-academic-information/{dependent_id}', 'InquiryController@setDependentAcademicInformation');
Route::post('inquiries/dependent-exam-information/{id}', 'InquiryController@setDependentExamInformation');
Route::post('inquiries/dependent-work-experience-information/{id}', 'InquiryController@setDependentWorkExperienceInformation');
Route::post('inquiries/dependent-travel-history-information/{id}', 'InquiryController@setDependentTravelHistoryInformation');

Route::put('inquiries/{visa_inquiry}/edit-personal-information/', 'InquiryController@update');
Route::put('inquiries/{inquiry_id}/edit-academic-information/', 'InquiryController@updateInquiryAcademic');
Route::put('inquiries/{inquiry_id}/edit-work-experience-information/', 'InquiryController@updateInquiryWorkExperience');
Route::put('inquiries/{inquiry_id}/edit-travel-history-information/', 'InquiryController@updateInquiryTravelHistory');
Route::put('inquiries/{inquiry_id}/edit-counselling-summary-information/', 'InquiryController@updateInquiryCounsellingSummary');
Route::put('inquiries/{inquiry_id}/edit-exam-information/', 'InquiryController@updateInquiryExam');

Route::put('inquiries/{dependent_id}/edit-dependent-detail/', 'InquiryController@updateDependentDetail');
Route::put('inquiries/{dependent_id}/edit-dependent-academic-information/', 'InquiryController@updateDependentAcademic');
Route::put('inquiries/{dependent_id}/edit-dependent-exam-information/', 'InquiryController@updateDependentExam');
Route::put('inquiries/{dependent_id}/edit-dependent-work-information/', 'InquiryController@updateDependentWorkExperience');
Route::put('inquiries/{dependent_id}/edit-dependent-travel-information/', 'InquiryController@updateDependentTravel');*/










