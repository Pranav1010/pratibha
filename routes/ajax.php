<?php

use App\Batch;
use App\Parents;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\User;

Route::get('/batches', function (Request $request) {
    $id = $request->get('data');
    return Batch::where('course_id',$id)->get();
});

Route::get('/parent-location',function (Request $request){
    $id = $request->get('data');
    return Parents::find($id);
})

?>