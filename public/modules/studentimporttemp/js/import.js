var requiredHeaderForStudent = ["Name", "Primary Mobile", "Secondary Mobile", "Activities", "Batches", "Address", "Birth Date", "Gender", "Fee Type","Fees Taken","Start Date"];
var requiredHeaderForTeacher = ["Name", "Mobile", "Permanent Address", "Permanent City"];

// This js uses the import.min.js

$(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
});

$(document).on('mouseover', '.error-td', function(){
    $(this).addClass('error-td-active');
});

$(document).on('mouseout', '.error-td', function(){
    $(this).removeClass('error-td-active');
});

$(document).ready( function() {
    $("#globalBack").attr('href','../../');
    var fileName = "";
    var same = false;

    $(':file').on('fileselect', function (event, numFiles, label) {

        same = (fileName != label);
        $('#fileName').text(label);
        $("#file_container").val(label);
        $(".alert-dismissable").hide();
        $("#sheetPreview").empty();
        var regex = /([a-zA-Z0-9\(\)\s_\\.\-:])+(.xls|.xlsx)$/;

        /*Checks whether the file is a valid excel file*/
        if (regex.test($("#fileToUpload").val().toLowerCase())) {
            $("#sheetContainer").show();
            $('.upload').removeAttr('disabled');
            var xlsxflag = false; /*Flag for checking whether excel is .xls format or .xlsx format*/
            if ($("#fileToUpload").val().toLowerCase().indexOf(".xlsx") > 0) {
                xlsxflag = true;
            }

            /*Checks whether the browser supports HTML5*/
            if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var data = e.target.result;
                    /*Converts the excel data in to object*/
                    if (xlsxflag) {
                        var workbook = XLSX.read(data, { type: 'binary' });
                    }
                    else {
                        var workbook = XLS.read(data, { type: 'binary' });
                    }

                    /*Gets all the sheetnames of excel in to a variable*/
                    var sheet_name_list = workbook.SheetNames;

                    /*This is used for restricting the script to consider only first sheet of excel*/
                    var cnt = 0;

                    /*Iterate through all sheets Convert the cell value to Json*/
                    sheet_name_list.forEach(function (y) {
                        if (xlsxflag) {
                            var exceljson = XLSX.utils.sheet_to_json(workbook.Sheets[y]);
                        }
                        else {
                            var exceljson = XLS.utils.sheet_to_row_object_array(workbook.Sheets[y]);
                        }
                        if (exceljson.length > 0 && cnt == 0) {
                            BindTable(exceljson, '#sheetPreview');
                            cnt++;
                        }
                    });
                    $('#sheetPreview').show();
                };
                if (xlsxflag) {/*If excel file is .xlsx extension than creates a Array Buffer from excel*/
                    reader.readAsArrayBuffer($("#fileToUpload")[0].files[0]);
                }
                else {
                    reader.readAsBinaryString($("#fileToUpload")[0].files[0]);
                }
            }
            else {
                toastr.error("Sorry! Your browser does not support HTML5!");
            }
        }
        else {
            toastr.error("Please upload a valid Excel file!");
            $("#sheetContainer").hide();
            $(".upload").attr('disabled',true);
        }
        resetUploadButton();
    });
});

$("#uploadTeacherFile").click(function(){

    loadUploadButton();
    $(".uploaded").remove();
    $(".error-text").text('');
    $(".alert-dismissable").hide();

    var fieldsAndJson = matchFieldsAndCreateJson(requiredHeaderForTeacher);
    var headerErrors = fieldsAndJson[1];

    if(!(headerErrors.length > 0)){

        var jsonData = fieldsAndJson[0];
        var data = JSON.stringify(jsonData);

        $.ajax({
            type: 'post',
            url : '/user-import/teachers/import',
            datatype: 'json',
            data:$("#teacherImport").serialize()+"&data="+data,
            success:function(response){
                toastr.success(response.length + ' row(s) successfully saved.');
                $("#getTeacherList").trigger('click');
            },
            error: function(error){
                var response = error.responseJSON;
                setErrorsAndBackground(response);
            }
        });
    }else{
        resetUploadButton();
        $("#errorList").html(getHeaderErrors(headerErrors));
        $(".alert-dismissable").show();
    }
});

$("#uploadStudentFile").click(function(){

    // call the loadUploadButton function from import.min.js
    loadUploadButton();
    $(".uploaded").remove();
    $(".error-text").text('');
    $(".alert-dismissable").hide();

    // call the matchFieldsAndCreateJson function from import.min.js for check headers and get json data.
    var fieldsAndJson = matchFieldsAndCreateJson(requiredHeaderForStudent);
    var headerErrors = fieldsAndJson[1];


        if(!(headerErrors.length > 0)){

            var jsonData = fieldsAndJson[0];
            var data = JSON.stringify(jsonData);
            $.ajax({
                type: 'post',
                url : '/studentimporttemp/',
                data:$("#studentImport").serialize()+data,
                success:function(response){
                    toastr.success(response.length + ' row(s) successfully saved.');
                    $("#getStudentList").trigger('click');
                },
                error: function(error){
                    var response = error.responseJSON;
                    // call the setErrorsAndBackground function from import.min.js
                    setErrorsAndBackground(response);
                }
            });
        }else{
            resetUploadButton();
            // call the getHeaderErrors function from import.min.js
            $("#errorList").html(getHeaderErrors(headerErrors));
            $(".alert-dismissable").show();
        }

});