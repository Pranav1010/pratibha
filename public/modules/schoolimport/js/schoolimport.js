$("#progress").hide();
$("#successMessage").hide();
$("#error").hide();
$('#schoolUpload').submit(function(e)
{
    progressBar();
    e.preventDefault();
    $("#progress").show();
    var data = new FormData(this);
    $('#schoolFile').prop('disabled', true);
    $('#uploadButton').prop('disabled', true);
    $.ajax({
        url: '/schoolimport/upload',
        type: 'POST',
        data: data,
        success:function(response){
            if(response == "success")
            {
                $("#successMessage").show();
            }
        },
        error:function (error) {
            $('#error').empty();
            $('#error').show();
            $('#error').append(error.responseJSON.errors.schoolFile);
        },
        cache: false,
        contentType: false,
        processData: false
    });
});



function progressBar()
{
    var interval=0;
    interval = setInterval(progress, 50);
    var progressPer =0;
    function progress()
    {
        if(progressPer < 100)
        {
            $.get('/schoolimport/progress',function (progress) {
                progressPer = progress;
            });
            $('#progressbar').css('width', progressPer+'%');
            $('#progressbar').text("");
            $('#progressbar').text(progressPer+'%');
        }
        else
        {
            $('#progressbar').css('width', progressPer+'%');
            $('#progressbar').text("");
            $('#progressbar').text(progressPer+'%');
            clearInterval(interval);
        }
    }
}
