$('.checkbox-minimal-blue').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
});

$('.radio-minimal-blue').iCheck({
    radioClass: 'iradio_flat-blue',
});

function createCourse(f, e) {
    e.preventDefault();
    var data = $(f).serialize();
    $('.form-group').removeClass('has-error');
    $('[for=inputError]').remove();

    $.ajax({
        url: $(f).attr('action'),
        data: data,
        type: $(f).attr('method'),
        success: function (response) {
            var data = JSON.parse(response);
            if (data != null) {

                $.pjax({
                    //timeout: 2000,
                    url: "/courses",
                    container: '#pjax-container'
                });
                toastr.success('Activity Added Successfully');
            }

        },
        error: function (errors) {
            errors = errors.responseJSON;
            $.each(errors, function (key, value) {

                if(key == 'branchId')
                {
                    $('.' + key).text('Branch Already Used This Activity');
                }
                if(key == 'name')
                {
                    $('.' + key).text('Branch Already Used This Activity');
                }
                console.log(value);
            });

        }
    })

    return false;
}

function updateCourse(f, e) {
    e.preventDefault();
    var data = $(f).serialize();
    $('.form-group').removeClass('has-error');
    $('[for=inputError]').remove();

    $.ajax({
        url: $(f).attr('action'),
        data: data,
        type: $(f).attr('method'),
        success: function (response) {
            if (response.success) {

                $.pjax({
                    //timeout: 2000,
                    url: "/courses",
                    container: '#pjax-container'
                });
                toastr.success('Activity Updated Successfully');
            }

        },
        error: function (errors) {
            errors = errors.responseJSON;
            $.each(errors, function (key, value) {

                if(key == 'branchId')
                {
                    $('.' + key).text('Branch Already Used This Activity');
                }
                if(key == 'name')
                {
                    $('.' + key).text('Branch Already Used This Activity');
                }
                console.log(value);
            });
        }
    })

    return false;

}