$('.openModal').click(function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    NProgress.start();
    $.ajax({
        type: 'GET',
        url: '/register/' + id,
        success: function (response) {
            $(".data-div").html(response);
            NProgress.done();
            $('#openModal').modal('show');

            setTimeout(function () {
                $('.course').select2({placeholder: 'Select Course'});
                $('.relation').select2({placeholder: 'Select Relation'});
                $('.batch').select2({placeholder: 'Select Batch'});
                $('#parents').select2({placeholder: 'Select Parent'});
                $('#city').select2({placeholder: 'Select City'});
                $('#school').select2({placeholder: 'Select School'});
                $('#blood_group').select2({placeholder: 'Select Blood Group'});
                $('#profession').select2({placeholder: 'Select Profession'});
                $('#isMember').select2({placeholder: 'Is Membership Fees Paid ?'});

                $('input[type="checkbox"].minimal').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                });
                $('#same_as_parent').on('ifChanged', function (event) {
                    var check = event.target.checked;
                    var parentId = $('#parents').val();
                    if (parentId == '') {
                        toastr.error('Please Select Parent');
                        ('#same_as_parent').iCheck('uncheck');
                    } else {
                        if (check) {
                            $.get('/ajax/parent-location', {data:parentId},function (response) {
                                $('#city').val(response.city_id);
                                $('#city').trigger('change');
                                $('#address').val(response.address);
                            })
                        }
                    }
                });
            }, 500);

            $('.bdate').datetimepicker({
                format: 'DD-MM-YYYY'
            });

            initDatePicker();
        },
        error: function () {
            toastr.error('Something went wrong.');
        }

    });
});

function initDatePicker() {
    $('.date').datetimepicker({
        format: 'MM-YYYY',
        defaultDate: new Date()
    });
}

var count = 1;

function addDiv() {
    count = count + 1;
    $('.course').select2('destroy');
    $('.batch').select2('destroy');
    $('#template .course').attr('id', count);
    $('#template .batch').attr('id', 'batch-' + count);
    return $('#template').html();
}

initDatePicker();

function removeBox(obj) {
    $(obj).parent().parent().remove();
}

function addBox() {
    var divData = addDiv();
    $('#course_date_div').append('<div>' + divData + '</div>');
    $('.course').select2({placeholder: 'Select Course'});
    $('.batch').select2({placeholder: 'Select Batch'});
    initDatePicker();
}

function courseChange(obj) {
    var id = $(obj).attr('id');
    var batch = $(obj).parents(".form-group").next(".form-group").find("select");
    batch.empty();
    $.get('/ajax/batches', {
        data: $(obj).val(),
        _token: "{{ csrf_token() }}"
    }, function (response) {
        $.each(response, function (k, value) {
            batch.append('<option value="' + value.id + '">' + value.name + '</option>');
        });
    });
}

function registerStudent(f, e) {
    e.preventDefault();
    var data = $(f).serialize();
    $('#error_combo').html('');
    $('.form-group').removeClass('has-error');

    $.ajax({
        url: $(f).attr('action'),
        data: data,
        type: $(f).attr('method'),
        success: function (response) {
            if (response.result === 'success') {
                $('#openModal').modal('hide');
                $('.modal-backdrop').remove();
                $.pjax({
                    //timeout: 2000,
                    url: "/students",
                    container: '#pjax-container'
                });
                toastr.success('Student Added Successfully');
            }

        },
        error: function (errors) {
            $('#openModal').animate({scrollTop: 0}, 'slow');
            $.each(JSON.parse(errors.responseText).error, function (key, value) {
                $('#error_' + key).html('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> ' + value + '</label>');
                $('#error_' + key).parent().parent().addClass('has-error');
            });

        }
    })

    return false;
}

