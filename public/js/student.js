$('.openModal').click(function () {
    console.log(1);
    var id = $(this).data('id');
    $.get('/student-detail/' + id, function (response) {
        $('.data-div').html(response);
        $('.student-modal').modal('show');
        $('.loader').hide();
    });
});


var parentCheckBox = $('#same_as_parent');
parentCheckBox.iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
});
parentCheckBox.on('ifChecked', function (event) {
    var check = event.target.checked;
    var parentId = $('#parents').val();
    if (parentId === '') {
        toastr.error('Please Select Parent');
        setTimeout(function(){
            parentCheckBox.iCheck('uncheck');
        }, 5);
    } else {
        if (check) {
            $.get('/ajax/parent-location', {data: parentId}, function (response) {
                var city = $('#city');
                city.val(response.city_id);
                city.trigger('change');
                $('#address').val(response.address);
            })
        }
    }
});
$('.course').select2({placeholder: 'Select Activity'});
$('.relation').select2({placeholder: 'Select relation'});
$('.batch').select2({placeholder: 'Select Batch'});
$('#parents').select2({placeholder: 'Select Parent'});
$('#city').select2({placeholder: 'Select City'});
$('#school').select2({placeholder: 'Select School'});
$('#blood_group').select2({placeholder: 'Select Blood Group'});
$('#profession').select2({placeholder: 'Select Profession'});
$('#isMember').select2({placeholder: 'Is Membership Fees Paid ?'});
$('#branch_name').select2({placeholder: 'Select Branch'});

function date() {
    $('.date').datetimepicker({
        format: 'MM-YYYY'
    });
    $('.bdate').datetimepicker({
        format: 'DD-MM-YYYY'
    });
}

$('#student_form').submit(function (e) {
    e.preventDefault();
    var data = $(this).serialize();
    $('.error').html('');
    $('.form-group').removeClass('has-error');
    $.post('/students', data, function (response) {
        if (response.result == 'success') {
            $(window).scrollTop(0);
            toastr.success('Student Added Successfully');
            window.location.href = '/students';
        } else {

        }
    }).fail(function (errors) {
        $.each(JSON.parse(errors.responseText).error, function (key, value) {
            $('#error_' + key).html('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> ' + value + '</label>');
            $('#error_' + key).parent().parent().addClass('has-error');
        });
    });
    return false;
});

$('#student_edit_form').submit(function (e) {
    e.preventDefault();
    var url = $(this).attr('action');
    var data = $(this).serialize();
    $('.error').html('');
    $('.form-group').removeClass('has-error');
    $.ajax({
        url: url,
        data: data,
        type: 'patch',
        success: function (response) {
            $(window).scrollTop(0);
            if (response.result == 'success') {
                setTimeout(function () {
                    window.location.href = '/students';
                }, 300);
                toastr.success('Student Updated');
            } else {
                // $.each(response.error, function (key, value) {
                //     $('#error_' + value).html('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> ' + value + ' field is required.</label>');
                // });
                // $.each(response.comboError, function (key, value) {
                //     $('#error_combo').html('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> &nbsp;' + value.courses + '</label>');
                // });
            }
        },
        error: function (errors) {

            $.each(JSON.parse(errors.responseText).error, function (key, value) {
                $('#error_' + key).html('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i> ' + value + '</label>');
                $('#error_' + key).parent().parent().addClass('has-error');
            });

        }
    })
    return false;
});

function courseChange(obj) {
    var id = $(obj).attr('id');
    $('#batch-' + id).html('');
    $.get('/ajax/batches', {
        data: $(obj).val(),
        _token: "{{ csrf_token() }}"
    }, function (response) {
        $.each(response, function (k, value) {
            $('#batch-' + id).append('<option value="' + value.id + '">' + value.name + '</option>');
        });
    });
}
var count = 1;

function addDiv() {
    count = count + 1;
    $('.course').select2("destroy");
    $('.batch').select2("destroy");
    $('#template .course').attr('id', count);
    $('#template .batch').attr('id', 'batch-' + count);
    return $('#template').html();
}
date();
$('.remove-box').click(function () {
    $(this).parent().parent().remove();
});

$('.add-box').click(function (e) {
    var divData = addDiv();
    $('#course_date_div').append('<div>' + divData + '</div>');
    $('.course').select2({placeholder: 'Select Activity'});
    $('.batch').select2({placeholder: 'Select Batch'});
    date();
    $('.remove-box').click(function () {
        $(this).parent().parent().remove();
    })
});

$('.delete-row').unbind('click').click(function () {
    var id = $(this).data('id');
    swal({
        title: "Are you sure to delete this item ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Confirm",
        closeOnConfirm: false,
        cancelButtonText: "Cancel"
    }, function () {
        $.ajax({
            method: 'post',
            url: '/students/' + id,
            data: {
                _method: 'delete',
                _token: LA.token,
            },
            success: function (data) {
                $.pjax.reload('#pjax-container');
                if (typeof data === 'object') {
                    if (data.status) {
                        swal(data.message, '', 'success');
                    } else {
                        swal(data.message, '', 'error');
                    }
                }
            }
        });
    });
});

function pauseCourse(id) {
    swal({
        title: 'Are you sure you want to pause this Activity?',
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,
        reverseButtons: true
    }, function (result) {
        if (result) {
            $.get('/course-pause/' + id, function (response) {

                if (response.status) {
                    toastr.success(response.message);
                    $('.modal-backdrop').hide();
                    $('.student-modal').modal('hide');
                    $.pjax.reload('#pjax-container');
                }

            });
        }

    });
};

function resumeCourse(id) {
    swal({
        title: 'Are you sure you want to Resume this Activity?',
        text: "",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,
        reverseButtons: true
    }, function (result) {

        if (result) {
            $.get('/course-resume/' + id, function (response) {

                if (response.status) {
                    toastr.success(response.message);
                    $('.modal-backdrop').hide();
                    $('.student-modal').modal('hide');
                    $.pjax.reload('#pjax-container');
                }

            });
        }

    });
};

function stopCourse(id, user) {
    $.get('/check-fees/' + id + '/' + user, function (res) {
        var titleText = '';
        var textValue = '';
        if (res == 'false') {
            titleText = 'Fees of this course is Incomplete !';
            textValue = 'Dou you still want to complete this Activity ?';
        } else {
            titleText = 'Are you sure you want to Stop this Activity?';
        }
        //Set ICheckBox
        $("#refund").iCheck({
            checkboxClass: 'icheckbox_square-blue'
        });
        //Ids
        $("#stopActivity").attr('data-userid',user);
        $("#stopActivity").attr('data-courseid',id);
        //Set Heading
        $("#course-stop-heading").text("");
        $("#course-stop-heading").text(titleText);

        $("#course-stop-heading2").text("");
        $("#course-stop-heading2").text(textValue);
        $(".course-stop-modal").modal('show')
        //alert("Refund Model Called")

    });

}

$('#stopActivity').click(function ()
{
    var userId =$(this).data('userid');
    var courseId =$(this).data('courseid');

    $.get('/course-stop/' + courseId, function (response) {

        if (response.status) {
            toastr.success(response.message);
            $('.modal-backdrop').hide();
            $('.student-modal').modal('hide');
            if($("#refund").is(':checked'))
            {
                window.location.href ="/students/refund?userid="+userId+"&courseid="+courseId;
            }
            $.pjax.reload('#pjax-container');
        } else {
            toastr.error(response.message);
        }

    });
});

function courseHistory(id) {
    $.get('/course-history/' + id, function (response) {

        $('.history-data').html(response);
        $('.history-modal').modal('show');
        $('.loader').hide();

    });
}