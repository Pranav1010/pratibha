$('.openModal').click(function () {
    console.log(1);
    var id = $(this).data('id');
    $.get('/student-detail/' + id, function (response) {
        $('.data-div').html(response);
        $('.student-modal').modal('show');
        $('.loader').hide();
    });
});

$('.parent').closest('.form-group').append("<a href='#' class='btn btn-default add-button' data-modal='add-parent-modal' onclick='openModal(this, event)'><i class='fa fa-plus'></i></a>");
$('.school').closest('.form-group').append("<a href='#' class='btn btn-default add-button' data-modal='add-school-modal' onclick='openModal(this, event)'><i class='fa fa-plus'></i></a>");
$('.city_id').closest('.form-group').append("<a href='#' class='btn btn-default add-button' data-modal='add-city-modal' onclick='openModal(this, event)'><i class='fa fa-plus'></i></a>");
$('.professions').closest('.form-group').append("<a href='#' class='btn btn-default add-button' data-modal='add-profession-modal' onclick='openModal(this, event)'><i class='fa fa-plus'></i></a>");

$('.mobile').inputmask('9999999999');

$('input[type="checkbox"].minimal').iCheck({
    checkboxClass: 'icheckbox_square-blue',
});

$('.form-history-back').on('click', function (event) {
    event.preventDefault();
    history.back(1);
});

var parentCheckBox = $('#same_as_parent');
parentCheckBox.on('ifChecked', function (event) {
    var check = event.target.checked;
    var parentId = $('#parents').val();
    if (parentId === '') {
        toastr.error('Please Select Parent');
        setTimeout(function(){
            parentCheckBox.iCheck('uncheck');
        }, 5);
    } else {
        if (check) {
            $.get('/ajax/parent-location', {data: parentId}, function (response) {
                var city = $('#city');
                city.val(response.city_id);
                city.trigger('change');
                $('#address').val(response.address);
            })
        }
    }
});

$('#courses').select2({ placeholder : 'Select Courses'});
$('.relation').select2({placeholder: 'Select relation'});
$('#parents').select2({placeholder: 'Select Parent'});
$('#city').select2({placeholder: 'Select City'});
$('#school').select2({placeholder: 'Select School'});
$('#blood_group').select2({placeholder: 'Select Blood Group'});
$('#profession').select2({placeholder: 'Select Profession'});

function date() {
    $('.date').datetimepicker({
        format: 'MM-YYYY'
    });
    $('.bdate').datetimepicker({
        format: 'DD-MM-YYYY'
    });
}
date();

$('#students_school_form').submit(function (e) {
    e.preventDefault();
    var data = $(this).serialize();
    $('.error').html('');
    $('.form-group').removeClass('has-error');
    $('.students-school [for=inputError]').remove();
    $.post('/students-school', data, function (response) {
        if (response.result == 'success') {
            $(window).scrollTop(0);
            toastr.success('Student Added Successfully');
            window.location.href = '/students-school';
        } else {

        }
    }).fail(function (errors) {
        $.each(JSON.parse(errors.responseText).error, function (key, value) {
            parentElement = $("#" + key.replace(".", "_")).closest(".form-group");
            parentElement.addClass("has-error");
            parentElement.find(".col-sm-8").append('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>' + value[0] + '</label>')
        });
    });
    return false;
});

$('#student_school_edit_form').submit(function (e) {
    e.preventDefault();
    var url = $(this).attr('action');
    var data = $(this).serialize();
    $('.error').html('');
    $('.form-group').removeClass('has-error');
    $('.students-school [for=inputError]').remove();
    $.ajax({
        url: url,
        data: data,
        type: 'patch',
        success: function (response) {
            $(window).scrollTop(0);
            if (response.result == 'success') {
                setTimeout(function () {
                    window.location.href = '/students-school';
                }, 300);
                toastr.success('Student Updated');
            }
        },
        error: function (errors) {

            $.each(JSON.parse(errors.responseText), function (key, value) {
                parentElement = $("#" + key.replace(".", "_")).closest(".form-group");
                parentElement.addClass("has-error");
                parentElement.find(".col-sm-8").append('<label class="control-label" for="inputError"><i class="fa fa-times-circle-o"></i>' + value[0] + '</label>')
            });

        }
    })
    return false;
});

function deleteRow(ele) {
    var id = $(ele).data('id');
    swal({
        title: "Are you sure to delete this item ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Confirm",
        closeOnConfirm: false,
        cancelButtonText: "Cancel"
    }, function () {
        $.ajax({
            method: 'post',
            url: '/students/' + id,
            data: {
                _method: 'delete',
                _token: LA.token,
            },
            success: function (data) {
                $.pjax.reload('#pjax-container');
                if (typeof data === 'object') {
                    if (data.status) {
                        swal(data.message, '', 'success');
                    } else {
                        swal(data.message, '', 'error');
                    }
                }
            }
        });
    });
}
