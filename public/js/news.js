$('form').submit(function (event) {



    $('.help-block').text('');

    event.preventDefault();
    var newsId = $('#newsId').val();
    var response = $(this).serialize();
    var formType = null;
    var formUrl = null;

    if(newsId)
    {
        formType = 'PUT';
        formUrl = '/news/'+newsId;
    }
    else
    {
        formType = 'POST';
        formUrl = '/news';
    }

    $.ajax({

        type: formType,
        url: formUrl,
        data: response,
        success: function (response) {
            if(response == "success"){


                if(newsId)
                {
                    $.pjax({url: "/news", container: '#pjax-container'});
                    toastr.success('Updated Successfully');
                }
                else
                {
                    $.pjax.reload("#pjax-container");
                    toastr.success('Added Successfully');

                }
            }
        },
        error: function (errorThrown) {

            errorThrown = errorThrown.responseJSON;

            if(errorThrown.news_details){
                var errors = errorThrown.news_details;

                $.each(errors, function (key, value) {

                    $('.' + key).text(value);

                    console.log(value);
                });
            }


        }
    })
});

$("body").unbind('click').on("click", '.open-branch-view-modal', function(){
    var id = $(this).data('id');
    NProgress.start();
    $.ajax({
        type:'GET',
        url:'/news/'+ id,
        success:function (response) {
            $("#modal-default-body").html(response);
            NProgress.done();
            $("#modal-default").modal("show");
        },
        error:function (error) {
            toastr.error('Something went wrong.');
        }

    });
});

$("body").on("click", '#goToEdit', function(event){
    event.preventDefault();
    $("#modal-default").modal("hide");
    window.location = $(this).data('href');
});