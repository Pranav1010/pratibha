function addRow()
{
    $("#youtubeRows").append('<div class="form-group youtube-url"> <label class="col-sm-3 control-label">Youtube URL:</label> <div class="col-sm-7"> <input type="url" class="form-control" name="videos[]" placeholder="Input Youtube URL" value=""> <span class="help-block organizer" style="color: red;" id="errorMessage"></span> </div><div class="col-sm-2"> <button class="btn btn-danger btn-sm remove" style="padding: 4px 8px" type="button"> <i class="fa fa-minus" aria-hidden="true"></i> </button> </div></div>');
}

$(document).on( "click",'.remove',function() {
    var row = $(this).parents('.youtube-url');
    row.remove();
});


$('form').submit(function (event) {



    $('.help-block').text('');

    event.preventDefault();
    var eventId = $('#eventId').val();
    var formType = null;
    var formUrl = null;

    if(eventId)
    {
        formType = 'POST';
        formUrl = '/events/'+eventId;
    }
    else
    {
        formType = 'POST';
        formUrl = '/events';
    }
    console.log(new FormData(this));

    $.ajax({

        type: formType,
        url: formUrl,
        data:  new FormData(this),
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        success: function (response) {
            if(response == "success"){


                if(eventId)
                {
                    $.pjax({url: "/events", container: '#pjax-container'});
                    toastr.success('Updated Successfully');
                }
                else
                {
                    $.pjax.reload("#pjax-container");
                    toastr.success('Added Successfully');

                }
            }
        },
        error: function (errorThrown) {

            errorThrown = errorThrown.responseJSON;

            if(errorThrown.event_details){
                var errors = errorThrown.event_details;

                $.each(errors, function (key, value) {

                    $('.' + key).text(value);

                    console.log(value);
                });
            }

            if(errorThrown.image_details){
                var errors = errorThrown.image_details;

                $.each(errors, function (key, value) {

                    $.each(value, function (k,v) {

                        $('.image').text(v);
                        console.log('#' + k + key);
                    })

                });
            }
        }
    })
});

$("body").unbind('click').on("click", '.open-branch-view-modal', function(){
    var id = $(this).data('id');
    NProgress.start();
    $.ajax({
        type:'GET',
        url:'/events/'+ id,
        success:function (response) {
            $("#modal-default-body").html(response);
            NProgress.done();
            $("#modal-default").modal("show");
        },
        error:function (error) {
            toastr.error('Something went wrong.');
        }

    });
});

$("body").on("click", '#goToGallery', function(event){
    event.preventDefault();
    $("#modal-default").modal("hide");
    window.location = $(this).data('href');
});

$("body").on("click", '#goToEdit', function(event){
    event.preventDefault();
    $("#modal-default").modal("hide");
    window.location = $(this).data('href');
});