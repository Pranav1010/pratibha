$(document).ready(function(){

    var div = $("#cheque");
    var payableAmountDiv = $("#payableAmountDiv");

    div.hide();
    payableAmountDiv.hide();

    // Payment Toggle Function
    function paymentToggle(element)
    {
        if(element.is(":checked"))
        {
            var value = element.val();
            if(value == "Cheque")
            {
                div.show();
            }
            else
            {
                div.hide();
            }
        }
    }

    $("input[name=payment_mode]").on('ifChecked', function(event){
        var element = $(this);
        paymentToggle(element)
    });

    $("input[name=refund_mode]").on('ifChecked', function(event){
        var element = $(this);
        paymentToggle(element)
    });

     $("#discount").keyup(function(e){

        var payableAmount = $("#payableAmount");
        var totalAmount = parseFloat($("#totalAmount").val());
        var discount = parseFloat($(this).val());

        if(isNaN(discount))
        {
            payableAmountDiv.hide();
        }
        else
        {
            payableAmountDiv.show();
        }

        if(e.keyCode == 53)
        {
            discount = totalAmount * discount / 100;
            $(this).val(discount);
        }
    
        if(!isNaN(totalAmount) && !isNaN(discount))
        {
            payableAmount.val((totalAmount-discount).toLocaleString());
        }

       

    });
});


$('form').submit(function (event) {



    $('.help-block').text('');

    event.preventDefault();
    var response = $(this).serialize();
    var form = $('#form').val();

    if(form == 'refund')
    {
        var schoolId = $('#schoolId').val();
        postData('/payments/refund/'+schoolId,response,form);
    }
    else
    {
        postData('/payments',response,form);
    }

});


function postData(url,response,form)
{
    $.ajax({

        type: 'POST',
        url: url,
        data: response,
        success: function (response) {
            if(response == "success"){
                $.pjax({url: "/payments", container: '#pjax-container'});

                if(form == 'refund')
                {
                    toastr.success('Refund Successfully');
                }
                else
                {
                    toastr.success('Payment Successfully');
                }

            }
        },
        error: function (errorThrown) {

            errorThrown = errorThrown.responseJSON;

            if(errorThrown.payment_details){
                var details = errorThrown.payment_details;

                $.each(details, function (key, value) {

                    $('.' + key).text(value);

                    console.log(value);
                });
            }
        }
    });

}


$('[data-toggle="popover"]').popover({html: true, placement: "left"});