var count = 0;
function addRow() {

    var contactPersonCount = $("#contactPersonCount").val();

    if(contactPersonCount)
    {
        count = contactPersonCount;
    }

    //update Counter value
    count++;
    if(contactPersonCount)
    {
        $("#contactPersonCount").val(count);
    }
    var row = '<tr class="removeRow' + count + '"><td><div class="row"><div class="col-sm-3"><label for="">Name</label></div><div class="col-sm-9"><input type="text" name="contactDetail[' + count + '][contactname]" class="form-control input-sm" id="name"><span class="help-block contactname-0" style="color: red" id="contactname' + count + '"></span></div></div><div class="row"><div class="col-sm-3"> <label for="">Email</label></div><div class="col-sm-9"><input type="text" name="contactDetail[' + count + '][email]" id="email" class="form-control input-sm"><span class="help-block email" style="color: red" id="email' + count + '"></span></div></div><div class="row"><div class="col-sm-3"><label for="">Mobile</label></div><div class="col-sm-7"><input type="text" name="contactDetail[' + count + '][mobile]" class="form-control input-sm" id="mobile" maxlength="10"><span class="help-block mobile" style="color: red" id="mobile' + count + '"></span></div><div class="col-sm-2"><button type="button" class="btn btn-danger btn-sm pull-right" value="remove" style="padding: 4px 8px" id="removeRow' + count + '" onclick="deleteRow(this.id)"><i class="fa fa-minus" aria-hidden="true"></i></button></div></div></td></tr>';
    $('#contact_person_container').append(row);
}

function deleteRow(id) {
    $("." + id).remove();
}

$('form').submit(function (event) {



    $('.help-block').text('');

    event.preventDefault();
    var response = $(this).serialize();
    var schoolId = $('#schoolId').val();
    var formType = null;
    var formUrl = null;

    if(schoolId)
    {
        formType = 'PUT';
        formUrl = '/schools/'+schoolId;
    }
    else
    {
        formType = 'POST';
        formUrl = '/schools';
    }


    $.ajax({

        type: formType,
        url: formUrl,
        data: response,
        success: function (response) {
            if(response == "success"){
               

                if(schoolId)
                {
                     $.pjax({url: "/schools", container: '#pjax-container'});
                    toastr.success('Updated Successfully');
                }
                else
                {
                    $.pjax.reload("#pjax-container");
                    toastr.success('Added Successfully');

                }
            }
        },
        error: function (errorThrown) {

            errorThrown = errorThrown.responseJSON;

            if(errorThrown.school_details){
                var branch = errorThrown.school_details;

                $.each(branch, function (key, value) {

                    $('.' + key).text(value);

                    console.log(value);
                });
            }

            if(errorThrown.contact_details){
                var contact = errorThrown.contact_details;

                $.each(contact, function (key, value) {

                    $.each(value, function (k,v) {

                        $('#' + k + key).text(v);
                        console.log('#' + k + key);
                    })

                });
            }
        }
    })
});

$("body").on("click", '.open-branch-view-modal', function(){
    var id = $(this).data('id');
    NProgress.start();
    $.ajax({
        type:'GET',
        url:'/schools/'+ id,
        success:function (response) {
            $("#modal-default-body").html(response);
            NProgress.done();
            $("#modal-default").modal("show");
        },
        error:function (error) {
            toastr.error('Something went wrong.');
        }

    });
});




