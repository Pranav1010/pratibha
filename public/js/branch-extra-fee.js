$('form').submit(function (event) {



    $('.help-block').text('');

    event.preventDefault();
    var response = $(this).serialize();
    var branchExtraFeeId = $('#branchExtraFeeId').val();
    var formType = null;
    var formUrl = null;

    if(branchExtraFeeId)
    {
        formType = 'PUT';
        formUrl = '/branch-extra-fee/'+branchExtraFeeId;
    }
    else
    {
        formType = 'POST';
        formUrl = '/branch-extra-fee';
    }


    $.ajax({

        type: formType,
        url: formUrl,
        data: response,
        success: function (response) {
            if(response == "success"){


                if(branchExtraFeeId)
                {
                    $.pjax({url: "/branch-extra-fee", container: '#pjax-container'});
                    toastr.success('Updated Successfully');
                }
                else
                {
                    $.pjax.reload("#pjax-container");
                    toastr.success('Added Successfully');

                }
            }
        },
        error: function (errorThrown) {

            errorThrown = errorThrown.responseJSON;

            if(errorThrown.branch_extra_fee_details){
                var errors = errorThrown.branch_extra_fee_details;

                $.each(errors, function (key, value) {

                    $('.' + key).text(value);

                    console.log(value);
                });
            }
        }
    })
});