var count = 0;
var lastRow = $('#lastRow');
if(lastRow)
{
    count = lastRow.val();
}

function addRow() {
    count++;
    lastRow.val(count);
    var row = '<tr class="removeRow' + count + '"><td colspan="4"><div class="row"><div class="col-sm-3"><label for="">Name</label></div><div class="col-sm-9"><input type="text" name="contactDetail[' + count + '][contactname]" class="form-control input-sm" id="name"><span class="help-block contactname-0" style="color: red" id="contactname' + count + '"></span></div></div><div class="row"><div class="col-sm-3"> <label for="">Email</label></div><div class="col-sm-9"><input type="text" name="contactDetail[' + count + '][email]" id="email" class="form-control input-sm"><span class="help-block email" style="color: red" id="email' + count + '"></span></div></div><div class="row"><div class="col-sm-3"><label for="">Mobile</label></div><div class="col-sm-7"><input type="text" name="contactDetail[' + count + '][mobile]" class="form-control input-sm" id="mobile" maxlength="10"><span class="help-block mobile" style="color: red" id="mobile' + count + '"></span></div><div class="col-sm-2"><button type="button" class="btn btn-danger btn-sm pull-right" value="remove" style="padding: 4px 8px" id="removeRow' + count + '" onclick="deleteRow(this.id)"><i class="fa fa-minus" aria-hidden="true"></i></button></div></div></td></tr>';
    $('#contact_person_container').append(row);
}

$("body").unbind('click').on("click", '.open-branch-view-modal', function(){
    var id = $(this).data('id');
    NProgress.start();
    $.ajax({
        type:'GET',
        url:'/branches/'+ id,
        success:function (response) {
            $("#modal-default-body").html(response);
            NProgress.done();
            $("#modal-default").modal("show");
        },
        error:function (error) {
            toastr.error('Something went wrong.');
        }

    });
});

function deleteRow(id) {
    $("." + id).remove();
}

$('#createBranch').submit(function (event) {

    $('.help-block').text('');

    event.preventDefault();
    var response = $('#createBranch').serialize();

    $.ajax({
        type: "post",
        url: "../branches",
        data: response,
        success: function (response) {
            if(response == "success"){
                $.pjax.reload("#pjax-container");
                toastr.success('Added Successfully');
            }
        },
        error: function (errorThrown) {

            errorThrown = errorThrown.responseJSON;

            if(errorThrown.branch_details){
                var branch = errorThrown.branch_details;

                $.each(branch, function (key, value) {

                    $('.' + key).text(value);

                    console.log(value);
                });
            }

            if(errorThrown.contact_details){
                var contact = errorThrown.contact_details;

                $.each(contact, function (key, value) {

                    $.each(value, function (k,v) {

                        $('#' + k + key).text(v);
                        console.log('#' + k + key);
                    })

                });
            }
        }
    })
});