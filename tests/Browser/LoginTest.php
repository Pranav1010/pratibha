<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Chrome;
use Tests\DuskTestCase;

class LoginTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('center/public/login')
                ->type('email', 'abc@abc.com')
                ->type('password', 'abcabc')
                ->press('Sign In')
                ->assertPathIs('/center/public/home')
                ->assertSee('Dashboard');
        });
    }
}
