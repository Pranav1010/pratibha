



<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Refund Fee</h3>

    </div>
    <form action="{{url('/students/refund')}}" method="POST" class="form-horizontal">

        {{ csrf_field() }}

        <div class="box-body">

            <div class="fields-group">
                <input type="hidden" name="userId" value="{{ $student->id }}">
                <input type="hidden" name="courseId" value="{{ $courseStudent->course->id }}">
                <div class="form-group">
                    <label class="col-sm-4 control-label">Student Name:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" value="{{ $student->name }}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Stop Activity:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" value="{{ $courseStudent->course->activity->name }}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Activity Start:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" value="{{ $courseStudent->start_date }}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Fee Type:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" value="{{ $feeType }}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Total Fee Paid:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" value="{{ $totalPaidFee }}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Pending Fee:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" value="{{ $pendingFee }}" disabled>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Refund Amount:</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" placeholder="Please Enter Refund Amount" name="refundAmount" required>
                        <span class="help-block refundAmount" style="color: red" id="errorMessage"></span>
                    </div>
                </div>

            </div>
        </div>

        <div class="box-footer">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="btn-group pull-right">
                    <button type="submit" class="btn btn-info pull-right" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit</button>
                </div>
                <div class="btn-group pull-left">
                    <button type="reset" class="btn btn-warning">Reset</button>
                </div>
            </div>
        </div>

    </form>
    <!-- /.box-body -->
</div>
<style>
    hr {
        border: 0;
        width: 96%;
        height: 1px;
    }
</style>

<script>
    $('form').submit(function () {
        $('.help-block').text('');
        event.preventDefault();
        var response = $(this).serialize();

        $.ajax({

            type: "POST",
            url: "/students/refund",
            data: response,
            success: function (response) {
                if(response == "success"){
                    $.pjax({url: "/students", container: '#pjax-container'});
                    toastr.success('Refund Successfully');
                }
            },
            error: function (errorThrown) {
                errorThrown = errorThrown.responseJSON;
                if(errorThrown.error)
                {
                    var error = errorThrown.error;

                    $.each(error, function (key, value) {

                        $('.' + key).text(value);
                    });
                }
            }
        })

    });
</script>