<style>
    .table:not(.no-header) tbody tr:first-child {
        background-color: white;
    }

    .modal-header {
        background-color: #e5e5e5;
    }
</style>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="myModalLabel"> {{ $course->name }} History </h4>
</div>
<div class="modal-body">
    <div class="text-center">
        <i class="fa fa-spin fa-spinner fa-2x loader"></i>
    </div>
    <div class="model-data">
        <div class="" style="padding-left: 0px !important;">
            <img src="{{ url('img/start.png') }}" width="25px;">
            <b style="padding-left: 10px;">
                @if(date('m/Y') < date("m/Y", strtotime('01-' . $courseStudent->start_date)))
                    Course Will Start At {{ date('M Y' , strtotime('01-'.$courseStudent->start_date)) }}
                @else
                    Course Started At {{ date('M Y' , strtotime('01-'.$courseStudent->start_date)) }}
                @endif
            </b>
        </div>
        @if($statuses->count() > 0)
            <table class="table table-responsive table-stripped"
                   style="color: #797979;margin-top: 20px;">
                <tbody>
                <tr>
                    <th class="col-md-2">&nbsp;</th>
                    <th class="col-md-4">
                        <b> <i class="fa fa-pause-circle-o" aria-hidden="true"></i> Paused At </b>
                    </th>
                    <th class="col-md-4">
                        <b> <i class="fa fa-play-circle-o" aria-hidden="true"></i> Resumed At </b>
                    </th>
                </tr>
                @foreach($statuses as $key => $value)
                    <tr>
                        <td>&nbsp;</td>
                        <td> {{ date('d - M - Y',strtotime($value->start_date))}} </td>

                        <td>{{ ($value->end_date != null) ? date('d - M - Y',strtotime($value->end_date)) : ' - '}} </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div height="20px;"> &nbsp;</div>
        @endif
        @if($courseStudent->end_date != null)
            <div class="" style="padding-left: 0px !important;">
                <img src="{{ url('img/stop.png') }}" width="25px;">
                <b style="padding-left: 10px;">
                    Course Completed At {{ date('M Y' , strtotime('01-'.$courseStudent->end_date)) }}
                </b>
            </div>
        @endif
    </div>
</div>

