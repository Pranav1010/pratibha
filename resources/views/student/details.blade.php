<style>
    ul {
        margin-top: 10px;
    }

    .modal-header {
        background-color: #e5e5e5;
    }

    .title-span {
        padding-left: 50px;
    }
</style>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
    </button>
    <h4 class="modal-title" id="myModalLabel"> Student </h4>
</div>
<div class="modal-body">
    <div class="text-center">
        <i class="fa fa-spin fa-spinner fa-2x loader"></i>
    </div>
    <div class="model-data">
        <table class="table no-header">
            <tr>
                <td class="col-md-12">
                    <div class="col-md-6">
                        <div style="padding-bottom: 10px;"><img src=" {{ url("img/personal-info.png") }}" width="25px">
                            <span class="title-span"> <b> Personal Information  </b> </span>
                        </div>

                        <table class="table table-responsive table-striped no-header" style="color: #797979;">
                            <tbody>
                            <tr>
                                <td class="col-md-2"><b> Name </b></td>
                                <td class="col-md-4"> {{ $student->name }} </td>
                                <td class="col-md-2"><b> Email </b></td>
                                <td class="col-md-4"> {{ isset($student->email) ? $student->email : '-' }} </td>

                            </tr>

                            <tr>
                                <td><b> Mobile </b></td>
                                <td> {{ isset($student->mobile) ? $student->mobile : '-' }} </td>
                                <td><b> Birth Date </b></td>
                                <td> {{ isset($student->student->bdate) ? Carbon\Carbon::createFromFormat('Y-m-d', $student->student->bdate)->toFormattedDateString() : '-' }} </td>
                            </tr>

                            <tr>
                                <td><b> City </b></td>
                                <td> {{ isset($student->city) ? $student->city->name : '-' }} </td>
                                <td><b> Blood Group </b></td>
                                <td> {{ (isset($student->student->student_blood_group) || ($student->student->student_blood_group) == 0) ? config('app.blood_group')[$student->student->student_blood_group] : '-' }} </td>
                            </tr>

                            <tr>
                                <td><b> School </b></td>
                                <td> {{ $student->student->school->name or '-'}} </td>
                                <td><b> Address </b></td>
                                <td> {{ isset($student->address) ? $student->address : '-' }} </td>
                            </tr>
                            <tr>
                                <td><b> Aadhar No </b></td>
                                <td> {{ isset($student->student->aadhar_no) ? $student->student->aadhar_no : '-' }} </td>
                                <td><b>Branch Name</b></td>
                                <td>{{ (isset(App\Branch::select('name')->find($student->student->branch_id)->name))? App\Branch::select('name')->find($student->student->branch_id)->name : ''}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <div style="padding-bottom: 10px;"><img src=" {{ url("img/male.png") }}" width="25px">
                            <span class="title-span"> <b> Father's Information </b> </span>
                        </div>

                        <table class="table table-responsive table-striped no-header" style="color: #797979;">

                            <tbody>

                            <tr>
                                <td class="col-md-2"><b> Name </b></td>
                                <td class="col-md-4"> {{ (isset($student->student->parent->father_name) ? $student->student->parent->father_name : '-') }} </td>
                                <td class="col-md-2"><b> Mobile </b></td>
                                <td class="col-md-4"> {{ isset($student->student->parent->father_mobile) ? $student->student->parent->father_mobile : '-' }} </td>
                            </tr>

                            <tr>
                                <td class="col-md-2"><b> Email </b></td>
                                <td> {{ isset($student->student->parent->father_email) ? $student->student->parent->father_email : '-' }} </td>
                                <td><b> Profession </b></td>
                                <td> {{ isset($student->student->parent->father_profession) ? $student->student->parent->fatherProfession->name : '-' }} </td>
                            </tr>
                            <tr>
                                <td class="col-md-2"><b> Blood Group </b></td>
                                <td> {{ isset($student->student->parent->father_blood_group) && ($student->student->parent->father_blood_group) != 0 ? config('app.blood_group')[$student->student->parent->father_blood_group] : '-' }} </td>

                                <td><b> Aadhar No </b></td>
                                <td> {{ isset($student->student->parent->aadhar_no_father) ? $student->student->parent->aadhar_no_father : '-' }} </td>

                            </tr>


                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="col-md-12">
                    <div class="col-md-6">
                        <div style="padding-bottom: 10px;"><img src=" {{ url("img/user.png") }}" width="25px">
                            <span class="title-span"> <b> Mother's Information </b> </span>
                        </div>

                        <table class="table table-responsive table-striped no-header" style="color: #797979;">

                            <tbody>

                            <tr>
                                <td class="col-md-2"><b> Name </b></td>
                                <td class="col-md-4">
                                    {{ isset($student->student->parent->mother_name) ? $student->student->parent->mother_name : '-' }}
                                </td>
                                <td class="col-md-2"><b> Mobile </b></td>
                                <td class="col-md-4"> {{ isset($student->student->parent->mother_mobile) ? $student->student->parent->mother_mobile : '-' }} </td>
                            </tr>

                            <tr>
                                <td class="col-md-2"><b> Email </b>
                                </th>
                                <td> {{ isset($student->student->parent->mother_email) ? $student->student->parent->mother_email : '-' }} </td>
                                <td><b> Profession </b>
                                </th>
                                <td> {{ isset($student->student->parent->mother_profession) ? $student->student->parent->motherProfession->name : '-' }} </td>
                            </tr>
                            <tr>
                                <td class="col-md-2"><b> Blood Group </b></td>
                                <td> {{ (isset($student->student->parent->mother_blood_group) && ($student->student->parent->mother_blood_group) != 0) ? config('app.blood_group')[$student->student->parent->mother_blood_group] : '-' }} </td>
                                <td><b> Aadhar No </b></td>
                                <td> {{ isset($student->student->parent->aadhar_no_mother) ? $student->student->parent->aadhar_no_mother : '-' }} </td>
                            </tr>

                            <tr>


                            </tr>

                            </tbody>

                        </table>
                    </div>

                    <div class="col-md-6">
                        <div style="padding-bottom: 10px;"><img src=" {{ url("img/guardian.png") }}" width="25px">
                            <span class="title-span"> <b> Guardian's Information </b> </span>
                        </div>

                        <table class="table table-responsive table-striped no-header" style="color: #797979;">

                            <tbody>

                            <tr>
                                <td class="col-md-2"><b> Name </b></td>
                                <td class="col-md-4">
                                    {{ isset($student->student->guardian_name) ? $student->student->guardian_name : '-' }}
                                </td>
                                <td class="col-md-2"><b> Mobile </b></td>
                                <td class="col-md-4"> {{ isset($student->student->guardian_mobile) ? $student->student->guardian_mobile : '-' }} </td>
                            </tr>

                            <tr>
                                <td class="col-md-2"><b> Email </b></td>
                                <td> {{ isset($student->student->guardian_email) ? $student->student->guardian_email : '-' }} </td>
                                <td><b> Profession </b></td>
                                <td> {{ isset($student->student->guardian_profession) ? $student->student->profession->name : '-' }} </td>
                            </tr>
                            <tr>
                                <td class="col-md-2"><b> Relation </b></td>
                                <td class="col-md-4"> {{ isset($student->student->guardian_relation) ? config('app.relation')[$student->student->guardian_relation] : '-' }} </td>
                                <td></td>
                                <td></td>
                            </tr>

                            <tr>


                            </tr>

                            </tbody>

                        </table>
                    </div>

                </td>
            </tr>
            <tr>
                <td colspan="2" class="col-md-12">
                    <div class="col-md-12">
                        <div style="padding-bottom: 10px;"><img src=" {{ url("img/activity.png") }}" width="25px">
                            <span class="title-span"> <b> Activities Information </b> </span>
                        </div>
                        @if(count($courses) > 0)
                            <table class="table col-md-12 table-responsive table-striped no-header">
                                @foreach($courses as $k => $value)

                                    <tr>
                                        <td class="col-md-7">
                                            <i class="fa fa-arrow-circle-right" aria-hidden="true"
                                               style="padding-right:10px;"></i>
                                            <b> {{  $value->course->activity->name  }} </b> - {{ $value->batch->name }}
                                        </td>
                                        <td class="col-md-3">
                                            @if($value->status == 0)
                                                <span class="label label-success">
                                            {{ $value->display_status }}
                                        </span>
                                            @elseif($value->status == 1)
                                                <span class="label " style="background-color: #f18917;">
                                            {{ $value->display_status }}
                                        </span>
                                            @elseif($value->status == 2)
                                                <span class="label label-danger">
                                            {{ $value->display_status }}
                                        </span>
                                            @endif
                                        </td>
                                        <td class="col-md-2">
                                            @if($value->status == 0)
                                                <button class="btn btn-primary btn-xs "
                                                        onclick="pauseCourse({{ $value->id }})"
                                                        title="Pause Course">
                                                    <i class="fa fa-pause" aria-hidden="true"></i>
                                                </button>
                                                <button class="btn btn-danger btn-xs"
                                                        onclick="stopCourse({{ $value->id }} , {{ $value->student_id }})"
                                                        title="Stop Course">
                                                    <i class="fa fa-stop" aria-hidden="true"></i>
                                                </button>
                                            @elseif($value->status == 1)
                                                <button class="btn btn-primary btn-xs "
                                                        onclick="resumeCourse({{ $value->id }})"
                                                        title="Resume Course">
                                                    <i class="fa fa-play" aria-hidden="true"></i>
                                                </button>
                                                <button class="btn btn-danger btn-xs"
                                                        onclick="stopCourse({{ $value->id }} , {{ $value->student_id}})"
                                                        title="Stop Course">
                                                    <i class="fa fa-stop" aria-hidden="true"></i>
                                                </button>
                                            @elseif($value->status == 2)
                                                <button class="btn btn-primary btn-xs"
                                                        title="Generate Certificate ">
                                                    <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                                </button>
                                            @endif
                                            <button class="btn btn-success btn-xs"
                                                    onclick="courseHistory({{ $value->id }})"
                                                    title="Course History">
                                                <i class="fa fa-eye" aria-hidden="true"></i>
                                            </button>

                                        </td>
                                    </tr>

                                @endforeach
                            </table>
                        @else
                            <div style="padding-left: 20px;color: #797979; "> No Activity Selected Yet !</div>
                        @endif
                    </div>

                </td>
            </tr>
        </table>
    </div>
</div>
