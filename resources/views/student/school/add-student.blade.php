<form action="/students-school" id="students_school_form" method="post" pjax-container class="form-horizontal">
    {{ csrf_field() }}
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Create</h3>

                <div class="box-tools">
                    <div class="btn-group pull-right" style="margin-right: 10px">
                        <a href="/students-school" class="btn btn-sm btn-default"><i class="fa fa-list"></i>&nbsp;List</a>
                    </div>
                    <div class="btn-group pull-right" style="margin-right: 10px">
                        <a class="btn btn-sm btn-default form-history-back"><i
                                    class="fa fa-arrow-left"></i>&nbsp;Back</a>
                    </div>
                </div>
            </div>
            <div class="container-fluid" style="padding-top: 10px;">
                <div class="form-group">
                    <label class="col-sm-2 control-label"><img src="{{ url('img/boy.png') }}" width="25px"></label>
                    <div class="col-sm-8">
                        <h4> Student's Information </h4>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                            <input type="text" id="name" name="name" value="" class="form-control name"
                                   placeholder="Input Name">
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2"> Courses </label>
                    <div class="col-sm-8">
                        <select class="form-control courses" name="courses[]" id="courses" multiple="multiple">
                            <option></option>
                            @foreach($courses as $course)
                                <option value="{{ $course->id }}"> {{ $course->name }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Parent</label>

                    <div class="col-sm-8">
                        <select class="form-control parent" id="parents" placeholder="Select Parent" name="parent">
                            <option></option>
                            @foreach($parents as $k => $val)
                                <option value="{{ $val->id }}"> {{ $val->father_name }}
                                    - {{ $val->father_mobile }} </option>
                            @endforeach
                        </select>

                    </div>

                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">Birth Date</label>
                    <div class="col-sm-8" style="">
                        <input type="text" name="bdate" placeholder="Birth Date" class="bdate form-control">
                    </div>
                </div>
                <div class="form-group  ">
                    <label for="mobile" class="col-sm-2 control-label">Primary Contact</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                            <input style="width: 150px" maxlength="10" type="text" id="mobile" name="mobile" value=""
                                   class="form-control mobile" placeholder="Primary Contact">
                        </div>

                    </div>

                </div>
                <div class="form-group ">
                    <label for="mobile" class="col-sm-2 control-label">Secondary Contact</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                            <input style="width: 150px" maxlength="10" type="text" id="second_contact_no"
                                   name="second_contact_no" value=""
                                   class="form-control mobile" placeholder="Secondary Contact">
                        </div>

                    </div>

                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                            <input type="text" id="email" name="email" value="" class="form-control email"
                                   placeholder="Input Email">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label"> Blood Group </label>

                    <div class="col-sm-8">
                        <select class="form-control" placeholder="Select Blood Group" id="blood_group">
                            <option></option>
                            @foreach(config('app.blood_group') as $k => $group)
                                <option value="{{ $k }}"> {{ $group }} </option>
                            @endforeach
                        </select>

                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">School</label>

                    <div class="col-sm-8">
                        <select placeholder="Select School" class="form-control school" name="school" id="school">
                            <option></option>
                            @foreach($schools as $k => $val)
                                <option value="{{ $val->id }}"> {{ $val->name }} </option>
                            @endforeach
                        </select>

                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Same As Parent</label>

                    <div class="col-sm-8 ">
                        <input type="checkbox" name="same_as_parent" id="same_as_parent" class="minimal">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">City</label>

                    <div class="col-sm-8">
                        <select placeholder="Select City" class="form-control city_id" name="city" id="city">
                            <option></option>
                            @foreach($cities as $k => $val)
                                <option value="{{ $val->id }}"> {{ $val->name }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group ">
                    <label for="address" class="col-sm-2 control-label">Address</label>

                    <div class="col-sm-8">
                        <textarea id="address" name="address" class="form-control" rows="5"
                                  placeholder="Input Address"></textarea>

                    </div>

                </div>

                <div class="form-group ">
                    <label for="address" class="col-sm-2 control-label">Aadhar No</label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="aadhar_no" name="aadhar_no"
                               placeholder="Input Aadhar No">
                    </div>

                </div>

            </div>
        </div>
    </div>

    {{--<div class="col-md-6">
        <div class="box box-info">
            <div class="container-fluid" style="padding-top: 10px;">

                <input type="hidden" name="is_inquiry" value="0">

                <div class="form-group">
                    <label class="col-sm-2 control-label"><img src="{{ url('img/activity.png') }}" width="25px"></label>
                    <div class="col-sm-8">
                        <h4> Enroll To Activity </h4>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2"> Courses </label>
                    <div class="col-sm-8">
                        <select class="form-control courses" name="courses[]" id="courses" multiple="multiple">
                            <option></option>
                            @foreach($courses as $course)
                                <option value="{{ $course->id }}"> {{ $course->name }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group  pull-right">
                    <div class="col-sm-12 pull-right">
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}

</form>
