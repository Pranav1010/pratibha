
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Edit</h3>

    </div>
    <form action="{{url('/events/'.$event->id)}}" method="POST" class="form-horizontal" enctype="multipart/form-data">

        {{ csrf_field() }}
        {{method_field('PUT')}}

         <input type="hidden" value="{{$event->id}}" id="eventId" name="eventId">

        @include('event.form')

    </form>
    <!-- /.box-body -->
</div>

<style>
    hr {
        border: 0;
        width: 96%;
        height: 1px;
    }
</style>
<script>


</script>