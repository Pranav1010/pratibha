<div class="box-body">
    <input type="hidden" value="{{  $branch->id or ''}}" name="branch_id">
    <div class="fields-group">
        <div class="form-group">
            <label class="col-sm-3 control-label">Event Title:</label>
            <div class="col-sm-8">
                <input type="text" class="form-control " name="name" placeholder="Input Event Title"
                       value="{{ $event->title or '' }}" autofocus>
                <span class="help-block name" style="color: red"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Organizer:</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="organizer" placeholder="Input Event Organizer"
                       value="{{ $event->organizer or '' }}">
                <span class="help-block organizer" style="color: red;" id="errorMessage"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Start Date & Time:</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="start_date" placeholder="Input Event Start Date And Time"
                       id="start" value="{{ isset($event->start_date) ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$event->start_date)->format('d/m/Y h:i a') : '' }}">
                <span class="help-block start_date" style="color: red;" id="errorMessage"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">End Date & Time:</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="end_date" placeholder="Input Event End Date And Time"
                       id="end" value="{{ isset($event->start_date) ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$event->end_date)->format('d/m/Y h:i a') : '' }}">
                <span class="help-block end_date" style="color: red;" id="errorMessage"></span>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label">Location:</label>
            <div class="col-sm-8">
                <textarea name="location" placeholder="Input Event Location" class="form-control"
                          rows="4"> {{ (isset($event->location)) ? $event->location : '' }} </textarea>
                <span class="help-block location" style="color: red" id="errorMessage"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Description:</label>
            <div class="col-sm-8">
                <textarea name="description" placeholder="Input Event Description" class="form-control" rows="4"
                          id="summernote"> {{ (isset($event->description)) ? $event->description : '' }} </textarea>
                <span class="help-block description" style="color: red" id="errorMessage"></span>
            </div>
        </div>

    </div>
    <div id="youtubeRows">

    </div>
    @if(isset($event->gallery))
        @foreach($event->gallery as $video)
            @if($video->type == 'video')
                <div class="form-group youtube-url">
                    <label class="col-sm-3 control-label">Youtube URL:</label>
                    <div class="col-sm-7">
                        <input type="url" class="form-control" name="videos[]" placeholder="Input Youtube URL" value="{{ $video->path }}"> <span class="help-block video" style="color: red;" id="errorMessage"></span> </div>
                    <div class="col-sm-2">
                        <button class="btn btn-danger btn-sm remove" style="padding: 4px 8px" type="button"> <i class="fa fa-minus" aria-hidden="true"></i> </button>
                    </div>
                </div>
            @endif
        @endforeach

    @endif
    <div class="form-group">
        <label class="col-sm-3 control-label">Youtube URL:</label>
        <div class="col-sm-7">
            <input type="url" class="form-control" name="videos[]" placeholder="Input Youtube URL" value="">
            <span class="help-block video[]" style="color: red;" id="errorMessage"></span>
        </div>
        <div class="col-sm-2">
            <button class="btn btn-default btn-sm" style="padding: 4px 8px" type="button" onclick="addRow()">
                <i class="fa fa-plus" aria-hidden="true"></i>
            </button>
        </div>

    </div>
</div>

<div class="box-footer">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="btn-group pull-right">
            <button type="submit" class="btn btn-info pull-right"
                    data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit
            </button>
        </div>
        <div class="btn-group pull-left">
            <button type="reset" class="btn btn-warning">Reset</button>
        </div>
    </div>
</div>
@include('event.modal.eventDetails');