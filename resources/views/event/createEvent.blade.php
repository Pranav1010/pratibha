
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Create New Event</h3>

    </div>
    <form action="{{url('/events')}}" method="POST" class="form-horizontal" enctype="multipart/form-data">

        {{ csrf_field() }}

        @include('event.form')

    </form>
    <!-- /.box-body -->
</div>

<style>
    hr {
        border: 0;
        width: 96%;
        height: 1px;
    }
</style>