<h1 class="my-4 text-center text-lg-left">Event Gallery</h1>

<div class="row text-center text-lg-left gallery" id="lightgallery">

    @foreach($event->gallery()->orderBy('id','desc')->get() as $image)
        @if($image->type == 'image')
            <div class="col-lg-3 col-md-4 col-xs-6 image-bottom-margin">
                <div class="image" data-src="upload/images/{{ $image->path }}">
                    <img class="img-fluid img-thumbnail overlay b-lazy" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="upload/thumbnail/{{ $image->path }}" alt="">
                </div>
                <a class="delete-button" style="width: 25px" id="{{ $image->id }}">
                    <img src="/img/cancel.svg" alt="Delete" style="width: 100%">
                </a>
            </div>
        @endif
    @endforeach
</div>
