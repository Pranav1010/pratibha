<div class="box box-info">
    <form action="{{ (isset($activity))? url('/adminactivity').'/'.$activity->id : url('/courses') }}" method="POST" onsubmit="createCourse(this, event)" class="form-horizontal"
          id="editActivity">

        <div class="box-header with-border">
            <h3 class="box-title">{{ (isset($activity))? 'Edit':'Create' }}</h3>

            <div class="box-tools">
                <div class="btn-group pull-right" style="margin-right: 10px">
                    <a href="/courses" class="btn btn-sm btn-default"><i class="fa fa-list"></i>&nbsp;List</a>
                </div>
                <div class="btn-group pull-right" style="margin-right: 10px">
                    <a class="btn btn-sm btn-default form-history-back"
                       onclick="window.history.go(-1); return false;"><i
                                class="fa fa-arrow-left"></i>&nbsp;Back</a>
                </div>

            </div>
        </div>

        {{ csrf_field() }}

        <div class="box-body">

            <div class="fields-group">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="name" id="name" class="form-control" placeholder="Activity Name" value="{{ (isset($activity))? $activity->name :''}}">
                        @if(isset($activity))
                            <input type="hidden" name="activityId" value="{{ $activity->id }}">
                        @endif
                    </div>
                </div>
            </div>
        </div>
            <div class="box-footer">
                <div class="col-md-2"></div>
                <div class="col-md-9">
                    <div class="btn-group pull-right">
                        <button type="submit" class="btn btn-info pull-right"
                                data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit
                        </button>
                    </div>
                </div>
            </div>
    </form>
</div>

@if(isset($activity))
    <script>
        $('#editActivity').submit(function( event ) {
            event.preventDefault();

            var data = $(this).serialize();

            $.ajax({

                type: 'POST',
                url: window.location.href,
                data: data,
                success: function (response) {
                    console.log(response);
                    if(response == "success")
                    {
                            $.pjax({url: "/courses", container: '#pjax-container'});
                            toastr.success('Updated Successfully');
                    }
                },
                error: function (errorThrown) {

                    errorThrown = errorThrown.responseJSON;

                    if(errorThrown.school_details){
                        var branch = errorThrown.school_details;

                        $.each(branch, function (key, value) {

                            $('.' + key).text(value);

                            console.log(value);
                        });
                    }
                }
            })
        });
    </script>
@endif