<div class="box box-info">
    <form action="{{url('/courses')}}" method="POST" onsubmit="createCourse(this, event)" class="form-horizontal"
          id="createBranch">

        <div class="box-header with-border">
            <h3 class="box-title">Create</h3>

            <div class="box-tools">
                <div class="btn-group pull-right" style="margin-right: 10px">
                    <a href="/courses" class="btn btn-sm btn-default form-history-back"><i class="fa fa-arrow-left"></i>&nbsp;Back</a>
                </div>
            </div>
        </div>

        {{ csrf_field() }}

        <div class="box-body">

            <div class="fields-group">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-3">
                            <select name="name" id="name" style="width: 100% !important;">
                                @foreach(\App\Activity::orderBy('name','ASC')->get() as $activity)
                                    @if($activity->id != 1)
                                        <option value="{{ $activity->id }}">{{ $activity->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        <span class="help-block name" style="color: red;"></span>
                    </div>
                </div>
                @if(isset($branch))
                    <input type="hidden" name="branchId" value="{{$branch[0]->id}}">
                @endif

                @if(isRole('administrator') || isRole('super-administrator'))
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Branch</label>
                        <div class="col-sm-3">
                            <select name="branchId" class="selectBranch" style="width: 100% !important;">
                                @foreach(\App\Branch::all() as $branch)
                                    <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                @endforeach
                            </select>
                            <span class="help-block branchId" style="color: red;"></span>
                        </div>
                    </div>
                @endif

                <div class="form-group">
                    <label class="col-sm-2 control-label">Fee Types</label>
                </div>
                @foreach($feeTypes as $key => $type)
                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-9">
                            <div class="col-md-12" style="padding-top: 5px;">
                                <div class="col-sm-3">
                                    <label>
                                        <input type="checkbox" class="checkbox-minimal-blue"
                                               name="fees[{{$key}}][check]">
                                        <span style="padding-left: 10px;"> {{ $type['name'] }} </span>
                                    </label>

                                </div>

                                <div class="col-sm-6">
                                    <input type="text" placeholder=" {{ $type['name'] }} Fees"
                                           class="form-control" id="fees_{{$key}}"
                                           name="fees[{{$key}}][fees]">
                                    <input type="hidden" name="fees[{{$key}}][id]" value="{{$type['id']}}">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-8 error" id="fees_{{$key}}_error"></div>
                            </div>
                        </div>
                    </div>
                @endforeach

                @if(count($feeTypes) == 0)
                    <div class="col-sm-9">
                        <h5> No Data In Fees Types</h5>
                    </div>
                @endif
            </div>

            <div class="box-footer">
                <div class="col-md-2"></div>
                <div class="col-md-9">
                    <div class="btn-group pull-right">
                        <button type="submit" class="btn btn-info pull-right"
                                data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>