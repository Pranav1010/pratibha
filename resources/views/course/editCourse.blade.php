<div class="box box-info">
    <form action="{{url('/courses/' . $course->id)}}" method="POST" onsubmit="updateCourse(this, event)"
          class="form-horizontal"
          id="createBranch">
        <div class="box-header with-border">
            <h3 class="box-title">Edit</h3>

            <div class="box-tools">
                <div class="btn-group pull-right" style="margin-right: 10px">
                    <a class="btn btn-sm btn-default form-history-back"
                       onclick="window.history.go(-1); return false;"><i
                                class="fa fa-arrow-left"></i>&nbsp;Back</a>
                </div>

            </div>
        </div>

        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT">
        <div class="box-body">

            <div class="fields-group">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-3">
                        <select name="name" style="width: 100%;">
                            @foreach(\App\Activity::all() as $activity)
                                @if($activity->id != 1)
                                    <option value="{{ $activity->id }}" {{ ($activity->id == $course->activity_id)? 'selected':'' }}>{{ $activity->name }}</option>
                                @endif
                            @endforeach
                        </select>
                        <span class="help-block name" style="color: red;"></span>
                        <input type="hidden" name="activityId" value="{{ $course->activity_id }}">
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8 error" id="name_error"></div>
                    </div>
                </div>
                    <input type="hidden" name="branchId" value="{{$course->branch_id}}">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Fee Types</label>
                </div>
                @foreach($feeTypes as $key => $type)
                    <div class="form-group">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-9">
                            <div class="col-md-12" style="padding-top: 5px;">
                                <div class="col-sm-3">
                                    <label>
                                        <input type="checkbox" class="checkbox-minimal-blue"
                                               name="fees[{{$key}}][check]" {{ (array_key_exists($type['id'],$feeTypeIds)) ? 'checked' : '' }}>
                                        <span style="padding-left: 15px;">
                                            {{ $type['name'] }}
                                        </span>
                                    </label>

                                </div>

                                <div class="col-sm-6">
                                    <input type="text" placeholder=" {{ $type['name'] }} Fees" class="form-control"
                                           name="fees[{{$key}}][fees]"
                                           value="{{ (array_key_exists($type['id'],$feeTypeIds)) ? $feeTypeIds[$type['id']] : '' }}"
                                           id="fees_{{ $key }}">
                                    <input type="hidden" name="fees[{{$key}}][id]" value="{{$type['id']}}">
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            @if(count($feeTypes) == 0)
                <div class="col-sm-9">
                    <h5> No Data In Fees Types</h5>
                </div>
            @endif
        </div>

        {{--<div class="form-group">
            <label class="col-sm-2 control-label"> Fees For School </label>
            <div class="col-sm-9">
                <input type="text" placeholder=" School Fees " id="school_fees" value="{{ $course->school_fees }}"
                       class="form-control"
                       name="school_fees">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label"> School Term </label>
            <div class="col-sm-9">
                <div class="col-sm-12">
                    <div class="col-sm-3">
                        <label>
                            <input type="radio" value="0" class="radio-minimal-blue"
                                   name="category" {{ ($course->category == 0) ? 'checked' : '' }}>
                            <span style="padding-left: 15px;">
                                Half Yearly
                            </span>
                        </label>

                    </div>

                    <div class="col-sm-3">
                        <label>
                            <input type="radio" value="1" class="radio-minimal-blue"
                                   name="category" {{ ($course->category == 1) ? 'checked' : '' }}>
                            <span style="padding-left: 15px;">
                                Yearly
                            </span>
                        </label>
                    </div>
                </div>
            </div>
        </div>--}}

        <div class="box-footer">
            <div class="col-md-2"></div>
            <div class="col-md-9">
                <div class="btn-group pull-right">
                    <button type="submit" class="btn btn-info pull-right"
                            data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
