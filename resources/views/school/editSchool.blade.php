
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Edit</h3>

    </div>
    <form action="{{url('/schools/'.$school->id)}}" method="POST" class="form-horizontal">

        {{ csrf_field() }}
        {{method_field('PUT')}}

         <input type="hidden" value="{{$school->id}}" id="schoolId" name="schoolId">

        @include('school.form')

    </form>
    <!-- /.box-body -->
</div>

 @include('school.modal.schoolDetails')

<style>
    hr {
        border: 0;
        width: 96%;
        height: 1px;
    }
</style>
<script>


</script>