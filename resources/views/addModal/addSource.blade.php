<div class="modal fade" id="add-source-modal" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" accept-charset="UTF-8" id="add-source-form" data-url="/sources/add-source" onsubmit="addData(this.id, event)"
                  class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Add Source</h4>
                </div>
                <div class="modal-body">

                    {{ csrf_field()}}

                    <div class="fields-group">
                        <div class="form-group  ">
                            <label for="name" class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                    <input type="text" id="name" name="name" value="" class="form-control name"
                                           placeholder="Input Name">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-8">
                        <div class="btn-group pull-right">
                            <button type="submit" class="btn btn-info pull-right"
                                    data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit
                            </button>
                        </div>
                        <div class="btn-group pull-left">
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

