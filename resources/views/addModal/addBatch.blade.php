<div class="modal fade courses" id="add-batch-modal" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <form data-url="{{url('/batches/add')}}" method="POST" onsubmit="addData(this.id, event)"
                  class="form-horizontal"
                  id="add-batch-form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Add Batch</h4>
                </div>

                {{ csrf_field() }}

                <div class="box-body">

                    <div class="fields-group">

                        <div class="form-group">
                            <label for="name" class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                    <input type="text" id="name" name="name" value="" class="form-control name"
                                           placeholder="Input Name">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="course" class="col-sm-3 control-label"> Activity </label>
                            <div class="col-sm-8">
                                <select name="course_id" id="course_id" class=" form-control course_id">
                                    <option></option>
                                    @if(isset($courses))
                                        @foreach($courses as $course)
                                            <option value="{{ $course->id }}"> {{ $course->activity->name }}{{ showActivityBranchName($course) }} </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <div class="col-md-3">
                    </div>
                    <div class="col-md-8">
                        <div class="btn-group pull-right">
                            <button type="submit" class="btn btn-info pull-right"
                                    data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit
                            </button>
                        </div>
                        <div class="btn-group pull-left">
                            <button type="reset" class="btn btn-warning">Reset</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
