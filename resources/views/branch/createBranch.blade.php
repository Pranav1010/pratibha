
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Create</h3>
    </div>

    <form action="{{url('/branches')}}" method="POST" class="form-horizontal" id="createBranch">

        {{ csrf_field() }}

        <div class="box-body">

            <div class="fields-group">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Branch Name:</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-pencil"></i>
                            </span>
                            <input type="text" class="form-control " name="name" placeholder="Input Branch Name" autofocus>
                        </div>
                        <span class="help-block name" style="color: red"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Contact No:</label>
                    <div class="col-sm-8">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-pencil"></i>
                            </span>
                            <input type="text" class="form-control" name="contact_no" placeholder="Input Contact Number" maxlength="10">
                        </div>
                        <span class="help-block contact_no" style="color: red;" id="errorMessage"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">City:</label>
                    <div class="col-sm-7">
                        <select class="form-control city_id" id="city" name="city_id">
                            @foreach(App\City::all() as $city)
                                <option value="{{$city->id}}">{{$city->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Address:</label>
                    <div class="col-sm-8">
                        <textarea name="address" placeholder="Input Address" class="form-control" rows="4"></textarea>
                        <span class="help-block address" style="color: red" id="errorMessage"></span>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <table id="dataTable" class="form table table-bordered table-responsive table-striped no-header compact">
                    <thead>
                    <tr>
                        <th><span style="font-size: 15px">Contact Person Details</span></th>
                    </tr>
                    </thead>
                    <tbody id="contact_person_container">
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="">Name</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" name="contactDetail[0][contactname]"
                                               class="form-control input-sm" id="name">
                                        <span class="help-block contactname-0" style="color: red" id="contactname0"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="">Email</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <input type="text" name="contactDetail[0][email]" id="email"
                                               class="form-control input-sm">
                                        <span class="help-block email" style="color: red" id="email0"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="">Mobile</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <input type="text" name="contactDetail[0][mobile]" class="form-control input-sm"
                                               id="mobile" maxlength="10">
                                        <span class="help-block mobile" style="color: red" id="mobile0"></span>
                                    </div>
                                    <div class="col-sm-2">
                                        <button class="btn btn-default btn-sm pull-right" style="padding: 4px 8px" type="button" onclick="addRow('dataTable')">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="box-footer">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="btn-group pull-right">
                    <button type="submit" class="btn btn-info pull-right" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit</button>
                </div>
                <div class="btn-group pull-left">
                    <button type="reset" class="btn btn-warning">Reset</button>
                </div>
            </div>
        </div>
    </form>
    <!-- /.box-body -->
</div>

<div class="modal fade" id="modal-default" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Branch Details</h4>
            </div>
            <div class="modal-body" id="modal-default-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary pull-right" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<style>
    hr {
        border: 0;
        width: 96%;
        height: 1px;
    }
</style>
<script>


</script>