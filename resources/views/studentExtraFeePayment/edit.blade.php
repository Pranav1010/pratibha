
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Edit</h3>

    </div>
    <form action="" method="POST" class="form-horizontal">

        {{ csrf_field() }}
        {{method_field('PUT')}}

         <input type="hidden" value="{{ $branchExtraFee->id }}" id="branchExtraFeeId" name="branchExtraFeeId">

        @include('branchExtraFee.form')

    </form>
    <!-- /.box-body -->
</div>
<style>
    hr {
        border: 0;
        width: 96%;
        height: 1px;
    }
</style>
<script>


</script>