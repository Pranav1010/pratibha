<div class="form-group">
    <label class="col-sm-3 control-label">Select Items:</label>
    <div class="col-sm-8">
        <select class="form-control" id="items" name="items[]" multiple>
            @if(isset($items))
                @foreach($items as $item)
                    <option value="{{ $item->id }}" data-amount="{{ $item->amount }}" >{{ $item->name }}</option>
                @endforeach
            @endif
        </select>
        <span class="help-block items" style="color: red" id="errorMessage"></span>
    </div>
</div>