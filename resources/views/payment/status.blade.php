<div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title">School Payment Information</h3>
  </div>

  <table class="table table-condensed text-center">
    <tbody>
        <tr>
          <th>School Name:</th>
          <th>{{ $school->name }}</th>
        </tr>
        <tr>
          <th>Total Amount:</th>
          <th>{{ number_format($totalAmount + $discount,2) }}</th>
        </tr>
        <tr>
          <th>Discount:</th>
          <th >{{ number_format($discount,2) }}</th>
        </tr>
        <tr>
          <th>Paid Amount:</th>
          <th>{{ number_format($totalPaidAmount,2) }}</th>
        </tr>
        <tr>
          <th>Pending Amount:</th>
          <th>{{ number_format($totalPendingAmount,2) }}</th>
        </tr>      
    </tbody>
  </table>

  <div class="box-header">
    <h3 class="box-title">School Payment History</h3>
  </div>
  <table class="table table-bordered text-center">
    <tbody>
        <tr>
            <th>Amount</th>
            <th>Date</th>
            <th>By</th>
        </tr>
      @forelse ($schoolPaymentHistory as $history)
        <tr>
            <td>{{ number_format($history->paid_amount,2) }}</td>
            <td>{{ Carbon\Carbon::parse($history->date)->format('d-m-Y') }}</td>
            <td>
                  
                  @if($history->payment_mode == "Cheque")

                      <span class="btn btn-link" title="Cheque Details" data-toggle="popover" data-trigger="hover" data-content="<b>Bank Name: {{ App\Bank::select('name')->find($history->bank_id)->name }} </b> <br/> <b>Cheque No: {{ $history->cheque_no}}</b>">{{ $history->payment_mode }}</span>
                
                  @else
                  
                    {{ $history->payment_mode }}
                 
                  @endif
            </td>
        </tr>
      @empty
        <tr>
          <td colspan="3">Records Not Available</td>
        </tr>
      @endforelse
    </tbody>
</table>
    
</div>