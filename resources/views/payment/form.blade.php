<div class="box-body">
    <input type="hidden" name="form" value="payment" id="form">
    <div class="fields-group">
        <div class="form-group">
            <label class="col-sm-3 control-label">School Name:</label>
            <div class="col-sm-8">
                    <input type="text" class="form-control " name="name" value="{{ $school->name or null }}" readonly>
                    <input type="hidden" name="schoolId" value="{{ $school->id }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Payment Date:</label>
            <div class="col-sm-8">
                    <input type="text" class="form-control datepicker" name="date" placeholder="Payment Date">
                <span class="help-block date" style="color: red;" id="errorMessage"></span>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-3 control-label">Total Amount:</label>
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-8"> 
                        <input type="text" class="form-control"  value="{{ number_format($payments->sum('amount'),2) }}" readonly>
                    </div>
                    <div class="col-sm-4">
                         <button type="button" class="btn btn-primary"  data-toggle="modal" data-target="#pendingViewModal" ><i class="fa fa-eye" aria-hidden="true"></i></button>
                    </div> 
                </div>       
            </div>
        </div>
        <input type="hidden" name="totalAmount" id="totalAmount" value="{{ $payments->sum('amount') }}">
        
        <div class="form-group">
            <label class="col-sm-3 control-label">Discount:</label>
            <div class="col-sm-8">
                    <input type="text" class="form-control" name="discount" id="discount" placeholder="Enter Discount">
                <span class="help-block discount" style="color: red;" id="errorMessage"></span>
            </div>
        </div>
        <div class="form-group" id="payableAmountDiv">
            <label class="col-sm-3 control-label">Payable Amount:</label>
            <div class="col-sm-8">
                    <input type="text" class="form-control" name="payableAmount"  value="" id="payableAmount" readonly>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Paid Amount:</label>
            <div class="col-sm-8">
                    <input type="text" class="form-control" name="paid_amount" placeholder="Enter Paid Amount">
                    <span class="help-block paid_amount" style="color: red;" id="errorMessage"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Payment Mode:</label>
            <div class="col-sm-8">
                <div class="radio">
                  <label><input type="radio" name="payment_mode" value="Cash"><big> Cash </big></label>
                  <label><input type="radio" name="payment_mode" value="Cheque"><big> Cheque </big></label>
                </div>
                <span class="help-block payment_mode" style="color: red;" id="errorMessage"></span>
            </div>
        </div>
        <div id="cheque">
            <div class="form-group">
                <label class="col-sm-3 control-label">Bank Name:</label>
                <div class="col-sm-8">
                    <select class="form-control" id="bank_name" name="bank_name">
                        <option value="">Select Bank</option>
                        @foreach(App\Bank::all() as $bank)
                            <option value="{{ $bank->id }}">{{ $bank->name }}</option>
                        @endforeach
                           
                    </select>
                    <span class="help-block bank_name" style="color: red" id="errorMessage"></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Cheque No:</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="cheque_no" placeholder="Enter Cheque No">
                    <span class="help-block cheque_no" style="color: red;" id="errorMessage"></span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Payment Remark:</label>
            <div class="col-sm-8">
                <textarea name="remark" placeholder="Enter Payment Remark" class="form-control" rows="4"></textarea>
                <span class="help-block remark" style="color: red" id="errorMessage"></span>
            </div>
        </div>
    </div>
</div>

<div class="box-footer">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="btn-group pull-right">
            <button type="submit" class="btn btn-info pull-right" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit</button>
        </div>
        <div class="btn-group pull-left">
            <button type="reset" class="btn btn-warning">Reset</button>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="pendingViewModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">School Pending Payment</h4>
      </div>
      <div class="modal-body">
         <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Year</th>
                    <th>Due Amount</th>
                </tr>
            </thead>
            <tbody>
                @foreach($payments as $payment)
                <tr>
                    <td>{{ $payment->year }}</td>
                    <td>{{ number_format($payment->amount,2) }}</td>
                </tr>
                @endforeach
                <tr>
                    <th><b>Total Amount</b></th>
                    <th><b>{{ number_format($payments->sum('amount'),2) }}</b></th>
                </tr>
            </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
    $().ready(function(){
        $('input[type="radio"]').iCheck({
            radioClass: 'iradio_square-blue'
        });

        $('.datepicker').datepicker({
            format: "dd/mm/yyyy"
        });
    });
</script>