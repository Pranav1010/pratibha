<div class="col-md-2">
    <div class="small-box bg-green-gradient" style="box-shadow: 3px 4px 10px #888;">
        <div class="inner">
            <h3>{{ number_format($totalPaidAmount,2) }}</h3>
            <p>Total Paid Amount</p>
        </div>
        <div class="icon">
            <i class="fa fa-arrow-down"></i>
        </div>

    </div>
</div>
<div class="col-md-2">
    <div class="small-box bg-purple-gradient" style="box-shadow: 3px 4px 10px #888;">
        <div class="inner">
            <h3>{{ number_format($totalPendingAmount,2) }}</h3>
            <p>Total Pending Amount</p>
        </div>
        <div class="icon">
            <i class="fa fa-rupee"></i>
        </div>

    </div>
</div>
<div class="col-md-2">
    <div class="small-box bg-aqua-gradient" style="box-shadow: 3px 4px 10px #888;">
        <div class="inner">
            <h3>{{ number_format($totalDiscountAmount,2) }}</h3>
            <p>Total Discount Amount</p>
        </div>
        <div class="icon">
            <i class="fa fa-percent"></i>
        </div>

    </div>
</div>
<div class="col-md-2">
    <div class="small-box bg-red-gradient" style="box-shadow: 3px 4px 10px #888;">
        <div class="inner">
            <h3>{{ number_format($totalRefundAmount,2) }}</h3>
            <p>Total Refund Amount</p>
        </div>
        <div class="icon">
            <i class="fa fa-rupee"></i>
        </div>

    </div>
</div>