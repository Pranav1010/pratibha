
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Create</h3>

    </div>
    <form action="" method="POST" class="form-horizontal">

        {{ csrf_field() }}

        @include('branchExtraFee.form')

    </form>
    <!-- /.box-body -->
</div>
<style>
    hr {
        border: 0;
        width: 96%;
        height: 1px;
    }
</style>
<script>


</script>