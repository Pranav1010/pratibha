<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Upload Logo</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form action="/auth/logoupload" method="post" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data" id="logoUpload">
        <div class="box-body text-center">
            <div class="row">
                <div class="col col-sm-4">Currently Used Logo:</div>
                <div class="col col-sm-8"><img src="{{ url('img/pratibha-logo.png') }}" style="width: 100%;"></div>
            </div>
            <br/>
            <div class="row">
                <div class="col col-sm-4">Upload New Logo:</div>
                <div class="col col-sm-8">
                    <input type="file" name="logo_file">
                </div>
            </div>
            <div class="row">
                <div class="col col-sm-12">
                    <span class="help-block logo_file" style="color: red" id="errorMessage"></span>
                </div>
            </div>
            <div class="row">
                <div class="col col-sm-12">
                    <span style="color: blue">Note: Image Size 200x50 For Best Result.</span>
                </div>
            </div>
        </div>
    {{ csrf_field() }}
        <!-- /.box-body -->
        <div class="box-footer">
            <div class="col-md-2">

            </div>
            <div class="col-md-8">
                <div class="btn-group pull-right">
                    <button type="submit" class="btn btn-info pull-right" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Save">Save</button>
                </div>
                <div class="btn-group pull-left">
                    <button type="reset" class="btn btn-warning">Reset</button>
                </div>
            </div>
        </div>
        <!-- /.box-footer -->
    </form>
</div>