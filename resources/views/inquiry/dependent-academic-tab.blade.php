<div class="box-body">
    <table id="dependentAcademicDataTable" class="form table  compact">
        <thead>
        <tr>
            <th>Exam Passed</th>
            <th>Institute / University</th>
            <th>Year Of Passing</th>
            <th>Board</th>
            <th>Result</th>
            <th>Backlogs</th>
            <th></th>
        </tr>
        </thead>
        <tbody id="dependent_academic_container">
        <!-- row1 -->
        <tr>
            <td>
                <input type="text" placeholder="SSC" name="dependent_academic[0][exam_passed]"
                       class="form-control input-sm" id="exam_passed">
            </td>
            <td>
                <input type="text" name="dependent_academic[0][institute]"
                       class="form-control input-sm"
                       id="dependent_institute">
            </td>
            <td>
                <input type="text" name="dependent_academic[0][year]" class="form-control input-sm"
                       id="dependent_year">
            </td>
            <td>
                <input type="text" name="dependent_academic[0][board]" class="form-control input-sm"
                       id="dependent_board">
            </td>
            <td>
                <input type="text" name="dependent_academic[0][result]" class="form-control input-sm"
                       id="dependent_result">
            </td>
            <td>
                <input type="text" name="dependent_academic[0][backlog]" class="form-control input-sm"
                       id="dependent_result">
            </td>
        </tr>
        <!-- row2 -->
        <tr>
            <td>
                <input type="text" placeholder="HSC" name="dependent_academic[1][exam_passed]"
                       class="form-control input-sm" id="dependent_exam_passed">
            </td>
            <td>
                <input type="text" name="dependent_academic[1][institute]"
                       class="form-control input-sm"
                       id="dependent_institute">
            </td>
            <td>
                <input type="text" name="dependent_academic[1][year]" class="form-control input-sm"
                       id="dependent_year">
            </td>
            <td>
                <input type="text" name="dependent_academic[1][board]" class="form-control input-sm"
                       id="dependent_board">
            </td>
            <td>
                <input type="text" name="dependent_academic[1][result]" class="form-control input-sm"
                       id="dependent_dependent_result">
            </td>
            <td>
                <input type="text" name="dependent_academic[1][backlog]" class="form-control input-sm"
                       id="dependent_result">
            </td>
        </tr>
        <!-- row3 -->
        <tr>
            <td>
                <input type="text" placeholder="Diploma" name="dependent_academic[2][exam_passed]"
                       class="form-control input-sm" id="dependent_exam_passed">
            </td>
            <td>
                <input type="text" name="dependent_academic[2][institute]"
                       class="form-control input-sm"
                       id="dependent_institute">
            </td>
            <td>
                <input type="text" name="dependent_academic[2][year]" class="form-control input-sm"
                       id="dependent_year">
            </td>
            <td>
                <input type="text" name="dependent_academic[2][board]" class="form-control input-sm"
                       id="dependent_board">
            </td>
            <td>
                <input type="text" name="dependent_academic[2][result]" class="form-control input-sm"
                       id="dependent_result">
            </td>
            <td>
                <input type="text" name="dependent_academic[2][backlog]" class="form-control input-sm"
                       id="dependent_result">
            </td>
        </tr>
        <!-- row4 -->
        <tr>
            <td>
                <input type="text" placeholder="Graduation" name="dependent_academic[3][exam_passed]dependent_academic[0]["
                       class="form-control input-sm" id="dependent_exam_passed">
            </td>
            <td>
                <input type="text" name="dependent_academic[3][institute]dependent_academic[0]["
                       class="form-control input-sm"
                       id="dependent_institute">
            </td>
            <td>
                <input type="text" name="dependent_academic[3][year]dependent_academic[0][" class="form-control input-sm"
                       id="dependent_year">
            </td>
            <td>
                <input type="text" name="dependent_academic[3][board]dependent_academic[0][" class="form-control input-sm"
                       id="dependent_board">
            </td>
            <td>
                <input type="text" name="dependent_academic[3][result]dependent_academic[0][" class="form-control input-sm"
                       id="dependent_result">
            </td>
            <td>
                <input type="text" name="dependent_academic[3][backlog]" class="form-control input-sm"
                       id="dependent_result">
            </td>
        </tr>
        <!-- row5-->
        <tr>
            <td>
                <input type="text" placeholder="Master" name="dependent_academic[4][exam_passed]"
                       class="form-control input-sm" id="dependent_exam_passed">
            </td>
            <td>
                <input type="text" name="dependent_academic[4][institute]"
                       class="form-control input-sm"
                       id="dependent_institute">
            </td>
            <td>
                <input type="text" name="dependent_academic[4][year]" class="form-control input-sm"
                       id="dependent_year">
            </td>
            <td>
                <input type="text" name="dependent_academic[4][board]" class="form-control input-sm"
                       id="dependent_board">
            </td>
            <td>
                <input type="text" name="dependent_academic[4][result]" class="form-control input-sm"
                       id="dependent_result">
            </td>
            <td>
                <input type="text" name="dependent_academic[4][backlog]" class="form-control input-sm"
                       id="dependent_result">
            </td>
        </tr>
        <!-- row6 -->
        <tr>
            <td>
                <input type="text" placeholder="ITI" name="dependent_academic[5][exam_passed]"
                       class="form-control input-sm" id="dependent_exam_passed">
            </td>
            <td>
                <input type="text" name="dependent_academic[5][institute]"
                       class="form-control input-sm"
                       id="dependent_institute">
            </td>
            <td>
                <input type="text" name="dependent_academic[5][year]" class="form-control input-sm"
                       id="dependent_year">
            </td>
            <td>
                <input type="text" name="dependent_academic[5][board]" class="form-control input-sm"
                       id="dependent_board">
            </td>
            <td>
                <input type="text" name="dependent_academic[5][result]" class="form-control input-sm"
                       id="dependent_result">
            </td>
            <td>
                <input type="text" name="dependent_academic[5][backlog]" class="form-control input-sm"
                       id="dependent_result">
            </td>
        </tr>
        <!-- row7 -->
        <tr>
            <td>
                <input type="text" placeholder="Other" name="dependent_academic[6][exam_passed]"
                       class="form-control input-sm" id="exam_passed">
            </td>
            <td>
                <input type="text" name="dependent_academic[6][institute]"
                       class="form-control input-sm"
                       id="dependent_institute">
            </td>
            <td>
                <input type="text" name="dependent_academic[6][year]" class="form-control input-sm"
                       id=dependent_"year">
            </td>
            <td>
                <input type="text" name="dependent_academic[6][board]" class="form-control input-sm"
                       id="dependent_board">
            </td>
            <td>
                <input type="text" name="dependent_academic[6][result]" class="form-control input-sm"
                       id="dependent_result">
            </td>
            <td>
                <input type="text" name="dependent_academic[6][backlog]" class="form-control input-sm"
                       id="dependent_result">
            </td>
            <td>
                <button type="button" class="btn btn-info btn-sm"
                        onclick="addRow('dependentAcademicDataTable')">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </button>
            </td>
        </tbody>
    </table>
    <div class="col-md-12 form-group" align="right">
        <button type="button" class="btn btn-primary inquiryDetailSubmit" id="submit-dependent-academic"
                style="margin: 20px 10px 20px 0">Save
        </button>
        <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
    </div>
</div>
