<div class="box-body dependent-detail">
    <div class="row">
        <div class="col-md-6">

            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control" name="dependentName">
                        <span class="help-block name" style="color: red" id="name0"></span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Birth Date</label>
                        <div class="input-group date">
                            <input type="text" class="form-control datepicker" id="dependentDate" name="dependentBirthDate">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Mobile</label>
                        <input type="text" class="form-control" maxlength="10" name="dependentMobile">
                        <span class="help-block mobile" style="color: red" id="mobile0"></span>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="dependentEmail">
                        <span class="help-block email" style="color: red" id="email0"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Children</label>
                        <input type="number" name="dependentChildren" placeholder="Children" class="form-control input-sm" min=0 id="children">
                    </div>
                </div>
            </div>
            <div class="row" id="child-info-title" style="display: none">
                <div class="col-md-12">
                    <div class="form-group">
                        <h3 class="box-title">Children Details</h3>
                    </div>
                </div>
            </div>
            <div class="child-info">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group pull-right">
                            <button type="button" class="btn btn-primary inquiryDetailSubmit" id="submit-dependent-detail" style="margin: 20px 10px 20px 0">Save</button>
                            <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="nav-tabs-custom" id="dependentTabs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#dependent_tab1" data-toggle="tab">Academic</a></li>
                    <li><a href="#dependent_tab2" data-toggle="tab" id="dependentExamTab">Exam</a></li>
                    <li><a href="#dependent_tab3" data-toggle="tab">Work Experience</a></li>
                    <li><a href="#dependent_tab4" data-toggle="tab" id="dependentTravelTab">Travel History</a></li>
                </ul>
                <div class="tab-content">
                    {{--Academic Tab--}}
                    <div id="dependent_tab1" class="tab-pane active">
                        @include('inquiry.dependent-academic-tab')
                    </div>
                    {{--Exam Tab--}}
                    <div id="dependent_tab2" class="tab-pane">
                        @include('inquiry.dependent-exam-tab')
                    </div>
                    {{--Work Experience Tab--}}
                    <div id="dependent_tab3" class="tab-pane">
                        @include('inquiry.dependent-work-experience-tab')
                    </div>
                    {{--Travel History Tab--}}
                    <div id="dependent_tab4" class="tab-pane">
                        @include('inquiry.dependent-travel-history-tab')
                    </div>
                </div>
            </div>
            {{--<div class="noDataDiv" style="background: #f7f7f7; height: 50%;">Save dependent details first.</div>--}}
        </div>
    </div>
    <!-- ************************************************************************************************ -->

    
    <!-- **************************************************************************************************** -->
</div>


