<div class="box-body">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label>Country</label>
                <input type="text" class="form-control" name="country" id="dependent_Country">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Visa Type</label>
                <select name="dependentTravelVisaType" class="form-control visa-type1" id="dependent_program">
                    <option value=""></option>
                    @foreach(config("app.visa_type") as $key => $visaTypes)
                        <option value="{{$key}}">{{$visaTypes}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Status</label><br>
                <select name="dependentTravelVisaStatus" class="form-control visa-status1" id="dependent_status">
                    <option value=""></option>
                    <option>Approved</option>
                    <option>Declined</option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Year</label>
                <input type="text" class="form-control" name="year" id="dependent_year2">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label>Duration</label>
                <input type="text" class="form-control" name="dependentYearDuration" id="dependent_yearDuration">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Remarks</label>
                <textarea name="dependent_remarks" class="form-control" rows="1" id="dependent_remark1"></textarea>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group pull-right" style="margin-top: 20px;">
                <button type="button" class="btn btn-info" id="dependent_addTravel">Add</button>
            </div>
        </div>
    </div>

    <!-- ************************* data table ************************************ -->
    <div class="dependent-travel-table-box" style="display:none;">
        <table id="travel" class="table table-striped table-bordered"
               cellspacing="0"
               width="100%">
            <thead>
            <tr>
                <th>Country</th>
                <th>Program</th>
                <th>Status</th>
                <th>Year</th>
                <th>Duration</th>
                <th>Remark</th>
                <th></th>
            </tr>
            </thead>
            <tbody id="travelData">
            <!-- tr -->
            </tbody>
        </table>
    </div>
    <!-- *************************end data table ********************************* -->
    <div class="col-md-12 form-group" align="right">
        <button type="button" class="btn btn-primary inquiryDetailSubmit" id="submit-travelHistory"
                style="margin: 20px 10px 20px 0">Save
        </button>
        <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
    </div>
</div>
