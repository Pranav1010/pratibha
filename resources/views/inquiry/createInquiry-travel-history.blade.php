<div class="box-body">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label>Country</label>
                <input type="text" name="travelCountry" class="form-control" id="Country" autofocus="autofocus">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Visa Type</label>
                <select name="travelVisaType" class="form-control visa-type" id="program">
                    <option value=""></option>
                    @foreach(config("app.visa_type") as $key => $visaTypes)
                        <option value="{{$key}}">{{$visaTypes}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Visa Status</label><br>
                <select name="travelVisaStatus" class="form-control visa-status" id="status">
                    <option value=""></option>
                    <option>Approved</option>
                    <option>Declined</option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Application Year</label>
                <input type="number" name="travelApplicationYear" class="form-control" id="appYear">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Duration</label>
                        <input type="number" class="form-control" placeholder="Years" name="travelYearDuration" id="year2">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" style="margin-top: 25px">
                        <input type="number" class="form-control" placeholder="Months" name="travelMonthDuration" id="month2">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Remarks</label>
                <textarea name="travelRemarks" class="form-control" rows="1" id="remark1"></textarea>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group pull-right" style="margin-top: 20px;">
                <button type="button" class="btn btn-info" id="addTravelBox">Add</button>
            </div>
        </div>
    </div>
    <!-- ************************* data table ************************************ -->
    <div class="travel-box" style="display:none;">
        <table id="travel" class="table table-striped table-bordered" cellspacing="0"
               width="100%">
            <thead>
            <tr>
                <th>Country</th>
                <th>Program</th>
                <th>Status</th>
                <th>Year</th>
                <th>Duration</th>
                <th>Remark</th>
                <th></th>
            </tr>
            </thead>
            <tbody id="travelHistory">
            <!-- tr -->
            </tbody>
        </table>
    </div>
    <!-- *************************end data table ********************************* -->
    <div class="col-md-12 form-group" align="right">
        <button type="button" class="btn btn-primary inquiryDetailSubmit" id="submit-travel-history"
                style="margin: 20px 10px 20px 0">Save
        </button>
        <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
    </div>
</div>
