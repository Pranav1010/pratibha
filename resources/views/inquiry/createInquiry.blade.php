<div class="box-body personal-info">
    @include('inquiry.createInquiry-personal-info')
</div>
<!-- end personal info -->
{{-- <div class="tabs" id="mainTabs" style="display: none;" > --}}
<div class="nav-tabs-custom" id="mainTabs" style="display: none">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab">Academic</a></li>
        <li><a href="#tab_2" data-toggle="tab">Exam</a></li>
        <li><a href="#tab_3" data-toggle="tab">Work Experience</a></li>
        <li><a href="#tab_4" data-toggle="tab">Dependent Details</a></li>
        <li><a href="#tab_5" data-toggle="tab">Travel History</a></li>
        <li><a href="#tab_6" data-toggle="tab">Counselling Summary</a></li>
    </ul>
    <div class="tab-content">
        <!-- Academic Tab-->
        <div class="tab-pane active" id="tab_1">
            @include('inquiry.createInquiry-academic-tab')
        </div>
        {{--Exam Tab--}}
        <div class="tab-pane" id="tab_2">
            @include('inquiry.createInquiry-exam-tab')
        </div>
        {{--Work Experience Tab--}}
        <div class="tab-pane" id="tab_3">
            @include('inquiry.createInquiry-work-experience-tab')
        </div>
        {{--Dependent Details Tab--}}
        <div class="tab-pane" id="tab_4">
            @include('inquiry.dependent-detail')
        </div>
        {{--Travel History Tab--}}
        <div class="tab-pane" id="tab_5">
            @include('inquiry.createInquiry-travel-history')
        </div>

        <div class="tab-pane" id="tab_6">
            @include('inquiry.createInquiry-counselling-summary-tab')
        </div>
    </div>
</div>