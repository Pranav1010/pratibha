<div class="box-body">
    <table id="workTable" class="form table  compact">
        <thead>
        <tr>
            <th>Company Name</th>
            <th>Designation</th>
            <th width="20%">Duration</th>
            <th>Location</th>
            <th>Remark</th>
            <th width="5%"></th>
        </tr>
        </thead>
        <tbody id="work_experience_container">
        <!-- row1 -->
        <tr>
            <td>
                <input type="text" name="work[0][company]" class="form-control input-sm"
                       id="company">
            </td>
            <td>
                <input type="text" name="work[0][designation]" class="form-control input-sm"
                       id="designation">
            </td>
            <td>
                <div class="row">
                    <div class="col-md-6">
                        <input type="number" name="work[0][durationYears]" placeholder="Years" class="form-control input-sm"
                               id="duration">
                    </div>
                    <div class="col-md-6">
                        <input type="number" name="work[0][durationMonths]" placeholder="Months" class="form-control input-sm"
                               id="duration">
                    </div>
                </div>
            </td>
            <td>
                <input type="text" name="work[0][location]" class="form-control input-sm"
                       id="location">
            </td>
            <td>
                <input type="text" name="work[0][remark]" class="form-control input-sm"
                       id="remark">
            </td>
        </tr>
        <!-- row2 -->
        <tr>
            <td>
                <input type="text" name="work[1][company]" class="form-control input-sm"
                       id="company">
            </td>
            <td>
                <input type="text" name="work[1][designation]" class="form-control input-sm"
                       id="designation">
            </td>
            <td>
                <div class="row">
                    <div class="col-md-6">
                        <input type="number" name="work[1][durationYears]" placeholder="Years" class="form-control input-sm"
                               id="duration">
                    </div>
                    <div class="col-md-6">
                        <input type="number" name="work[1][durationMonths]" placeholder="Months" class="form-control input-sm"
                               id="duration">
                    </div>
                </div>
            </td>
            <td>
                <input type="text" name="work[1][location]" class="form-control input-sm"
                       id="location">
            </td>
            <td>
                <input type="text" name="work[1][remark]" class="form-control input-sm"
                       id="remark">
            </td>
        </tr>
        <!-- row3 -->
        <tr>
            <td>
                <input type="text" name="work[2][company]" class="form-control input-sm"
                       id="company">
            </td>
            <td>
                <input type="text" name="work[2][designation]" class="form-control input-sm"
                       id="designation">
            </td>
            <td>
                <div class="row">
                    <div class="col-md-6">
                        <input type="number" name="work[2][durationYears]" placeholder="Years" class="form-control input-sm"
                               id="duration">
                    </div>
                    <div class="col-md-6">
                        <input type="number" name="work[2][durationMonths]" placeholder="Months" class="form-control input-sm"
                               id="duration">
                    </div>
                </div>
            </td>
            <td>
                <input type="text" name="work[2][location]" class="form-control input-sm"
                       id="location">
            </td>
            <td>
                <input type="text" name="work[2][remark]" class="form-control input-sm"
                       id="remark">
            </td>
            <td>
                <button type="button" class="btn btn-info btn-sm"
                        onclick="addRowWork('workTable')">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                </button>
            </td>
        </tr>
        </tbody>
    </table>
    <div class="col-md-12 form-group" align="right">
        <button type="button" class="btn btn-primary inquiryDetailSubmit" id="submit-workExperience" style="margin-right: 10px">Save</button>
        <a href="{{url('/inquiries')}}" class="btn btn-primary">Cancel</a>
    </div>
</div>
