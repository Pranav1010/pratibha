<table class="table no-header table-bordered">
    <tbody>
    <tr>
        <td width="20%">
            <b>Inquiry Name</b>
        </td>
        <td>
            {{$inquiry->name or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Mobile No.</b>
        </td>
        <td>
            {{$inquiry->mobile or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Email</b>
        </td>
        <td>
            {{$inquiry->email or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Address</b>
        </td>
        <td>
            {{$inquiry->address or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>City</b>
        </td>
        <td>
            {{$inquiry->city->name or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Source</b>
        </td>
        <td>
            {{$inquiry->source->name or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Reference Name</b>
        </td>
        <td>
            {{$inquiry->friend_name or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Tag Name</b>
        </td>
        <td>
            {{$inquiry->tag or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Remarks</b>
        </td>
        <td>
            {{$inquiry->remarks or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Assigned To</b>
        </td>
        <td>
            {{$inquiry->user->name or "-"}}
        </td>
    </tr>
    </tbody>
</table>
<table class="table no-header table-bordered">
    <caption><h4>Activity Details:</h4></caption>
    <thead>
    <th>Name</th>
    <th>Fee</th>
    </thead>
    <tbody>

    @foreach($courses as $key=>$value)
        <tr>
            <td>
                {{$value->name or "-"}}
            </td>
            <td>
                {{$value->fees or "-"}}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>