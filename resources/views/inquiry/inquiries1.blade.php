@extends('adminlte::page')

@section('title', 'Inquiries')

@section('content_header')
    <h1>Inquiries</h1>
@stop


@section('content')

    <div class="box">
        <div align="right">
            <a href="{{url('/inquiries/create')}}" class="btn btn-primary" style="margin: 10px">Add New Inquiry</a>
        </div>
        <div class="box-body">
            <table id="example" class="table table-bordered table-striped dt-responsive">
                <thead>
                <tr>
                    <th>Sr. No.</th>
                    <th>Name</th>
                    <th>Mobile</th>
                    <th>City</th>
                    <th>Area</th>
                    <th>Pincode</th>
                    <th>Country</th>
                    <th>Course</th>
                    <th>Assigned User</th>
                    <th>Status</th>
                    <th>Total Follow Ups</th>
                    <th>Days Past</th>
                    <th width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1?>
                @foreach($inquiries as $inquiry)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{$inquiry->name}}</td>
                        <td>{{$inquiry->mobile}}</td>
                        <td>{{$inquiry->city}}</td>
                        <td>{{$inquiry->area}}</td>
                        <td>{{$inquiry->pincode}}</td>
                        <td>{{$inquiry->country}}</td>
                        <td>{{$inquiry->course->name or ""}}</td>
                        <td>{{$inquiry->getUserNames($inquiry->id, $userId)}}</td>
                        <td>
                            <label for="status"
                                   class="label label-{{config('app.inquiry_status.'.$inquiry->status.'.class')}}">
                                {{config('app.inquiry_status.'.$inquiry->status.'.name')}}
                            </label>
                        </td>
                        <td>{{$inquiry->getNumberOfFollowUps($inquiry->id)}}</td>
                        <td>{{$inquiry->getDaysFromStartDateOfInquiry($inquiry->created_at)}}</td>
                        <td>
                            <div class="btn-group" role="group" aria-label="...">

                                <a href="{{url('/inquiries/'.$inquiry->id.'/followUp?inquiry_wise_user_id='.$inquiry->inquiry_wise_user_id)}}" class="btn btn-success btn-sm"
                                   data-toggle="tooltip" title="View"><i
                                            class="glyphicon glyphicon-eye-open"></i></a>


                                <a href="{{url('/inquiries/'.$inquiry->id.'/edit')}}"
                                   class="btn btn-primary btn-sm"
                                   data-toggle="tooltip" title="Edit"><i
                                            class="glyphicon glyphicon-edit"></i></a>

                                <a href="#" class="btn btn-danger btn-sm"
                                   onclick="event.preventDefault(); $('#form-{{$inquiry->id}}').submit();"
                                   data-toggle="tooltip" title="Delete"><i
                                            class="glyphicon glyphicon-trash"></i></a>

                				@if(!empty($inquiry->reason) || !empty($inquiry->newReason))
                				    <button type="button" class="btn bg-navy btn-flat btn-sm" title="Click me to know reason" data-toggle="modal" data-target="#myModal-{{$inquiry->id}}">
                				      <i class="fa fa-info-circle"></i>
                				    </button>
                                @endif

                                <form action="{{url('/inquiries/'.$inquiry->id)}}"
                                      onsubmit="return confirm('Are you sure you want to delete this item?'); "
                                      id="form-{{$inquiry->id}}" method="post" style="display: hidden">
                                    {{csrf_field()}}
                                    {{method_field("DELETE")}}
                                </form>

                				@if(!empty($inquiry->newReason))
                					<!-- Modal -->
                					<div class="modal fade" id="myModal-{{$inquiry->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                					  <div class="modal-dialog" role="document">
                					    <div class="modal-content">
                					      <div class="modal-header">
                					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                					        <h4 class="modal-title" id="myModalLabel"><b>Reason</b></h4>
                					      </div>
                					      <div class="modal-body">
                                            <table class="tg">
                                              <tr>
                                                <th class="tg-qb4h" style="width: 20%"><b>Reporter : </b></th>
                                                <th class="tg-qb4h">{{$inquiry->assignor->name or ""}}</th>
                                              </tr>
                                              <tr>
                                                <td class="tg-3we0"><b>Assignee(s) :</b></td>
                                                <td class="tg-3we0">{{$inquiry->getUserNames($inquiry->id, $userId)}}</td>
                                              </tr>
                                              <tr>
                                                <td class="tg-qb4h"><b>Reason :</b></td>
                                                <td class="tg-qb4h">{{$inquiry->newReason}}</td>
                                              </tr>
                                            </table>
                					      </div>
                					      <div class="modal-footer">
                					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                					      </div>
                					    </div>
                					  </div>
                					</div>
                				@endif

                				@if(!empty($inquiry->reason))
                					<!-- Modal -->
                					<div class="modal fade" id="myModal-{{$inquiry->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                					  <div class="modal-dialog" role="document">
                					    <div class="modal-content">
                					      <div class="modal-header">
                					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                					        <h4 class="modal-title" id="myModalLabel"><b>Reason</b></h4>
                					      </div>
                					      <div class="modal-body">
                					      	{!!html_entity_decode($inquiry->reason)!!}
                					      </div>
                					      <div class="modal-footer">
                					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                					      </div>
                					    </div>
                					  </div>
                					</div>
                				@endif

                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop

@section("adminlte_js")
    <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#example').DataTable({
                responsive: true,
                columns:[
                    { responsivePriority: 1 },
                    { responsivePriority: 1 },
                    { responsivePriority: 2 },
                    { responsivePriority: 2 },
                    { responsivePriority: 2 },
                    { responsivePriority: 3 },
                    { responsivePriority: 3 },
                    { responsivePriority: 2 },
                    { responsivePriority: 2 },
                    { responsivePriority: 2 },
                    { responsivePriority: 2 },
                    { responsivePriority: 2 },
                    { responsivePriority: 1 }
                ]
            });
            $('[data-toggle="tooltip"]').tooltip({html: true});

            $("#inquiry-menu").css("background", "white");
            $("#inquiry-menu a").css("color", "#265671");
        });
    </script>
@endsection