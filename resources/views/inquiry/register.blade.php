<div class="container-fluid" style="padding-top: 10px;">


    <form action="{{ ($student->username == '') ? '/students' : '/students/'.$student->user_id }}"
          onsubmit="registerStudent(this, event)" method="{{ ($student->username == '') ? 'POST' : 'PUT' }}"
          class="form-horizontal">

        {{ csrf_field() }}

        <div class="form-group">
            <label class="col-sm-2 control-label"><img src="{{ url('img/boy.png') }}" width="25px"></label>
            <div class="col-sm-8">
                <h4> Student's Information </h4>
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Name</label>

            <div class="col-sm-8">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                    <input type="text" id="name" name="name" value=" {{ $student->name }}" class="form-control name"
                           placeholder="Input Name">
                </div>
                <div class="col-sm-12 error" id="error_name">

                </div>
            </div>


        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Select Gender</label>
            <div class="col-sm-8">
                <select placeholder="Select Gender" class="form-control gender" name="gender">
                    <option value="">Select Gender</option>
                    <option value="M">Male</option>
                    <option value="F">Female</option>
                </select>
                <div class="col-sm-12 error" style="color: red;" id="error_gender">

                </div>
            </div>
        </div>
        @if(isRole('administrator') || isRole('super-administrator'))
            <div class="form-group">
                <label class="col-sm-2 control-label">Branch Name</label>

                <div class="col-sm-8">
                    <select placeholder="Select Branch" class="form-control branch_id" name="branch_name"
                            id="branch_name">
                        <option></option>
                        @foreach(App\Branch::all() as $branch)
                            <option value="{{ $branch->id }}"> {{ $branch->name }} </option>
                        @endforeach
                    </select>
                </div>
            </div>
        @endif

        @if(isset($branch))
            <input type="hidden" name="branch_name" value="{{ $branch->id }}">
        @endif

        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Parent</label>

            <div class="col-sm-8">
                <select class="form-control register parent" id="parents" placeholder="Select Parent" name="parent">
                    <option></option>
                    @foreach($parents as $k => $val)
                        <option value="{{ $val->id }}" {{ ($student->parent && $student->parent == $val->id) ? 'selected' : '' }}> {{ $val->father_name }}
                            - {{ $val->father_mobile }} </option>
                    @endforeach
                </select>
                <div id="error_parent" class="col-sm-12 error">

                </div>
            </div>
            <a href='#' class='btn btn-default add-button' data-modal='add-parent-modal'
               onclick='openModal(this, event)'><i class='fa fa-plus'></i></a>

        </div>
        <div class="form-group">
            <label for="address" class="col-sm-2 control-label">Birth Date</label>
            <div class="col-sm-8" style="">
                <input type="text" name="bdate" value="{{ ($student->bdate) ? $student->bdate : '' }}"
                       placeholder="Birth Date" class="bdate form-control">
            </div>
        </div>
        <div class="form-group  ">
            <label for="mobile" class="col-sm-2 control-label">Primary Contact</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                    <input style="width: 150px" maxlength="10" type="text" id="mobile" name="mobile"
                           value="{{ $student->mobile }}"
                           class="form-control mobile" placeholder="Input Mobile">
                </div>
                <label id="error_mobile" class="error col-sm-12">

                </label>
            </div>

        </div>
        <div class="form-group ">
            <label for="mobile" class="col-sm-2 control-label">Secondary Contact</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                    <input style="width: 150px" maxlength="10" type="text" id="second_contact_no" name="second_contact_no" value=""
                           class="form-control mobile" placeholder="Secondary Contact">
                </div>
                <label id="error_mobile"  class="error col-sm-12">

                </label>
            </div>

        </div>
        <div class="form-group  ">
            <label for="email" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-8">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                    <input type="text" id="email" name="email" value="{{ $student->email }}" class="form-control email"
                           placeholder="Input Email">
                </div>
            </div>
        </div>

        <div class="form-group  ">
            <label for="address" class="col-sm-2 control-label"> Blood Group </label>

            <div class="col-sm-8">
                <select class="form-control register" placeholder="Select Blood Group" id="blood_group">
                    <option></option>
                    @foreach(config('app.blood_group') as $k => $group)
                        <option value="{{ $k }}" {{ ($student->blood_group && $student->blood_group == $val->id) ? 'selected' : '' }}> {{ $group }} </option>
                    @endforeach
                </select>

            </div>

        </div>
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">School</label>

            <div class="col-sm-8">
                <select placeholder="Select School" class="form-control register" name="school" id="school">
                    <option></option>
                    @foreach($schools as $k => $val)
                        <option value="{{ $val->id }}" {{ ($student->school && $student->school == $val->id) ? 'selected' : '' }}> {{ $val->name }} </option>
                    @endforeach
                </select>
                <div id="error_school" class="col-sm-12 error">

                </div>
            </div>
            <a href='#' class='btn btn-default add-button' data-modal='add-school-modal'
               onclick='openModal(this, event)'><i class='fa fa-plus'></i></a>

        </div>
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Same As Parent</label>

            <div class="col-sm-8 ">
                <input type="checkbox" name="same_as_parent" id="same_as_parent" class="minimal">
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">City</label>

            <div class="col-sm-8">
                <select placeholder="Select City" class="form-control register" name="city" id="city">
                    <option></option>
                    @foreach($cities as $k => $val)
                        <option value="{{ $val->id }}" {{ ($student->city_id && $student->city_id == $val->id) ? 'selected' : '' }}> {{ $val->name }} </option>
                    @endforeach
                </select>
            </div>
            <a href='#' class='btn btn-default add-button' data-modal='add-city-modal' onclick='openModal(this, event)'><i
                        class='fa fa-plus'></i></a>
        </div>
        <div class="form-group  ">
            <label for="address" class="col-sm-2 control-label">Address</label>

            <div class="col-sm-8">
                <textarea id="address" name="address" class="form-control" rows="5"
                          placeholder="Input Address"> {{ ($student->address) ? $student->address : '' }} </textarea>
                <div id="error_address" class="error col-sm-12">

                </div>
            </div>

        </div>
        <div class="form-group  ">
            <label for="address" class="col-sm-2 control-label">Aadhar No</label>

            <div class="col-sm-8">
                <input type="text" class="form-control" id="aadhar_no" name="aadhar_no" placeholder="Input Aadhar No">
            </div>

        </div>

        <div class="form-group ">
            <label for="address" class="col-sm-2 control-label">Membership</label>

            <div class="col-sm-8">
                <select name="isMember" id="isMember" class="form-control">
                    <option></option>
                    <option value="0">Fee Not Paid</option>
                    <option value="1">Fee Paid</option>
                    <option value="2"> Fee Waiver</option>
                </select>
                <label id="error_isMember" class="error col-sm-12">

                </label>
            </div>

        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label"><img src="{{ url('img/guardian.png') }}" width="25px"></label>
            <div class="col-sm-8">
                <h4> Guardian's Information </h4>
            </div>
        </div>

        <div class="form-group  ">
            <label for="address" class="col-sm-2 control-label">Name</label>

            <div class="col-sm-8">
                <input type="text" class="form-control"
                       value="{{ ($student->guardian_name) ? $student->guardian_name : '' }}" name="guardian_name">
            </div>

        </div>
        <div class="form-group  ">
            <label for="address" class="col-sm-2 control-label">Relation</label>

            <div class="col-sm-8">
                <select type="text" class="form-control relation register" name="guardian_relation">
                    <option></option>
                    @foreach(config('app.relation') as $k => $value)

                        <option value="{{ $k }}" {{ ($student->guardian_relation && $student->guardian_relation == $val->id) ? 'selected' : '' }}> {{ $value }} </option>

                    @endforeach
                </select>
            </div>

        </div>
        <div class="form-group  ">
            <label for="address" class="col-sm-2 control-label">Mobile</label>

            <div class="col-sm-8">
                <input type="text" class="form-control"
                       value="{{ ($student->guardian_mobile) ? $student->guardian_mobile : '' }}"
                       name="guardian_mobile">
            </div>

        </div>
        <div class="form-group  ">
            <label for="address" class="col-sm-2 control-label">Email</label>

            <div class="col-sm-8">
                <input type="text" class="form-control"
                       value="{{ ($student->guardian_email) ? $student->guardian_email : '' }}" name="guardian_email">
            </div>

        </div>
        <div class="form-group  ">
            <label for="address" class="col-sm-2 control-label">Profession</label>

            <div class="col-sm-8">
                <select class="form-control register" id="profession" name="guardian_profession">
                    <option></option>
                    @foreach($professions as $profession)
                        <option value="{{ $profession->id }}"
                                value="{{ ($student->guardian_profession && $student->guardian_profession == $profession->id) ? 'selected' : '' }}"> {{ $profession->name }} </option>
                    @endforeach
                </select>
            </div>

        </div>

        <hr>

        <input type="hidden" name="inquiry_id" value="{{ $student->id }}">

        <div class="form-group">
            <label class="col-sm-2 control-label"><img src="{{ url('img/activity.png') }}" width="25px"></label>
            <div class="col-sm-8">
                <h4> Enroll To Courses </h4>
            </div>
        </div>

        <div id="course_date_div">
            <div class="form-group">
                <label for="address" class="col-sm-2 control-label">Course</label>
                <div class="col-sm-8">
                    <select class="form-control course register" onchange="courseChange(this)" id=""
                            name="courses[]">
                        <option value="">Select Course</option>
                        @foreach($course as $key => $value)

                            <option value="{{ $value->id }}">
                                {{ $value->activity->name }}
                            </option>

                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="address" class="col-sm-2 control-label">Batch</label>
                <div class="col-sm-8">
                    <select class="form-control batch register" id="" name="batches[]">
                        <option value="">Batch</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="address" class="col-sm-2 control-label">Starts from</label>
                <div class="col-sm-5" style="">
                    <input type="text" name="date[]" placeholder="Start Month" class="date form-control ">
                </div>
                <button class="btn btn-default" onclick="addBox()" type="button"><i class="fa fa-plus"></i>
                </button>
            </div>
            @php $i=0; @endphp
            @foreach($student->courseOfInquiries as $inquiryCourse)
                {{--@if($i==0)--}}

                {{--@else--}}
                <div>
                    <div class="form-group">
                        <label for="address" class="col-sm-2 control-label">Course</label>
                        <div class="col-sm-8">
                            <select class="form-control course register" onchange="courseChange(this)" id=""
                                    name="courses[]">
                                <option value="">Select Course</option>
                                @foreach($course as $key => $value)

                                    <option value="{{ $value->id }}"
                                            @if($value->id == $inquiryCourse->course_id) selected @endif>
                                        {{ $value->activity->name }}
                                    </option>

                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address" class="col-sm-2 control-label">Batch</label>
                        <div class="col-sm-8">
                            <select class="form-control batch register" id="" name="batches[]">
                                @foreach($inquiryCourse->course->batch as $batch)
                                    <option value="{{ $batch->id }}">
                                        {{ $batch->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address" class="col-sm-2 control-label">Starts from</label>
                        <div class="col-sm-5" style="">
                            <input type="text" name="date[]" placeholder="Start Month" class="date form-control ">
                        </div>
                        <button class="btn btn-danger" onclick="removeBox(this)" type="button"><i
                                    class="fa fa-minus"></i></button>
                    </div>
                </div>
                {{--@endif--}}
                @php $i++; @endphp
            @endforeach
            <div id="hidden_user_course" style="display: none;">
                @if(isset($student->user))
                @foreach($student->user->userCourse->where('status','!=',2)  as $userCourses)
                    <div class="form-group">
                        <label for="address" class="col-sm-2 control-label">Course</label>
                        <div class="col-sm-8">
                            <select class="form-control course register" onchange="courseChange(this)" id=""
                                    name="courses[]">
                                <option value="">Select Course</option>
                                @foreach($course as $key => $value)

                                    <option value="{{ $value->id }}"
                                            {{ ($value->id == $userCourses->course_id) ? 'selected':'' }} >
                                        {{ $value->activity->name }}
                                    </option>

                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address" class="col-sm-2 control-label">Batch</label>
                        <div class="col-sm-8">
                            <select class="form-control batch register" id="" name="batches[]">
                                @foreach($inquiryCourse->course->batch as $batch)
                                    <option value="{{ $batch->id }}" {{ ($value->id == $userCourses->batch_id) ? 'selected':'' }}>
                                        {{ $batch->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address" class="col-sm-2 control-label">Starts from</label>
                        <div class="col-sm-5" style="">
                            <input type="text" name="date[]" placeholder="Start Month" class="date form-control ">
                        </div>
                        <button class="btn btn-danger" onclick="removeBox(this)" type="button"><i
                                    class="fa fa-minus"></i></button>
                    </div>
                @endforeach
                @endif
            </div>


        </div>

        <div class="form-group">
            <div class="col-sm-2">

            </div>
            <div class="col-sm-8">
                <label id="error_course">

                </label>
            </div>
            <div class="col-sm-8">

                <label id="error_batch">

                </label>

            </div>
            <div class="col-sm-8">
                <label id="error_date">

                </label>
            </div>
        </div>

        <div class="form-group  pull-right">
            <div class="col-sm-12 pull-right">
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
        </div>

    </form>

</div>


<div id="template" style="display: none;">

    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">Course</label>
        <div class="col-sm-8">
            <select class="form-control course register" onchange="courseChange(this)" id="" name="courses[]">
                <option value="">Select Course</option>
                @foreach($course as $key => $value)

                    <option value="{{ $value->id }}">
                        {{ $value->activity->name }}
                    </option>

                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">Batch</label>
        <div class="col-sm-8">
            <select class="form-control batch register" id="" name="batches[]">
                <option value="">Batch</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">Starts from</label>
        <div class="col-sm-5" style="">
            <input type="text" name="date[]" placeholder="Start Month" class="date form-control ">
        </div>
        <button class="btn btn-danger" onclick="removeBox(this)" type="button"><i class="fa fa-minus"></i></button>
    </div>
</div>

