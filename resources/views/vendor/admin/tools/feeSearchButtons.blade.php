<div class="btn-group pull-left" style="margin-left: 5px;">
    @php $type = request('type'); @endphp
    <span class="btn btn-default btn-flat fee-filter @if(!isset($type) || request('type') == 4) active @endif" data-type="4">All</span>
    <span class="btn btn-default btn-flat fee-filter @if(request('type') == 1) active @endif" data-type="1">Unpaid</span>
    <span class="btn btn-default btn-flat fee-filter @if(request('type') == 2) active @endif" data-type="2">Partially Paid</span>
    <span class="btn btn-default btn-flat fee-filter @if(request('type') == 3) active @endif" data-type="3">Fully Paid</span>

</div>