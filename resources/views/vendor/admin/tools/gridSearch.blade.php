<div class="col-md-8 col-lg-8 pull-right" style="padding-right: 5px;">
    <div style="float: right; padding: 0 5px" class="btn-group pull-right">
        <button type="submit" class="btn btn-sm btn-primary"  id="search-submit">Search</button>
        <button type="button" class="btn btn-sm btn-warning"  id="reset-search">Reset</button>
    </div>
    <div style="padding-right: 0; float: right; width: 50%">
        <input type="text" class="form-control" name="grid-search" placeholder="Search" value="{{request('search')}}" id="grid-search" style="padding: 0 5px; height: 30px;">
    </div>
</div>
