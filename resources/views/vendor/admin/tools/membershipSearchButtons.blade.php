<div class="btn-group pull-left" style="margin-left: 5px;">
    @php $type = request('type'); @endphp
    <span class="btn btn-default btn-flat membership-filter @if(!isset($type) || request('type') == 1) active @endif" data-type="1">Expired / New</span>
    <span class="btn btn-default btn-flat membership-filter @if(request('type') == 2) active @endif" data-type="2">About to Expire</span>
    <span class="btn btn-default btn-flat membership-filter @if(request('type') == 3) active @endif" data-type="3">Active</span>
    <span class="btn btn-default btn-flat membership-filter @if(request('type') == 4) active @endif" data-type="4">All</span>
</div>