<form {!! $attributes !!}>
    <div class="box-body">

        {!! csrf_field() !!}

        @foreach($fields as $field)
            {!! $field->render() !!}
        @endforeach

        <button type="submit" class="btn btn-info">{{ trans('admin.submit') }}</button>
    </div>

    <!-- /.box-body -->
    {{--<div class="box-footer">--}}
        {{--@if( ! $method == 'GET')--}}
            {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
        {{--@endif--}}
        {{--<div class="col-md-2"></div>--}}

        {{--<div class="col-md-8">--}}
            {{--<div class="btn-group pull-left">--}}
            {{--<button type="reset" class="btn btn-warning pull-right">{{ trans('admin.reset') }}</button>--}}
            {{--</div>--}}


        {{--</div>--}}

    {{--</div>--}}
</form>
