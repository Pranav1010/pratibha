@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet"
          href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'blue') . '.min.css')}} ">
    @stack('css')
    @yield('css')
@stop

@section('body_class', 'layout-top-nav' .' skin-' . config('adminlte.skin', 'blue') . (config('adminlte.layout') ? [
    'boxed' => 'layout-boxed',
    'fixed' => 'fixed',
    'top-nav' => 'layout-top-nav'
][config('adminlte.layout')] : '') . (config('adminlte.collapse_sidebar') ? ' sidebar-collapse ' : ''))

@section('body')
    <div class="wrapper">

        <!-- Main Header -->
        <header class="main-header">
            @if(config('adminlte.layout') == 'top-nav')
                <nav class="navbar navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <span>Inquiries</span>
                            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="navbar-brand">
                                {!! config('adminlte.logo', '<b>Admin</b>LTE') !!}
                            </a>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                            <ul class="nav navbar-nav">
                                @each('adminlte::partials.menu-item-top-nav', $adminlte->menu(), 'item')
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    @else
                        <!-- Logo -->
                            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="logo">
                                <span>Inquiries</span>
                                <!-- mini logo for sidebar mini 50x50 pixels -->
                                <span class="logo-mini">{!! config('adminlte.logo_mini', '<b>A</b>LT') !!}</span>
                                <!-- logo for regular state and mobile devices -->
                                <span class="logo-lg">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</span>
                            </a>

                            <!-- Header Navbar -->
                            <nav class="navbar collapse navbar-collapse" id="navbar-collapse" role="navigation"
                                 style="z-index: -1">
                                <!-- Sidebar toggle button-->
                                {{-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                                    <span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>
                                </a> --}}
                                @endif
                                <ul class="nav navbar-nav" style="background: #265671">
                                    <li id="dashboard-menu"><a href="{{url('/home')}}"><i
                                                    class="fa fa-dashboard"></i><span> Dashboard</span></a></li>
                                    <li id="inquiry-menu"><a href="{{url('/inquiries')}}"><i
                                                    class="fa fa-info-circle "></i><span> Inquiries</span></a></li>
                                    @if(Auth::id() == 1)
                                        <li id="user-menu"><a href="{{url('/users')}}"><i class="fa fa-user"></i><span> Team</span></a>
                                        </li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" id="master-menu" data-toggle="dropdown">
                                                <i class="fa fa-bars" aria-hidden="true"></i>
                                                Master <span class="caret"></span></a>
                                            <ul class="dropdown-menu" role="menu">
                                                <li id="country-menu"><a href="{{url('/countries')}}">Country</a></li>
                                                <li id="city-menu"><a href="{{url('/cities')}}">City</a></li>
                                                <li id="area-menu"><a href="{{url('/areas')}}">Area</a></li>
                                                <li id="course-menu"><a
                                                            href="{{url('/courses')}}"><span> Programs</span></a></li>
                                                <li id="branch-menu"><a
                                                            href="{{url('/branches')}}"><span> Branches</span></a></li>
                                                <li id="source-menu"><a href="{{url('/sources')}}"><span> Sources</span></a>
                                                </li>
                                                <li id="role-menu"><a href="{{url('/roles')}}"><span> Roles</span></a>
                                                </li>
                                                <li id="department-menu"><a href="{{url('/departments')}}"><span> Departments</span></a>
                                                </li>
                                                <li id="designation-menu"><a href="{{url('/designations')}}"><span> Designations</span></a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif
                                </ul>


                                <!-- Navbar Right Menu -->
                                <div class="navbar-custom-menu">

                                    <ul class="nav navbar-nav">
                                        <li>
                                            <a>Hello, &nbsp;<strong>{{ Auth::user()->name }}</strong></a>
                                        </li>
                                        <li>
                                            @if(config('adminlte.logout_method') == 'GET' || !config('adminlte.logout_method') && version_compare(\Illuminate\Foundation\Application::VERSION, '5.3.0', '<'))
                                                <a href="{{ url(config('adminlte.logout_url', 'auth/logout')) }}">
                                                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                                </a>
                                            @else
                                                <a href="#"
                                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                                </a>
                                                <form id="logout-form"
                                                      action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}"
                                                      method="POST" style="display: none;">
                                                    @if(config('adminlte.logout_method'))
                                                        {{ method_field(config('adminlte.logout_method')) }}
                                                    @endif
                                                    {{ csrf_field() }}
                                                </form>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                            @if(config('adminlte.layout') == 'top-nav')
                    </div>
                    @endif
                </nav>
        </header>

    @if(config('adminlte.layout') != 'top-nav')
        <!-- Left side column. contains the logo and sidebar -->
        {{-- <aside class="main-sidebar">

            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

                <!-- Sidebar Menu -->
                <ul class="sidebar-menu">
                    @each('adminlte::partials.menu-item', $adminlte->menu(), 'item')
                </ul>
                <!-- /.sidebar-menu -->
            </section>
            <!-- /.sidebar -->
        </aside> --}}
    @endif

    <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @if(config('adminlte.layout') == 'top-nav')
                <div class="container">
                @endif

                <!-- Content Header (Page header) -->
                    <section class="content-header">
                        @yield('content_header')
                    </section>

                    <!-- Main content -->
                    <section class="content">

                        @yield('content')

                    </section>
                    <!-- /.content -->
                    @if(config('adminlte.layout') == 'top-nav')
                </div>
                <!-- /.container -->
            @endif
        </div>
        <!-- /.content-wrapper -->

    </div>
    <!-- ./wrapper -->
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/app.min.js') }}"></script>
    @stack('js')
    @yield('js')
@stop
