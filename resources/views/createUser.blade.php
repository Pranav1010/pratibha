@extends('adminlte::page')

@section('title', 'Manage Team')

@section('content')

    <div class="register-box">
        <div class="register-logo">
            <a href="/">Register Team</a>
        </div>

        <div class="register-box-body">
            <p class="login-box-msg">Register a new membership</p>

            <form action="{{url('/users')}}" method="post">
                {!! csrf_field() !!}

                <div class="form-group has-feedback">
                    <input type="text" name="name" class="form-control" value="{{ old('name') }}"
                           placeholder="Full name">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    @if ($errors->has('name'))
                        <span class="help-block" style="color: #dd4b39">
                            {{ $errors->first('name') }}
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                           placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block">
                           {{ $errors->first('email') }}
                        </span>
                    @endif
                </div>

                <div class="form-group has-feedback {{ $errors->has('username') ? 'has-error' : '' }}">
                    <input type="text" name="username" class="form-control" value="{{ old('username') }}"
                           placeholder="Username">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('username'))
                        <span class="help-block">
                            {{ $errors->first('username') }}
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <input type="password" name="password" class="form-control"
                           placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            {{ $errors->first('password') }}
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <input type="password" name="password_confirmation" class="form-control"
                           placeholder="Retype Password">
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            {{ $errors->first('password_confirmation') }}
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <select name="branch_id" class="form-control">
                        <option value="0">Select Branch</option>
                        @foreach($branches as $branch)
                            <option value="{{$branch->id}}">{{$branch->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('branch_id'))
                        <span class="help-block" style="color: #dd4b39">
                            {{ $errors->first('branch_id') }}
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <select name="role_id" class="form-control">
                        <option value="0">Select Role</option>
                        @foreach($roles as $role)
                            <option value="{{$role->id}}">{{$role->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('role_id'))
                        <span class="help-block" style="color: #dd4b39">
                            {{ $errors->first('role_id') }}
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <select name="designation_id" class="form-control">
                        <option value="0">Select Designation</option>
                        @foreach($designations as $designation)
                            <option value="{{$designation->id}}">{{$designation->name}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('designation_id'))
                        <span class="help-block" style="color: #dd4b39">
                            {{ $errors->first('designation_id') }}
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <label class="radio-inline ">
                        <input type="radio" name="status" value="1"> Active
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="status" value="2"> Inactive
                    </label>
                </div>
                <div align="right">
                    <button type="submit" class="btn btn-primary btn-flat">Register</button>
                    <button type="reset" class="btn btn-primary btn-flat">Reset</button>
                    <a href="{{url('/users')}}" class="btn btn-primary btn-flat">Cancel</a>
                </div>
            </form>
        </div>
        <!-- /.form-box -->
    </div><!-- /.register-box -->
@stop

@section('adminlte_js')
    @yield('js')
@stop

