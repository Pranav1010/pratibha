<table class="table table-bordered">
    <thead>
    <tr>
        @foreach($headers as $header)
            <th>{{$header}}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($rows as $row)
        <tr>
            @foreach($columns as $column)
                <td>{{(isset($row[$column])) ? $row[$column] : ''}}</td>
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>