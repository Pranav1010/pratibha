<div class="row">
    {{--<div class="col-md-2">--}}
        {{--<div class="small-box bg-blue-gradient" style="box-shadow: 3px 4px 10px #888;">--}}
            {{--<div class="inner">--}}
                {{--<h3>{{$totalFee}}</h3>--}}
                {{--<p>Total Fees</p>--}}
            {{--</div>--}}
            {{--<div class="icon">--}}
                {{--<i class="fa fa-rupee"></i>--}}
            {{--</div>--}}
            {{--<a href="{{url('/inquiries')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="col-md-2">
        <div class="small-box bg-green-gradient" style="box-shadow: 3px 4px 10px #888;">
            <div class="inner">
                <h3>{{$paidAmount}}</h3>
                <p>Total Received</p>
            </div>
            <div class="icon">
                <i class="fa fa-arrow-down"></i>
            </div>
            {{--<a href="{{url('/inquiries')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
        </div>
    </div>
    <div class="col-md-2">
        <div class="small-box bg-purple-gradient" style="box-shadow: 3px 4px 10px #888;">
            <div class="inner">
                <h3>{{$totalDiscount}}</h3>
                <p>Total Discount</p>
            </div>
            <div class="icon">
                <i class="fa fa-percent"></i>
            </div>
            {{--<a href="{{url('/inquiries')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
        </div>
    </div>
    <div class="col-md-2">
        <div class="small-box bg-blue-gradient" style="box-shadow: 3px 4px 10px #888;">
            <div class="inner">
                <h3>{{$refundAmount}}</h3>
                <p>Total Refund</p>
            </div>
            <div class="icon">
                <i class="fa fa-arrow-up"></i>
            </div>
            {{--<a href="{{url('/inquiries')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
        </div>
    </div>
    <div class="col-md-2">
        <div class="small-box bg-aqua-gradient" style="box-shadow: 3px 4px 10px #888;">
            <div class="inner">
                <h3>{{$totalAdvance}}</h3>
                <p>Total Advance</p>
            </div>
            <div class="icon">
                <i class="fa fa-rupee"></i>
            </div>
            {{--<a href="{{url('/inquiries')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
        </div>
    </div>
    <div class="col-md-2">
        <div class="small-box bg-red-gradient" style="box-shadow: 3px 4px 10px #888;">
            <div class="inner">
                <h3>{{$pendingAmount}}</h3>
                <p>Total Pending</p>
            </div>
            <div class="icon">
                <i class="fa fa-rupee"></i>
            </div>
            {{--<a href="{{url('/inquiries')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
        </div>
    </div>
</div>