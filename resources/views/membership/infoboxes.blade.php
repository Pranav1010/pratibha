<div class="row">
    <div class="col-md-2">
        <div class="small-box bg-red-gradient" style="box-shadow: 3px 4px 10px #888;">
            <div class="inner">
                <h3>{{$expired}}</h3>
                <p>Expired / New</p>
            </div>
            <div class="icon">
                <i class="fa fa-ban"></i>
            </div>
            {{--<a href="{{url('/inquiries')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
        </div>
    </div>
    <div class="col-md-2">
        <div class="small-box bg-yellow-gradient" style="box-shadow: 3px 4px 10px #888;">
            <div class="inner">
                <h3>{{$aboutToExpire}}</h3>
                <p>About to Expire</p>
            </div>
            <div class="icon">
                <i class="fa fa-arrow-down"></i>
            </div>
            {{--<a href="{{url('/inquiries')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
        </div>
    </div>
    <div class="col-md-2">
        <div class="small-box bg-green-gradient" style="box-shadow: 3px 4px 10px #888;">
            <div class="inner">
                <h3>{{$active}}</h3>
                <p>Active</p>
            </div>
            <div class="icon">
                <i class="fa fa-thumbs-o-up"></i>
            </div>
            {{--<a href="{{url('/inquiries')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>--}}
        </div>
    </div>
</div>