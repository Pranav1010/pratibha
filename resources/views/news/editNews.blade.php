
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Edit</h3>
    </div>
    <form action="{{url('/news/'.$news->id)}}" method="POST" class="form-horizontal">

        {{ csrf_field() }}
        {{method_field('PUT')}}

         <input type="hidden" value="{{$news->id}}" id="newsId" name="newsId">

        @include('news.form')

    </form>
    <!-- /.box-body -->
</div>

<style>
    hr {
        border: 0;
        width: 96%;
        height: 1px;
    }
</style>
<script>


</script>