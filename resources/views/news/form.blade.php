<div class="box-body">
    <input type="hidden" value="{{  $branch->id or ''}}" name="branch_id">
    <div class="fields-group">
        <div class="form-group">
            <label class="col-sm-2 control-label">News Title:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control " name="title" placeholder="Input News Title"
                       value="{{ $news->title or '' }}">
                <span class="help-block title" style="color: red"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Author:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="author" placeholder="Input News Author"
                       value="{{ $news->author or '' }}">
                <span class="help-block author" style="color: red;" id="errorMessage"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Date:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="date" placeholder="Input News Date"
                       id="date" value="{{ isset($news->date) ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$news->date)->format('d/m/Y h:i a') : '' }}">
                <span class="help-block date" style="color: red;" id="errorMessage"></span>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">Description:</label>
            <div class="col-sm-10">
                <textarea name="description" placeholder="Input Event Description" class="form-control" rows="4"
                          id="summernote"> {{ $news->description or '' }} </textarea>
                <span class="help-block description" style="color: red" id="errorMessage"></span>
            </div>
        </div>

    </div>
</div>

<div class="box-footer">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="btn-group pull-right">
            <button type="submit" class="btn btn-info pull-right"
                    data-loading-text="<i class='fa fa-spinner fa-spin '></i> Submit">Submit
            </button>
        </div>
        <div class="btn-group pull-left">
            <button type="reset" class="btn btn-warning">Reset</button>
        </div>
    </div>
</div>

@include('news.modal.newsDetails');