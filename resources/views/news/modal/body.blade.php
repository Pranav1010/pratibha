<table class="table no-header table-bordered">
    <tbody>
    <tr>
        <td width="25%">
            <b>News Title</b>
        </td>
        <td>
            {{$news->title or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Author</b>
        </td>
        <td>
            {{$news->author or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Date & Time</b>
        </td>
        <td>
            {{$news->date or "-"}}
        </td>
    </tr>
    <tr>
        <td>
            <b>Description</b>
        </td>
        <td>
            {!! $news->description !!}
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <button data-href="/news/{{ $news->id }}/edit" class="btn btn-primary pull-right" id="goToEdit">Edit Details</button>
        </td>
    </tr>
    </tbody>
</table>
