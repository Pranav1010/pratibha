
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Refund Information</h3>

    </div>
    <form action="{{url('/payments/refund/'.$school->id)}}" method="POST" class="form-horizontal">

        {{ csrf_field() }}

        @include('refund.form')

    </form>
    <!-- /.box-body -->
</div>
<style>
    hr {
        border: 0;
        width: 96%;
        height: 1px;
    }
</style>
<script>

$(document).ready(function(){
    $('[data-toggle="popover"]').popover({html: true, placement: "left"});   
});
</script>