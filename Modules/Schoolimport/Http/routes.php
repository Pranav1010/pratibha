<?php

Route::group(['middleware' => 'web', 'prefix' => 'schoolimport', 'namespace' => 'Modules\Schoolimport\Http\Controllers'], function()
{
    Route::get('/progress', 'SchoolimportController@progress');
    Route::post('/upload', 'SchoolimportController@upload');
    Route::resource('/', SchoolimportController::class);
});
