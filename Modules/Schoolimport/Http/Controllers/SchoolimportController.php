<?php

namespace Modules\Schoolimport\Http\Controllers;

use App\School;
use App\SchoolFees;
use App\SchoolPendingPayment;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Encore\Admin\Auth\Database\Role;
use Illuminate\Database\QueryException;
use Encore\Admin\Facades\Admin;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Nwidart\Modules\Facades\Module;
use Encore\Admin\Layout\Content;

class SchoolimportController extends Controller
{
    public function __construct()
    {
        Admin::script([
            "jQuery.getScript('" . Module::asset('schoolimport:js/schoolimport.js') . "');",
        ]);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('School Import & Export');
            $content->body(view('schoolimport::index'));
        });
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function upload(Request $request)
    {
        $validation = $request->validate([
            'schoolFile' => 'required|mimes:xlsx',
        ]);
        $request->file('schoolFile')->move(public_path('master'), 'Schools.xlsx');
        $file = public_path('master/Schools.xlsx');
        Excel::load($file, function($reader) use($request) {

           $sheet = $reader->all();
           $rows = $sheet->all();
           Session::forget('progress');
           Session::put('progress',0);
           $totalRows = count($rows);
           $i=1;
           foreach ($rows as $row)
           {
               $schoolFees =new SchoolFees();
               $schoolFees->school_id = $row->id;
               $schoolFees->total_fees = $row->amount;
               $schoolFees->date = date('Y-m-d');
               $schoolFees->term = $row->financial_year;
               $schoolFees->save();

               $schoolPendingPayment =new SchoolPendingPayment();
               $schoolPendingPayment->school_id = $row->id;
               $schoolPendingPayment->amount = $row->amount;
               $schoolPendingPayment->year = $row->financial_year;
               $schoolPendingPayment->save();
               $per = ($i * 100)/$totalRows;
               Session::put('progress',$per);
               $i++;
           }

        });

        return "success";

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function progress()
    {
        if(Session::has('progress'))
        {
            return Session::get('progress');
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $financialYear = $request->get('financialYear');
        $file = Excel::create('Schools', function($excel) use($financialYear) {

            $excel->sheet('Schools', function($sheet) use($financialYear){

                $sheet->row(1, array(
                    'ID', 'School Name','City','Address','Amount','Financial Year'
                ));

                $sheet->cells('A1:F1', function($cells) {

                    // Set font weight to bold
                    $cells->setFontWeight('bold');

                });

                $schools = School::all();
                $i=2;
                foreach ($schools as $school)
                {
                    $sheet->row($i, array(
                        $school->id, $school->name,$school->city->name,$school->address,0,$financialYear
                    ));
                    $i++;
                }

                $sheet->freezeFirstRowAndColumn();

            });

        })->download('xlsx');
#
        return response()->file($file);
    }



    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('schoolimport::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
