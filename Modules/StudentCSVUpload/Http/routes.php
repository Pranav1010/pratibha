<?php

Route::group(['middleware' => 'web', 'prefix' => 'studentcsvupload', 'namespace' => 'Modules\StudentCSVUpload\Http\Controllers'], function()
{
    Route::get('/upload', 'StudentCSVUploadController@upload');
    Route::resource('/', 'StudentCSVUploadController');
});
