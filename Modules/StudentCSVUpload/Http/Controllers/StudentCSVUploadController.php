<?php

namespace Modules\StudentCSVUpload\Http\Controllers;
use App\AppUser;
use Carbon\Carbon;
use App\Batch;
use App\City;
use App\Course;
use App\CourseFeeType;
use App\CourseStudent;
use App\CourseStudentSchool;
use App\Fee;
use App\FeeDetail;
use App\Parents;
use App\PendingFee;
use App\Student;
use App\Activity;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class StudentCSVUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('studentcsvupload::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('studentcsvupload::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $filename = $request->file;
        $file = fopen($filename, "r");
        $i=0;
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
        {
            if($i != 0)
            {
                echo "<pre>";
                echo "</br>------------------------------------------------------------------------------</br>";
                echo '<h4>SR No'.$i.'->'.$getData[0]."</h4></br>";
                print_r($this->saveStudent($getData));
                echo "</pre>";
            }
            $i++;
        }

        fclose($file);
    }

    public function upload()
    {
        $filename = base_path('public/upload/files/Student_CSV_File.csv');
        $file = fopen($filename, "r");
        $i=0;
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
        {
            if($i != 0)
            {
                echo "<pre>";
                echo "</br>------------------------------------------------------------------------------</br>";
                echo '<h4>SR No'.$i.'->'.$getData[0]."</h4></br>";
                print_r($this->saveStudent($getData));
                echo "</pre>";
            }
            $i++;
        }

        fclose($file);
    }

    public function saveStudent($row)
    {
        try {
            DB::beginTransaction();
            $user = new AppUser();
            $user->user = $row[1]; //username
            $user->password = bcrypt(config('app.default_password')); //password
            $user->name = trim($row[0]); //name
            $user->contact_no = $row[1]; //Primary Mobile
            $user->address = $row[6]; //Address
            $user->fee_status = 2; //Fee Status
            $user->role_id = 3; //Role Id
            $user->relationship_id = 0; //Relationship Id
            $user->save();

            $student = new Student();
            $student->student_category = 0;
            $student->second_contact_no = $row[2];
            $student->gender = $row[8]; //gender
            $student->user_id = $user->id; //User ID
            $student->bdate = ($row[7] != "") ? Carbon::createFromFormat('d/m/Y', trim($row[7]))->toDateTimeString() : null; //Birth Date
            $student->fee_status = 1; //Fee Status
            $student->branch_id = 1; //Branch ID
            $student->save();

            $user->relationship_id = $student->id;
            $user->save();

            $courses = explode(",", $row[3]);
            $batches = explode(",", $row[4]);
            $fee_types = explode(",", $row[16]);
            $fees_taken = explode(",", $row[17]);

            $i = 0;
            echo "</br>";
            foreach ($courses as $course) {
                $activity = Activity::where("name", trim($course))->first();
                $course_info = Course::where("activity_id", $activity->id)->where("branch_id", $student->branch_id)->first();

                echo $activity->name.",";

                if ($course_info) {
                    $batch = 2;
                    if ($batches) {
                        $batch_info = Batch::where("name","LIKE","%".trim($batches[$i])."%")->where("course_id", $course_info->id)->first();
                        $batch = $batch_info ? $batch_info->id : 2;
                    }

                    $fee_type = $this->getFeeType($fee_types[$i]);

                    $months = [3,2,1,12,11,10,9,8,7,6,5,4];
                    $joinMonth = 4;
                    //Student Course Start Month Calculation
                    if($fee_types[$i] == 'HY')
                    {
                        $month = 6 - (int)$fees_taken[$i];
                        if($month >= 0 AND $month <= 12)
                        {
                            $joinMonth = $months[$month];
                            if($joinMonth > 3)
                            {
                                $year = date("Y");
                                $previousyear = $year -1;
                                $joinMonth = $joinMonth."-".$previousyear;
                            }
                            else {
                                $year = date("Y");
                                $joinMonth = $joinMonth."-".$year;
                            }
                        }
                        elseif ($month >= -1)
                        {
                            $year = date("Y");
                            $joinMonth = '04-'.$year;
                        }
                    }
                    else if($fee_types[$i] == 'Y')
                    {
                        $month = 12 - (int)$fees_taken[$i];
                        if($month >= 0 AND $month <= 12)
                        {
                            $joinMonth = $months[$month];
                            if($joinMonth > 3)
                            {
                                $year = date("Y");
                                $previousyear = $year -1;
                                $joinMonth = $joinMonth."-".$previousyear;
                            }
                            else {
                                $year = date("Y");
                                $joinMonth = $joinMonth."-".$year;
                            }
                        }
                        elseif ($month >= -1)
                        {
                            $year = date("Y");
                            $joinMonth = '04-'.$year;
                        }

                    }
                    else if($fee_types[$i] == 'M')
                    {
                        $year = date("Y");
                        $joinMonth = '04-'.$year;
                    }


                    $courseStudent= CourseStudent::create([
                        "course_id" => $course_info->id,
                        "student_id" => $user->id,
                        "start_date" => $joinMonth,
                        "batch_id" => $batch,
                        "fee_type" => $fee_type
                    ]);

                    $fee_amount = CourseFeeType:: where([
                        "course_id" => $course_info->id,
                        "fee_type_id" => $fee_type
                    ])->value("fees");

                    //Half Yearly Calculation
                    if($fee_types[$i] == 'HY')
                    {
                        if($fees_taken[$i] == 6)
                        {
                            $fee_info = Fee::create([
                                "user_id" => $user->id,
                                "payment_date" => date("Y-m-d H:i:s",mktime(0,0,0,4,1,2018)),
                                "net_amount" => $fee_amount,
                                "lump_sum_amount" => 0,
                                "payment_mode" => 0
                            ]);

                            FeeDetail::create([
                                "fee_id" => $fee_info->id,
                                "course_id" => $course_info->id,
                                "discount" => 0,
                                "paid_amount" => $fee_amount
                            ]);
                        }

                        if($fees_taken[$i] < 6)
                        {

                            $perMonthPrice = $fee_amount/6;
                            $paidAmount = (float)$fees_taken[$i] * (float)$perMonthPrice;
                            $pendingAmount = $fee_amount - $paidAmount;

                            $fee_info = Fee::create([
                                "user_id" => $user->id,
                                "payment_date" => date("Y-m-d H:i:s",mktime(0,0,0,4,1,2018)),
                                "net_amount" => $fee_amount,
                                "lump_sum_amount" => 0,
                                "payment_mode" => 0
                            ]);

                            FeeDetail::create([
                                "fee_id" => $fee_info->id,
                                "course_id" => $course_info->id,
                                "discount" => 0,
                                "paid_amount" => $paidAmount
                            ]);

                            //Pending Fees
                            PendingFee::create([
                                "user_id" => $user->id,
                                "course_id" => $course_info->id,
                                "month" => 8,
                                "year" => 2018,
                                "fees" => $pendingAmount,
                                "fees_type" => $fee_type
                            ]);

                        }

                        if($fees_taken[$i] > 6)
                        {
                            $perMonthPrice = $fee_amount/6;
                            $paidAmount = $fees_taken[$i] * $perMonthPrice;
                            $advancePaid = $paidAmount - $fee_amount;
                            $student->advance_amount = $student->advance_amount + $advancePaid;
                            $fee_info = Fee::create([
                                "user_id" => $user->id,
                                "payment_date" => date("Y-m-d H:i:s",mktime(0,0,0,4,1,2018)),
                                "net_amount" => $fee_amount,
                                "lump_sum_amount" => 0,
                                "payment_mode" => 0
                            ]);

                            FeeDetail::create([
                                "fee_id" => $fee_info->id,
                                "course_id" => $course_info->id,
                                "discount" => 0,
                                "paid_amount" => $fee_amount
                            ]);
                        }
                    }

                    //Yearly Calculation
                    if($fee_types[$i] == 'Y')
                    {
                        if($fees_taken[$i] == 12)
                        {
                            $fee_info = Fee::create([
                                "user_id" => $user->id,
                                "payment_date" => date("Y-m-d H:i:s",mktime(0,0,0,4,1,2018)),
                                "net_amount" => $fee_amount,
                                "lump_sum_amount" => 0,
                                "payment_mode" => 0
                            ]);

                            FeeDetail::create([
                                "fee_id" => $fee_info->id,
                                "course_id" => $course_info->id,
                                "discount" => 0,
                                "paid_amount" => $fee_amount
                            ]);
                        }

                        if($fees_taken[$i] < 12)
                        {
                            $perMonthPrice = $fee_amount/12;
                            $paidAmount = $fees_taken[$i] * $perMonthPrice;
                            $pendingAmount = $fee_amount - $paidAmount;

                            $fee_info = Fee::create([
                                "user_id" => $user->id,
                                "payment_date" => date("Y-m-d H:i:s",mktime(0,0,0,4,1,2018)),
                                "net_amount" => $fee_amount,
                                "lump_sum_amount" => 0,
                                "payment_mode" => 0
                            ]);

                            FeeDetail::create([
                                "fee_id" => $fee_info->id,
                                "course_id" => $course_info->id,
                                "discount" => 0,
                                "paid_amount" => $paidAmount
                            ]);

                            //Pending Fees
                            PendingFee::create([
                                "user_id" => $user->id,
                                "course_id" => $course_info->id,
                                "month" => 8,
                                "year" => 2018,
                                "fees" => $pendingAmount,
                                "fees_type" => $fee_type
                            ]);

                        }

                        if($fees_taken[$i] > 12)
                        {
                            $perMonthPrice = $fee_amount/12;
                            $paidAmount = $fees_taken[$i] * $perMonthPrice;
                            $advancePaid = $paidAmount - $fee_amount;
                            $student->advance_amount = $student->advance_amount + $advancePaid;
                            $fee_info = Fee::create([
                                "user_id" => $user->id,
                                "payment_date" => date("Y-m-d H:i:s",mktime(0,0,0,4,1,2018)),
                                "net_amount" => $fee_amount,
                                "lump_sum_amount" => 0,
                                "payment_mode" => 0
                            ]);

                            FeeDetail::create([
                                "fee_id" => $fee_info->id,
                                "course_id" => $course_info->id,
                                "discount" => 0,
                                "paid_amount" => $fee_amount
                            ]);
                        }
                    }

                    //Monthly Calculation
                    if($fee_types[$i] == 'M')
                    {
                        $payMonth = 4;
                        if($fees_taken[$i] == 5)
                        {
                            for($k=0;$k<5;$k++)
                            {
                                $fee_info = Fee::create([
                                    "user_id" => $user->id,
                                    "payment_date" => date("Y-m-d H:i:s",mktime(0,0,0,$payMonth++,1,2018)),
                                    "net_amount" => $fee_amount,
                                    "lump_sum_amount" => 0,
                                    "payment_mode" => 0
                                ]);

                                FeeDetail::create([
                                    "fee_id" => $fee_info->id,
                                    "course_id" => $course_info->id,
                                    "discount" => 0,
                                    "paid_amount" => $fee_amount
                                ]);
                            }
                        }

                        if($fees_taken[$i] < 5)
                        {
                            $pendingMonth = 5 - (int)$fees_taken[$i];
                            $pendingAmount = ((int)$pendingMonth * (int)$fee_amount);

                            for($k=0;$k < $fees_taken[$i];$k++)
                            {
                                $fee_info = Fee::create([
                                    "user_id" => $user->id,
                                    "payment_date" => date("Y-m-d H:i:s",mktime(0,0,0,$payMonth++,1,2018)),
                                    "net_amount" => $fee_amount,
                                    "lump_sum_amount" => 0,
                                    "payment_mode" => 0
                                ]);

                                FeeDetail::create([
                                    "fee_id" => $fee_info->id,
                                    "course_id" => $course_info->id,
                                    "discount" => 0,
                                    "paid_amount" => $fee_amount
                                ]);
                            }


                            //Pending Fees
                            PendingFee::create([
                                "user_id" => $user->id,
                                "course_id" => $course_info->id,
                                "month" => 8,
                                "year" => 2018,
                                "fees" => $pendingAmount,
                                "fees_type" => $fee_type
                            ]);

                        }

                        if($fees_taken[$i] > 5)
                        {
                            $advanceMonth = $fees_taken[$i] - 5;
                            $advancePaid = $advanceMonth * $fee_amount;
                            $student->advance_amount = $student->advance_amount + $advancePaid;

                            for($k=0;$k<5;$k++)
                            {
                                $fee_info = Fee::create([
                                    "user_id" => $user->id,
                                    "payment_date" => date("Y-m-d H:i:s",mktime(0,0,0,$payMonth++,1,2018)),
                                    "net_amount" => $fee_amount,
                                    "lump_sum_amount" => 0,
                                    "payment_mode" => 0
                                ]);

                                FeeDetail::create([
                                    "fee_id" => $fee_info->id,
                                    "course_id" => $course_info->id,
                                    "discount" => 0,
                                    "paid_amount" => $fee_amount
                                ]);
                            }
                        }
                    }


                    $student->fee_status = 2;
                    $user->fee_status = 2;
                    $user->save();
                    $student->save();
                }
                $i++;
            }
            //Create Unpaid Membership
            PendingFee::create([
                "user_id" => $user->id,
                "course_id" => 1,
                "month" => 8,
                "year" => 2018,
                "fees" => 500,
                "fees_type" => 2
            ]);


        } catch (QueryException $exception) {
            DB::rollBack();
            //dd($exception->getMessage());
            return [
                "status" => false,
                //"error" => $exception->getMessage()
            ];
        }
        DB::commit();
        return [
            "status" => true,
            //"error" => $user
        ];
    }

    function getFeeType($feeType)
    {
        switch ($feeType) {
            case "M" :
                return 1;
                break;
            case "Y" :
                return 2;
                break;
            case "HY" :
                return 4;
                break;
            case "S" :
                return 3;
                break;
        }

        return 0;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('studentcsvupload::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('studentcsvupload::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
