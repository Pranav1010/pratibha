<div class="modal-content">
    <form id="studentImport" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6 col-md-offset-1">
                    <p><b>Instructions :</b></p>
                    <table class="table table-striped no-header table-bordered">
                        <tbody>
                        <tr>
                            <td width="20%">
                                <h5><b>STEP 1 :</b></h5>
                            </td>
                            <td>
                                <label class="btn btn-sm" style="background-color: #0db700; color: white">
                                    Click Here <input type="file" accept=".xls,.xlsx" required class="mark_file" style="display: none"
                                                      id="fileToUpload"/>
                                    <input type="hidden" name="bulk_file" id="file_container" value=""/>
                                    <input type="hidden" name="data[]" id="data"/>
                                </label>
                                <span id="fileName" style="color: #0db700">(Filename)&nbsp;</span>
                                <span>&nbsp;to select file filled with student information.</span>
                                <br>
                                <span class="text-danger bulk_file error-text">{{ $errors->first('bulk_file') }}</span>
                            </td>
                        </tr>
                        <tr>

                            <td>
                                <h5><b>STEP 2 :</b></h5>
                            </td>
                            <td>
                                <a href="/students" id="getStudentList" style="margin: 0; padding: 0;"></a>
                                <button type="button" data-loading-text="Uploading..." class="btn btn-sm btn-info upload"
                                        title="Click here to submit data" id="uploadStudentFile" disabled="true">
                                    Submit Data
                                </button>
                                <span class="text-danger bulk_file error-text">{{ $errors->first('bulk_file') }}</span>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
                <div class="col-md-4">
                    <p><b>Information :</b></p>
                    <table class="table table-striped no-header table-bordered">
                        <tbody>
                        <tr>
                            <td style="background-color: rgb(240, 251, 239)" width="5%">
                            </td>
                            <td>
                                This color represents row inserted successfully.
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #fff0f0">
                            </td>
                            <td>
                                This color represents row contains some error.
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="/img/warning.png"
                                     alt="Error" style="width: 20px">
                            </td>
                            <td>
                                Hovering on it will display appropriate error message.
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row" id="sheetContainer" style="display:none;">
                <div class="col-sm-12">
                    <hr>
                    <div class="alert alert-danger alert-dismissable" style="display:none;">
                        <a href="#" class="close" onclick="hideErrorList()">&times;</a>
                        <strong>Error:</strong><br> <span id="errorList"></span>
                    </div>
                </div>
                <div class="col-sm-12" style="width:auto;max-width: 100%;">
                    <table id="sheetPreview" class="table table-bordered table-condensed"
                           style="width:100%;display: block;overflow-x: auto;white-space: nowrap;">
                    </table>
                </div>
            </div>
        </div>
    </form>
</div>