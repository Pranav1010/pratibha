<?php

Route::group(['middleware' => 'web', 'prefix' => 'studentimporttemp', 'namespace' => 'Modules\StudentImportTemp\Http\Controllers'], function()
{
    Route::resource('/', 'StudentImportTempController');
});
