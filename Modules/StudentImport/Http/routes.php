<?php

Route::group(['middleware' => ['web'], 'prefix' => 'studentimport', 'namespace' => 'Modules\StudentImport\Http\Controllers'], function()
{
    Route::resource('/', StudentImportController::class);
});
